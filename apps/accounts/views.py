import os
import environ
from django.conf import settings

from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page, cache_control

from accounts.models import *
from products.models import Product
from orders.models import Order, OrderItem
from finances.models import Summary
from .forms import *
from .decorators import allowed_users, unauthenticated_merchant, setup_required
from decimal import Decimal

from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

# ImageKit IO
import imagekit

# Email Verification
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string

from weasyprint import HTML, CSS
from django.views.decorators.csrf import csrf_exempt

from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from .tokens import account_activation_token

from .services import send_confirmation_email
import json, os

from django.urls import reverse
from django.db.models import Q

# To see how to set environment variables, see https://github.com/sendgrid/sendgrid-python
# Create your views here.

if not settings.LOCAL_DEV:
    import subprocess
    import ast
    
    def get_environ_vars():
        completed_process = subprocess.run(
            ['/opt/elasticbeanstalk/bin/get-config', 'environment'],
            stdout=subprocess.PIPE,
            text=True,
            check=True
        )

        return ast.literal_eval(completed_process.stdout)

def load_pending_items(request):
    days = int(request.GET.get("days"))
    earliest_time = datetime.today()
    latest_time = earliest_time + timedelta(days=days)
    
    # If tomorrow, get only for tomorrow
    if days == 1: earliest_time = latest_time
    
    pending_items = OrderItem.objects.filter(
        order__shop=request.user,
        order__complete=True,
        delivery_date__gte=earliest_time, 
        delivery_date__lte=latest_time
    ).order_by('quantity')
    pending_items = pending_items.filter(Q(order__order_status="PENDING") | Q(order__order_status="PREPARING"))

    # Get all items and its quantity
    items = {}
    for item in pending_items:
        if item.product.name in items:
            items[item.product.name] += item.quantity
        else:
            items[item.product.name] = item.quantity

    # Sort according to quantity
    items = dict(sorted(items.items(), key=lambda x: x[1], reverse=True))
    return render(request, 'dropdowns/pending-items.html', {'pending_items':items})

@setup_required
@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60*10)
@cache_control(private=True)
def dashboard(request):
    user = request.user

    try: products = Product.objects.filter(user=user)
    except: products = None

    summary, created = Summary.objects.get_or_create(shop=user)
    orders = Order.objects.filter(shop=user, complete=True)

    # Total Expected Revenue
    if created:
        total_revenue = 0
        for order in orders:
            total_revenue += order.total
        summary.total_revenue = total_revenue
        summary.save()

    total_revenues = summary.total_revenue

    # Total orders from last month
    last_month = date.today() - relativedelta(months=+1)
    last_month_orders = Order.objects.filter(shop=user, complete=True, order_date__lte=last_month)
    
    # Total Expected Revenue from last month
    last_month_revenues = Decimal(0)
    for order in last_month_orders:
        last_month_revenues += order.total
        
    if last_month_revenues == Decimal(0):
        revenue_growth_rate = (total_revenues/1)
    else:
        revenue_growth_rate = ((total_revenues-last_month_revenues)/last_month_revenues)

    order_count = orders.count()
    if last_month_orders.count() == 0: 
        order_growth_rate = (order_count/1)
    else: 
        last_month_order_count = last_month_orders.count()
        order_growth_rate = ((order_count-last_month_order_count)/last_month_order_count)

    earliest_time = datetime.today()
    latest_time = earliest_time + timedelta(days=0)
    pending_items = OrderItem.objects.filter(
        order__shop=request.user,
        order__complete=True,
        delivery_date__gte=earliest_time, 
        delivery_date__lte=latest_time
    ).order_by('quantity')
    pending_items = pending_items.filter(Q(order__order_status="PENDING") | Q(order__order_status="PREPARING"))

    # Get all items and its quantity
    items = {}
    for item in pending_items:
        if item.product.name in items:
            items[item.product.name] += item.quantity
        else:
            items[item.product.name] = item.quantity

    # Sort according to quantity
    items = dict(sorted(items.items(), key=lambda x: x[1], reverse=True))

    context = {
        'products':products, 
        'orders':orders, 
        'total_revenues':total_revenues, 
        'revenue_growth_rate': revenue_growth_rate, 
        'order_growth_rate':order_growth_rate,
        'pending_items': items
    }
    return render(request, 'quicklink/dashboard.html', context)

@unauthenticated_merchant
@cache_page(60 * 60)
def verified_merchant(request):
    context = {}
    return render(request, 'invites/verified.html')

@unauthenticated_merchant
@cache_page(60 * 60)
def invalid_invite_merchant(request):
    context = {}
    return render(request, 'invites/invalid.html')

@unauthenticated_merchant
@cache_page(60 * 60)
def uninvited_merchant(request):
    context = {}
    return render(request, 'invites/not-in-beta.html')

# @csrf_exempt
@unauthenticated_merchant
# @cache_page(10)
# @csrf_protect
# @cache_page(60 * 60)
def register_merchant(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid() and form.validate_password():
            user = User.objects._create_user(
                email=form.cleaned_data.get("email"),
                password=form.cleaned_data.get("password1")
            )
            user.role = User.Types.MERCHANT
            user.is_active = False
            merchant_group, created = Group.objects.get_or_create(name='Merchant')
            user.groups.add(merchant_group)

            user.is_active = True
            user.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')

            # to_email = user.email
            # user_id = user.pk
            # uid = urlsafe_base64_encode(force_bytes(user.pk))
            # token = default_token_generator.make_token(user)
            # link = request.build_absolute_uri(reverse('accounts:email-confirmation', args=(uid, token)))
            # send_confirmation_email(to_email, user_id, link, request)
            # return redirect('accounts:email-verification', user_id)
            return redirect('accounts:dashboard')
    context = {'form':form}
    return render(request, 'accounts/merchant-register.html', context)

@csrf_exempt
@unauthenticated_merchant
@cache_page(60 * 60)
@cache_control(private=True)
def email_verification(request, user_id):
    user = User.objects.get(id=user_id)
    to_email = user.email
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token = default_token_generator.make_token(user)
    link = request.build_absolute_uri(reverse('accounts:email-confirmation', args=(uid, token)))

    if request.is_ajax and request.method == "POST":
        send_confirmation_email(to_email, user_id, link, request)

    context = {'user': user, 'customer':False}
    return render(request, 'accounts/email-verification.html', context)

@csrf_exempt
@unauthenticated_merchant
@cache_page(15)
@cache_control(private=True)
def email_confirmation(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        success = True
    else:
        success = False
    context = {"success":success, 'customer':False}
    return render(request, 'accounts/email-confirmation.html', context)

from ph_geography.models import Province, Municipality
def load_cities(request):
    province = request.GET.get('province_pk')
    province = Province.objects.get(name=province.upper())
    municipalities = Municipality.objects.filter(province=province).order_by('name')
    # municipalities = Province.objects.select_related("municipality").get(name=province.upper()) FAIL
    return render(request, 'dropdowns/cities.html', {'municipalities':municipalities})

def load_times(request):
    from_hour = int(request.GET.get('hour'))
    
    if from_hour >= 8 and from_hour < 12:
        new_hours = [(i,f"{i}:00 AM") for i in range(from_hour + 1, 12)]
        new_hours += [(12, "12:00 PM"),]
        new_hours += [(i,f"{i-12}:00 PM") for i in range(13,24)]
        new_hours += [(0, "12:00 AM"),]
        new_hours += [(i,f"{i}:00 AM") for i in range(1, from_hour + 1)]
    elif from_hour == 12:
        new_hours = [(i,f"{i-12}:00 PM") for i in range(13,24)]
        new_hours += [(0, "12:00 AM"),]
        new_hours += [(i,f"{i}:00 AM") for i in range(1, 12)]
        new_hours += [(12, "12:00 PM"),]
    elif from_hour >= 13 and from_hour < 24:
        new_hours = [(i,f"{i-12}:00 PM") for i in range(from_hour + 1,24)]
        new_hours += [(0, "12:00 AM"),]
        new_hours += [(i,f"{i}:00 AM") for i in range(1, 12)]
        new_hours += [(12, "12:00 PM"),]
        new_hours += [(i,f"{i-12}:00 PM") for i in range(13,from_hour + 1)]
    elif from_hour == 0:
        new_hours = [(i,f"{i}:00 AM") for i in range(1, 12)]
        new_hours += [(12, "12:00 PM"),]
        new_hours += [(i,f"{i-12}:00 PM") for i in range(13, 24)]
        new_hours += [(0, "12:00 AM"),]
    else:
        new_hours = [(i,f"{i}:00 AM") for i in range(from_hour + 1, 12)]
        new_hours += [(12, "12:00 PM"),]
        new_hours += [(i,f"{i-12}:00 PM") for i in range(13, 24)]
        new_hours += [(0, "12:00 AM"),]
        new_hours += [(i,f"{i}:00 AM") for i in range(1, from_hour + 1)]

    return render(request, 'dropdowns/time.html', {'new_hours':new_hours})

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def register_shop_information(request):
    user = request.user
    form = ShopInformationForm(shop=user)
    # province = Province.objects.get(name="METRO MANILA")
    # cities = Municipality.objects.filter(province=province)
    # selected_city = None
    if request.method == 'POST':
        form = ShopInformationForm(request.POST, shop=user)
        if form.is_valid():
            shop_info, created = ShopInformation.objects.get_or_create(user=user)
            shop_info.shop_name = form.cleaned_data.get("shop_name")
            shop_info.shop_contact_number = form.cleaned_data.get("shop_contact_number")
            shop_info.shop_username = form.cleaned_data.get("shop_username")
            shop_info.shop_email = form.cleaned_data.get("shop_email")
            shop_info.save()

            # shop_info.shop_slug = shop_info.slugify_name
            # shop_info.save()

            if form.cleaned_data.get("line1"):
                shop_address, created = Address.objects.get_or_create(user=user, default=True)
                shop_address.line1 = form.cleaned_data.get("line1")
                shop_address.line2 = form.cleaned_data.get("line2")

                # Get codes -- saving IDs as foreign key is not working lol
                # city = Municipality.objects.get(name=form.cleaned_data.get("city"))
                # province = Province.objects.get(code=form.cleaned_data.get("province"))

                shop_address.city = request.POST.get("city")
                shop_address.province = form.cleaned_data.get("province")
                shop_address.postal_code = form.cleaned_data.get("postal_code")
                shop_address.save()

            social_links, created = SocialMediaLink.objects.get_or_create(user=user)
            social_links.instagram = form.cleaned_data.get("instagram")
            social_links.facebook = form.cleaned_data.get("facebook")
            social_links.twitter = form.cleaned_data.get("twitter")
            social_links.save()
            return redirect('accounts:merchant-add-logo')
    # else:
    #     if hasattr(user, 'shop_info'):
    #         shop_address, created = Address.objects.get_or_create(user=user, default=True)
    #         if shop_address.province != None: province = Province.objects.get(name=shop_address.province.upper()) # Get user's province
    #         else: province = Province.objects.get(name="METRO MANILA") # If not available, use Metro Manila as default
    #         cities = Municipality.objects.filter(province=province) # Get available cities
    #         selected_city = shop_address.city # Manually render selected city


    # context = {'form':form, 'cities':cities, 'selected_city':selected_city}
    context = {'form':form}
    return render(request, 'accounts/shop-information.html', context)

### The Set Callback URLs API allows you to set your sub-accounts' Callback URLs.
# Use your production key to set production URLs; use your development key to set development URLs.
# Note: Production callback URLs have to use the https protocol.
def account_callback(request, user_id):
    user_id = user_id
    return HttpResponse(request.body)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def register_shop_logo(request):
    user = request.user
    shop, created = ShopInformation.objects.get_or_create(user=user)
    form = ShopBrandingForm(shop=shop)

    if request.method == 'POST':
        form = ShopBrandingForm(request.POST, request.FILES)
        if form.is_valid():
            logo, created = ShopLogo.objects.get_or_create(shop=shop)
            brand, created = ShopBranding.objects.get_or_create(shop=shop)
            if request.FILES:
                logo.logo = form.cleaned_data.get('logo')
                brand.thumbnail = form.cleaned_data.get('thumbnail')
            brand.description = form.cleaned_data.get('description')
            logo.save()
            brand.save()
            return redirect('accounts:merchant-add-settings')

    context = {'form':form, 'shop':shop}
    return render(request, 'accounts/shop-branding.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def register_shop_settings(request):
    user = request.user
    shop, created = ShopInformation.objects.get_or_create(user=user)
    selected_dates = []
    
    if hasattr(shop, 'shop_general_settings'):
        form = ShopSettingsForm(request=request)
        selected_dates = WEEKDAYS.get_selected_values(shop.shop_general_settings.delivery_days)
    else:
        form = ShopSettingsForm(request=request)

    if request.method == 'POST':
        form = ShopSettingsForm(request.POST)
        if form.is_valid():
            shop_settings, created = ShopGeneralSettings.objects.get_or_create(shop=shop)
            shop_settings.shop_customer_fees=form.cleaned_data.get('shop_customer_fees')
            shop_settings.delivery_days=form.cleaned_data.get('delivery_days')
            shop_settings.delivery_from_hour=form.cleaned_data.get('from_hour')
            shop_settings.delivery_to_hour=form.cleaned_data.get('to_hour')

            if form.cleaned_data.get('delivery_days') == 127:
                shop_settings.delivery_everyday = True
            else:
                shop_settings.delivery_everyday = False

            if "checkbox-cutoff-time" in request.POST:
                shop_settings.cutoff_time=form.cleaned_data.get('cutoff_time')
            else:
                shop_settings.cutoff_time=None

            if "checkbox-cutoff-days" in request.POST:
                shop_settings.cutoff_days=form.cleaned_data.get('cutoff_days')
            else:
                shop_settings.cutoff_days=None

            shop_settings.save()
        return redirect('accounts:merchant-add-delivery')
    context = {'form':form, 'selected_dates':selected_dates}
    return render(request, 'accounts/shop-settings.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def register_shop_deliveries(request):
    user = request.user
    shop, created = ShopInformation.objects.get_or_create(user=user)
    shop_address, created = Address.objects.get_or_create(user=user, default=True)
    use_same_address = False
    
    form = DeliverySettingsForm(shop=user)
    
    if hasattr(shop, 'shop_delivery_settings'):
        if shop.shop_delivery_settings.line1 == shop_address.line1 and shop.shop_delivery_settings.city == shop_address.city:
            use_same_address = True

    if request.method == 'POST':
        form = DeliverySettingsForm(request.POST, shop=user)
        if form.is_valid():
            shop_delivery_settings, created = ShopDeliverySettings.objects.get_or_create(shop=shop)
            if 'seller_books' in request.POST:
                shop_delivery_settings.seller_books = True
                # shop.shop_delivery_fees = form.clean_delivery_fees()
                # shop.save()
            else:
                shop_delivery_settings.seller_books = False
                # shop.shop_delivery_fees = None
                # shop.save()
            
            if 'buyer_books' in request.POST: shop_delivery_settings.buyer_books = True
            else: shop_delivery_settings.buyer_books = False

            if 'buyer_picks_up' in request.POST: shop_delivery_settings.buyer_picks_up = True
            else: shop_delivery_settings.buyer_picks_up = False

            if form.cleaned_data.get('buyer_books') == True or form.cleaned_data.get('buyer_picks_up') == True:
                if 'use_shop_address' in request.POST:
                    shop_delivery_settings.line1 = shop_address.line1
                    shop_delivery_settings.line2 = shop_address.line2
                    shop_delivery_settings.city = shop_address.city
                    shop_delivery_settings.province = shop_address.province
                    shop_delivery_settings.postal_code = shop_address.postal_code
                else:    
                    shop_delivery_settings.line1 = form.cleaned_data.get('line1')
                    shop_delivery_settings.line2 = form.cleaned_data.get('line2')
                    shop_delivery_settings.city = request.POST.get('city')
                    shop_delivery_settings.province = form.cleaned_data.get('province')
                    shop_delivery_settings.postal_code = form.cleaned_data.get('postal_code')

            if 'mobile_number' in request.POST: shop_delivery_settings.mobile_number = True
            else: shop_delivery_settings.mobile_number = False

            if 'facebook' in request.POST: shop_delivery_settings.facebook = True
            else: shop_delivery_settings.facebook = False
            
            if 'instagram' in request.POST: shop_delivery_settings.instagram = True
            else: shop_delivery_settings.instagram = False

            shop_delivery_settings.save()
        return redirect('accounts:merchant-add-payment')

    context = {'form':form, 'use_same_address':use_same_address}
    return render(request, 'accounts/shop-deliveries.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def register_shop_account(request):
    user = request.user
    user_account, created = BankAccount.objects.get_or_create(user=user, saved=True)

    if hasattr(user, 'user_account'):
        form = ShopAccountForm(initial={
            'bank_name': user_account.bank_name,
            'cardholder_name': user_account.cardholder_name,
            'account_number': user_account.account_number,
        })
    else:
        form = ShopAccountForm()

    if request.method == 'POST':
        form = ShopAccountForm(request.POST)
        if form.is_valid():
            user_account.bank_name = form.cleaned_data.get('bank_name')
            user_account.cardholder_name = form.cleaned_data.get('cardholder_name')
            user_account.account_number = form.cleaned_data.get('account_number')
            user_account.save()
            messages.success(request, 'Profile successfully registered!')
            return redirect('accounts:dashboard')

    context = {'form':form}
    return render(request, 'accounts/shop-account.html', context)

@unauthenticated_merchant
# @csrf_exempt
# @cache_page(10)
# @csrf_protect
# @cache_page(60 * 60)
def login_merchant(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')

        user = authenticate(email=email, password=password)

        if user is not None:
            if user.is_active:
                login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                return redirect('accounts:dashboard')
            else:
                messages.error(request, "User is inactive. Please contact support@quicklink.ph if something is wrong.")    
                # return redirect ('accounts:login')
        else:
            messages.error(request, "Email or password is incorrect.")
            # return redirect ('accounts:login')
    
    context = {}
    return render(request, 'accounts/merchant-login.html', context)

def logout_merchant(request):
    logout(request)
    return redirect('accounts:login')

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def edit_shop(request):
    user = request.user
    shop = ShopInformation.objects.get(user=user)
    context = {'user':user, 'shop':shop}
    return render(request, 'settings/shop.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def edit_shop_information(request):
    user = request.user
    shop = ShopInformation.objects.get(user=user)
    shop_address, address_created = Address.objects.get_or_create(user=user, default=True)
    user_links, links_created = SocialMediaLink.objects.get_or_create(user=user)

    if address_created: province = Province.objects.get(name="METRO MANILA")
    else: province = Province.objects.get(name=shop_address.province.upper())
    cities = Municipality.objects.filter(province=province)

    # Manually render selected city
    selected_city = shop_address.city

    form = ShopInformationForm(shop=user)

    if request.method == 'POST':
        if request.POST:
            shop_info_form = ShopInformationForm(request.POST, shop=user)
            if shop_info_form.is_valid():
                shop_info, created = ShopInformation.objects.get_or_create(user=user)
                shop_info.shop_name = shop_info_form.cleaned_data.get("shop_name")
                shop_info.shop_contact_number = shop_info_form.cleaned_data.get("shop_contact_number")
                shop_info.shop_username = shop_info_form.cleaned_data.get("shop_username")
                shop_info.shop_email = shop_info_form.cleaned_data.get("shop_email")
                shop_info.save()

                # shop_info.shop_slug = shop_info.slugify_name
                # shop_info.save()

                shop_address.line1 = shop_info_form.cleaned_data.get("line1")
                shop_address.line2 = shop_info_form.cleaned_data.get("line2")
                shop_address.city = request.POST.get("city")
                shop_address.province = shop_info_form.cleaned_data.get("province")
                shop_address.postal_code = shop_info_form.cleaned_data.get("postal_code")
                shop_address.save()

                social_links, created = SocialMediaLink.objects.get_or_create(user=user)
                social_links.instagram = shop_info_form.cleaned_data.get("instagram")
                social_links.facebook = shop_info_form.cleaned_data.get("facebook")
                social_links.twitter = shop_info_form.cleaned_data.get("twitter")
                social_links.save()
        messages.success(request, 'Shop information successfully saved!')
        return redirect('accounts:shop')

    context = {
        'user':user, 
        'shop':shop, 
        'form':form, 
        'cities':cities, 
        'selected_city':selected_city,
        'is_edit':True
    }
    return render(request, 'accounts/shop-information.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def edit_shop_branding(request):
    user = request.user
    shop = ShopInformation.objects.get(user=user)
    form = ShopBrandingForm(shop=shop)

    if request.method == 'POST':
        form = ShopBrandingForm(request.POST, request.FILES)
        if form.is_valid():
            logo, created = ShopLogo.objects.get_or_create(shop=shop)
            brand, created = ShopBranding.objects.get_or_create(shop=shop)
            if request.FILES:
                logo.logo = form.cleaned_data.get('logo')
                brand.thumbnail = form.cleaned_data.get('thumbnail')
            brand.description = form.cleaned_data.get('description')
            logo.save()
            brand.save()
            messages.success(request, 'Shop branding successfully saved!')
            return redirect('accounts:shop')

    context = {'user':user, 'shop':shop, 'form':form, 'is_edit':True}
    return render(request, 'accounts/shop-branding.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def edit_shop_settings(request):
    user = request.user
    shop = ShopInformation.objects.get(user=user)
    selected_dates = []
    
    if hasattr(shop, 'shop_general_settings'):
        form = ShopSettingsForm(request=request)
        selected_dates = WEEKDAYS.get_selected_values(shop.shop_general_settings.delivery_days)
    else:
        form = ShopSettingsForm(request=request)

    if request.method == 'POST':
        form = ShopSettingsForm(request.POST)
        if form.is_valid():
            shop_settings, created = ShopGeneralSettings.objects.get_or_create(shop=shop)
            shop_settings.shop_customer_fees=form.cleaned_data.get('shop_customer_fees')
            shop_settings.delivery_days=form.cleaned_data.get('delivery_days')
            shop_settings.delivery_from_hour=form.cleaned_data.get('from_hour')
            shop_settings.delivery_to_hour=form.cleaned_data.get('to_hour')

            if form.cleaned_data.get('delivery_days') == 127:
                shop_settings.delivery_everyday = True
            else:
                shop_settings.delivery_everyday = False

            if "checkbox-cutoff-time" in request.POST:
                shop_settings.cutoff_time=form.cleaned_data.get('cutoff_time')
            else:
                shop_settings.cutoff_time=None

            if "checkbox-cutoff-days" in request.POST:
                shop_settings.cutoff_days=form.cleaned_data.get('cutoff_days')
            else:
                shop_settings.cutoff_days=None

            shop_settings.save()
            messages.success(request, 'Shop general settings successfully saved!')
            return redirect('accounts:shop')
    context = {'user':user, 'shop':shop, 'form':form, 'selected_dates':selected_dates, 'is_edit':True}
    return render(request, 'accounts/shop-settings.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def edit_shop_deliveries(request):
    user = request.user
    shop = ShopInformation.objects.get(user=user)
    shop_address, created = Address.objects.get_or_create(user=user, default=True)
    use_same_address = False

    form = DeliverySettingsForm(shop=user)
    
    if hasattr(shop, 'shop_delivery_settings'):
        if shop.shop_delivery_settings.line1 == shop_address.line1 and shop.shop_delivery_settings.city == shop_address.city:
            use_same_address = True

    if request.method == 'POST':
        form = DeliverySettingsForm(request.POST, shop=user)
        if form.is_valid():
            shop_delivery_settings, created = ShopDeliverySettings.objects.get_or_create(shop=shop)
            if 'seller_books' in request.POST:
                shop_delivery_settings.seller_books = True
                # shop.shop_delivery_fees = form.clean_delivery_fees()
                # shop.save()
            else:
                shop_delivery_settings.seller_books = False
                # shop.shop_delivery_fees = None
                # shop.save()
            
            if 'buyer_books' in request.POST: shop_delivery_settings.buyer_books = True
            else: shop_delivery_settings.buyer_books = False

            if 'buyer_picks_up' in request.POST: shop_delivery_settings.buyer_picks_up = True
            else: shop_delivery_settings.buyer_picks_up = False

            if form.cleaned_data.get('buyer_books') == True or form.cleaned_data.get('buyer_picks_up') == True:
                if 'use_shop_address' in request.POST:
                    shop_delivery_settings.line1 = shop_address.line1
                    shop_delivery_settings.line2 = shop_address.line2
                    shop_delivery_settings.city = shop_address.city
                    shop_delivery_settings.province = shop_address.province
                    shop_delivery_settings.postal_code = shop_address.postal_code
                else:    
                    shop_delivery_settings.line1 = form.cleaned_data.get('line1')
                    shop_delivery_settings.line2 = form.cleaned_data.get('line2')
                    shop_delivery_settings.city = form.cleaned_data.get('city')
                    shop_delivery_settings.province = form.cleaned_data.get('province')
                    shop_delivery_settings.postal_code = form.cleaned_data.get('postal_code')

            if 'mobile_number' in request.POST: shop_delivery_settings.mobile_number = True
            else: shop_delivery_settings.mobile_number = False

            if 'facebook' in request.POST: shop_delivery_settings.facebook = True
            else: shop_delivery_settings.facebook = False
            
            if 'instagram' in request.POST: shop_delivery_settings.instagram = True
            else: shop_delivery_settings.instagram = False

            shop_delivery_settings.save()
            messages.success(request, 'Shop delivery and courier successfully saved!')
            return redirect('accounts:shop')
    context = {'user':user, 'shop':shop, 'form':form, 'use_same_address':use_same_address, 'is_edit':True}
    return render(request, 'accounts/shop-deliveries.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(5)
def edit_shop_account(request):
    user = request.user
    shop = ShopInformation.objects.get(user=user)
    user_account, created = BankAccount.objects.get_or_create(user=user, saved=True)

    if hasattr(user, 'user_account'):
        form = ShopAccountForm(initial={
            'bank_name': user_account.bank_name,
            'cardholder_name': user_account.cardholder_name,
            'account_number': user_account.account_number,
        })
    else:
        form = ShopAccountForm()

    if request.method == 'POST':
        form = ShopAccountForm(request.POST)
        if form.is_valid():
            user_account.bank_name = form.cleaned_data.get('bank_name')
            user_account.cardholder_name = form.cleaned_data.get('cardholder_name')
            user_account.account_number = form.cleaned_data.get('account_number')
            user_account.save()
            messages.success(request, 'Shop bank account successfully saved!')
            return redirect('accounts:shop')
            
    context = {'user':user, 'shop':shop, 'form':form, 'is_edit': True}
    return render(request, 'accounts/shop-account.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60 * 60)
def account_settings(request):
    user = request.user
    shop = ShopInformation.objects.get(user=user)
    form = NewPasswordForm()

    if request.method == "POST":
        if request.is_ajax and request.POST.get('message') == "delete_account":
            logout(request)
            user.is_active = False
            user.save()
            return redirect('accounts:login')
        else:
            form = NewPasswordForm(request.POST)
            if form.is_valid() and form.validate_old_pass(user):
                if form.validate_new_pass():
                    new_pass = form.cleaned_data.get("new_password1")
                    user.set_password(new_pass)
                    user.save()
                    login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                    messages.success(request, 'Successfully changed password!')

    context = {'user':user, 'shop':shop, 'form':form}
    return render(request, 'settings/account.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
# @cache_page(60 * 60)
def notification_settings(request):
    user = request.user
    shop = user.shop_info
    merchant_notifications, created = MerchantNotification.objects.get_or_create(user=user)
    settings = {
        'received_payouts': merchant_notifications.received_payouts,
        'daily_order_summary': merchant_notifications.daily_order_summary,
        'product_updates': merchant_notifications.product_updates,
        'marketing_updates': merchant_notifications.marketing_updates
    }
        
    context = {'user':user, 'shop':shop, 'settings':settings}
    return render(request, 'settings/notifications.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
def save_notification_settings(request):
    user = request.user
    merchant_notifications, created = MerchantNotification.objects.get_or_create(user=user)

    if request.method == "POST" and request.is_ajax:
        if request.POST.get("message") == "received_payouts":
            if request.POST.get("status") == "true":
                merchant_notifications.received_payouts = True
            elif request.POST.get("status") == "false":
                merchant_notifications.received_payouts = False
                messages.success(request, "Disabled received payouts notification!")
        if request.POST.get("message") == "daily_order_summary":
            if request.POST.get("status") == "true":
                merchant_notifications.daily_order_summary = True
                messages.success(request, "Toggled daily order summary notification!")
            elif request.POST.get("status") == "false":
                merchant_notifications.daily_order_summary = False
                messages.success(request, "Disabled daily order summary notification!")
        if request.POST.get("message") == "product_updates":
            if request.POST.get("status") == "true":
                merchant_notifications.product_updates = True
                messages.success(request, "Toggled product updates notification!")
            elif request.POST.get("status") == "false":
                merchant_notifications.product_updates = False
                messages.success(request, "Disabled product updates notification!")
        if request.POST.get("message") == "marketing_updates":
            if request.POST.get("status") == "true":
                merchant_notifications.marketing_updates = True
                messages.success(request, "Toggled marketing updates notification!")
            elif request.POST.get("status") == "false":
                merchant_notifications.marketing_updates = False
                messages.success(request, "Disabled marketing updates notification!")
        merchant_notifications.save()
        return redirect('accounts:notification-settings')
    return JsonResponse(request.POST, safe=False)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60 * 60)
def resource_settings_faqs(request):
    user = request.user
    context = {'user':user}
    return render(request, 'settings/faqs.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60 * 60)
def resource_settings_feedback(request):
    user = request.user
    form = ShopFeedbackForm()

    if request.method == "POST":
        form = ShopFeedbackForm(request.POST)
        if form.is_valid():
            feedback = MerchantFeedback.objects.create(
                user=user,
                name=form.cleaned_data.get("name"),
                email=form.cleaned_data.get("email"),
                shop_name=form.cleaned_data.get("shop_name"),
                feedback=form.cleaned_data.get("feedback")
            )
            feedback.save()

            discord_url = "https://discord.com/api/webhooks/841016374268526592/Dkauo1H_0tqcLSHEO7B0jK594wSSMBlQMKpBIM9r4kEY17kC22zxv5C4rl2uB65XBNy2"
            if settings.LOCAL_DEV == False:
                if hasattr(user, 'shop_info'):
                    try:
                        icon_url = user.shop_info.shop_logo.logo.url
                    except:
                        icon_url = "https://ik.imagekit.io/ripzjge77zz/Quicklink_App/Marketplace/Settings/Group_358_s1885d_rX-X.png"
                elif hasattr(user, 'customer_info'):
                    try:
                        icon_url = user.customer_info.customer_dp.profile_pic.url
                    except:
                        icon_url = "https://ik.imagekit.io/ripzjge77zz/Quicklink_App/Marketplace/Settings/Group_358_s1885d_rX-X.png"
            else:
                icon_url = "https://ik.imagekit.io/ripzjge77zz/Quicklink_App/Marketplace/Settings/Group_358_s1885d_rX-X.png"
            
            webhook = DiscordWebhook(url=discord_url)
            embed = DiscordEmbed(
                title="New Quickie Feedback!", description="Make every quickie happy :>", color='f8f8f8'
            )
            embed.set_author(
                name=feedback.name,
                icon_url=icon_url,
            )
            embed.add_embed_field(name="Email", value=feedback.email, inline=False)
            embed.add_embed_field(name="Shop Name", value=feedback.shop_name, inline=False)
            embed.add_embed_field(name="Feedback", value=feedback.feedback, inline=False)
            webhook.add_embed(embed)
            response = webhook.execute()
            messages.success(request, "Successfully sent feedback. We'll get back to you shortly!")

    context = {'user':user, 'form':form}
    return render(request, 'settings/feedback_form.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60 * 60)
def resource_settings_privacy_policy(request):
    user = request.user
    context = {'user':user}
    return render(request, 'settings/privacy_policy.html', context)

@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60 * 60)
def resource_settings_agreement(request):
    user = request.user
    context = {'user':user}
    return render(request, 'settings/agreement.html', context)

def handler_400(request, exception, template_name="error/error_400.html"):
    response = render(request, 'error/error_400.html', {})
    response.status_code = 400
    return response
    
def handler_403(request, exception, template_name="error/error_403.html"):
    response = render(request, 'error/error_403.html', {})
    response.status_code = 403
    return response

def handler_404(request, exception, template_name="error/error_404.html"):
    response = render(request, 'error/error_404.html', {})
    response.status_code = 404
    return response

def handler_500(request, template_name="error/error_500.html"):
    # error_form_url = request.build_absolute_uri(reverse('accounts:error-form'))
    # response = render(request, 'error/error_500.html', {'error_form_url':error_form_url})
    response = render(request, 'error/error_500.html', {})
    response.status_code = 500
    return response

def handler_csrf(request, reason=""):
    return render(request, 'error/error_500.html', {})
    
from discord_webhook import DiscordWebhook, DiscordEmbed
from django.conf import settings

def error_form_submission(request):
    user = request.user
    icon_url = "https://ik.imagekit.io/ripzjge77zz/Quicklink_App/Marketplace/Settings/Group_358_s1885d_rX-X.png"
    if settings.LOCAL_DEV == False and user.is_authenticated:
        if hasattr(user, 'shop_info'):
            if hasattr(user.shop_info, "shop_logo"):
                icon_url = user.shop_info.shop_logo.logo.url
        else:
            if hasattr(user, 'customer_info'):
                if hasattr(user.customer_info, "customer_dp"):
                    icon_url = user.customer_info.customer_dp.profile_pic.url

    if request.method == 'POST':
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        summary = request.POST.get('summary', '')
        description = request.POST.get('description', '')

        discord_url = "https://discord.com/api/webhooks/840992854360588368/Gg-I9wKnQDNizKUM8wIR1l1sOxUxbnJZpHuW2QaMrVpSNgfZC3GhiH6z-xPGHvzP0gY_"
        webhook = DiscordWebhook(url=discord_url)
        embed = DiscordEmbed(
            title="New Quickie Bug Report!", description="Make every quickie happy :>", color='f8f8f8'
        )
        try:
            embed.set_author(
                name=name,
                icon_url=icon_url,
            )
        except:
            embed.set_author(
                name=name
            )
        embed.add_embed_field(name="Reported by", value=f"{name} ({email})", inline=False)
        embed.add_embed_field(name="Summary", value=summary, inline=False)
        embed.add_embed_field(name="Description", value=description, inline=False)
        webhook.add_embed(embed)
        response = webhook.execute()
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=500)