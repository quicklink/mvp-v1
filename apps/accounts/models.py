from django.db import models
from django.utils.timezone import datetime, timedelta
from django.contrib.auth.models import UnicodeUsernameValidator, AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from .validators import only_int, exp_date

from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFill, ResizeToFit
from model_utils.fields import MonitorField

import re


### Create your models here.
## Django Roles, Permissions, and Groups: https://medium.com/djangotube/django-roles-groups-and-permissions-introduction-a54d1070544
## Django Custom Authentication: https://docs.djangoproject.com/en/3.1/topics/auth/customizing/

##### SUMMARY OF ACCOUNT MODELS:
#### Super Admin — Quicklink High-Ranking Programmers (CRUD All)
### Admin — Quicklink Founders, High-Ranking Employees, and Programmers (CUD Some, R All)
## Staff — Quicklink Employees
# Merchants — Quicklink Clients / Users ()
# Customers — Customers of Quicklink Clients

class BaseAccountsModel(models.Model):
    class Meta:
        abstract = True
        app_label = 'accounts'

class MyUserManager(BaseUserManager):
    ## A custom user manager to deal with emails as unique identifiers for auth
    # instead of usernames. The default that's used is "UserManager"

    def _create_user(self, email, password, **extra_fields):
        # Creates and saves a User with the given email and password.
        if not email:
            raise ValueError('The email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)

    def get_merchants(self):
        return super().get_queryset().filter(role='MERCHANT')

    def get_customers(self):
        return super().get_queryset().filter(role='CUSTOMER')

class User(AbstractBaseUser, PermissionsMixin):
    class Types(models.TextChoices):
        SUPER_ADMIN = "SUPER_ADMIN", "Super Admin"
        ADMIN = "ADMIN", "Admin"
        QUICKIES = "QUICKIES", "Quickies"
        MERCHANT = "MERCHANT", "Merchant"
        CUSTOMER = "CUSTOMER", "Customer"

    role = models.CharField(_('role'), max_length=50, choices=Types.choices, default=Types.ADMIN, null=True)
    
    username = None
    email = models.EmailField(_("email address"), 
        max_length=150, 
        null=True, 
        unique=True,
        help_text=_("Please use a business email address."),
        error_messages={"unique": _("That email is already being used by another account."),},
    )

    password = models.CharField(max_length=150, null=True)

    device_id = models.CharField(max_length=200, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []
    
    objects = MyUserManager()

    def __str__(self):
        if self.email:
            return f"{self.email}"
        elif self.device_id:
            return f"{self.device_id}"

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def get_initials(self):
        initials = []
        if hasattr(self, 'shop_info'):
            for name in list(self.shop_info.shop_name.split(' ')):
                initials.append(name[0])
            return ''.join(initials)
        elif hasattr(self, 'info_customer'):
            for name in list(self.info_customer.customer_name.split(' ')):
                initials.append(name[0])
            return ''.join(initials)
        else:
            return False

    class Meta:
        app_label = 'accounts'

# ACCOUNT INFORMATION
# Address
class Address(BaseAccountsModel):
    CITIES = [
        ("Caloocan City", _("Caloocan")),
        ("Las Piñas City", _("Las Piñas")),
        ("Makati City", _("Makati")),
        ("Malabon City", _("Malabon")),
        ("Malanduyong City", _("Malanduyong")),
        ("Manila City", _("Manila")),
        ("Marikina City", _("Marikina")),
        ("Muntinlupa City", _("Muntinlupa")),
        ("Navotas City", _("Navotas")),
        ("Parañaque City", _("Parañaque")),
        ("Pasay City", _("Pasay")),
        ("Pasig City", _("Pasig")),
        ("Pateros City", _("Pateros")),
        ("Quezon City", _("Quezon City")),
        ("San Juan City", _("San Juan")),
        ("Taguig City", _("Taguig")),
        ("Valenzuela City", _("Valenzuela")),
    ]

    PROVINCES = [
        ("Metro Manila", _("Metro Manila")),
    ]

    user = models.ForeignKey(User, related_name='user_address', on_delete=models.CASCADE, null=True, blank=True)
    line1 = models.CharField(_("address line1"), null=True, blank=False, max_length=155)
    line2 = models.CharField(_("address line2"), null=True, blank=True, max_length=155)
    city = models.CharField(_("address city"), null=True, blank=True, max_length=155)
    province = models.CharField(_("address province"), null=True, blank=True, max_length=155)
    postal_code = models.CharField(_("address postal code"), null=True, max_length=4, blank=False, validators=[only_int])
    default = models.BooleanField(_('default address'), null=True, default=False)

    def __str__(self):
        return f"{self.line1}, {self.line2}, {self.city}, {self.province}, Philippines, {self.postal_code}"

    def display_address(self):
        if self.city:
            if self.city[-1] == "y":
                return f"{self.city}"
            else:
                return f"{self.city} City"
        # if self.line2 and self.city:
        #     return f"{self.line2}, {self.city}"
        # elif self.line1 and self.city:
        #     return f"{self.line1}, {self.city}"

# Social Media Links
class SocialMediaLink(BaseAccountsModel):
    user = models.OneToOneField(User, related_name='user_links', on_delete=models.CASCADE, null=True, blank=True)
    instagram = models.CharField(_("instagram link"), null=True, blank=True, default="", max_length=255)
    facebook = models.CharField(_("facebook link"), null=True, blank=True, default="", max_length=255)
    twitter = models.CharField(_("twitter link"), null=True, blank=True, default="", max_length=255)

    def __str__(self):
        accounts = []
        if self.instagram: accounts.append('Instagram')
        if self.facebook: accounts.append('Facebook')
        if self.twitter: accounts.append('Twitter')
        if len(accounts) == 0:
            return "No social media accounts"
        return f"Linked social media accounts: {', '.join(accounts)}"

# Bank Account Information
class BankAccount(BaseAccountsModel):
    BANKS = [
        ('', 'Choose bank / e-wallet'),
        ("DC/CC", _("Debit/Credit Card")),
        # Direct Debit
        ("BA_BPI", _("Bank of the Philippine Islands (BPI)")),
        ("BA_BDO", _("Banco de Oro (BDO)")),
        ("BA_UBP", _("Unionbank Philippines (UBP)")),
        # E-Wallet
        ("PH_GCASH", _("GCash PH")),
        ("PH_PAYMAYA", _("PayMaya PH")),
        ("PH_GRABPAY", _("GrabPay")),
    ]
    
    user = models.ForeignKey(User, related_name='user_account', on_delete=models.SET_NULL, null=True)
    bank_name = models.CharField(_("bank name"), choices=BANKS, null=True, max_length=55)
    cardholder_name = models.CharField(_("cardholder name"), null=True, max_length=155)
    account_number = models.CharField(_("account number"), null=True, max_length=55)
    exp_date = models.CharField(_("expiration date"), null=True, blank=True, max_length=5, validators=[exp_date])
    cvv = models.CharField(_("card verification value"), null=True, blank=True, max_length=3)
    saved = models.BooleanField(_('saved bank account'), null=True, default=False)
    linked_account_token_id = models.CharField(_("xendit linked account token id"), null=True, blank=True, max_length=155)

    @property
    def get_last_four(self):
        account_number = ''.join(self.account_number.split(' '))
        last_four = account_number[-4:]
        return f"{last_four}"

    def __str__(self):
        if hasattr(self.user, 'shop_info'):
            shop_info = self.user.shop_info
            return f"[{shop_info.shop_name}] {self.cardholder_name} - {self.account_number}"
        else:
            return f"{self.cardholder_name} - {self.account_number}"

class CustomerNotification(BaseAccountsModel):
    user = models.ForeignKey(User, related_name='customer_notifications', on_delete=models.CASCADE, null=True, blank=True)
    sms = models.BooleanField(_('sms notifications'), null=True, default=False)
    email = models.BooleanField(_('email notifications'), null=True, default=False)
    default = models.BooleanField(_('default notifications setting'), null=True, default=False)

    def __str__(self):
        return f"SMS: {self.sms} | Email: {self.email}"

class MerchantNotification(BaseAccountsModel):
    user = models.OneToOneField(User, related_name='merchant_notifications', on_delete=models.CASCADE, null=True)
    # Get an email notification every time we send your revenue to your bank account.
    received_payouts = models.BooleanField(null=True, default=True)

    # Get an email that outlines your orders for the day.
    daily_order_summary = models.BooleanField(null=True, default=True)

    # Get informed whenever we release a new feature on Quicklink!
    product_updates = models.BooleanField(null=True, default=True)

    # Stay updated with news about Quicklink!
    marketing_updates = models.BooleanField(null=True, default=True)

    def __str__(self):
        if hasattr(self.user, "shop_info"):
            if self.user.shop_info.shop_name[-1] == 's':
                try:
                    return f"{self.user.shop_info.shop_name}' Notification Settings"
                except:
                    return f"{self.user}'s Notification Settings"
            else:
                try:
                    return f"{self.user.shop_info.shop_name}'s Notification Settings"
                except:
                    return f"{self.user}'s Notification Settings"
        else:
            return f"{self.user}'s Notification Settings"

class MerchantFeedback(BaseAccountsModel):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="shop_feedback")
    name = models.CharField(_("Merchant Feedback Sender"), null=True, max_length=55)
    email = models.EmailField(_("Merchant Feedback Email"), null=True, max_length=55)
    shop_name = models.CharField(_("Merchant Feedback Shop"), null=True, max_length=55)
    feedback = models.CharField(_("Merchant Feedback"), null=True, max_length=255)

    def __str__(self):
        try:
            return f"Feedback from {self.user.shop_info.shop_name}"
        except:
            return f"Feedback from {self.user}"

# MERCHANT
# class MerchantManager(models.Manager):
#     def get_queryset(self, *args, **kwargs):
#         return super().get_queryset(*args, **kwargs).filter(type=User.Types.MERCHANT)

# class Merchant(User):
#     base_type = User.Types.MERCHANT
#     objects = MerchantManager()

#     class Meta:
#         proxy = True

#     def save(self, *args, **kwargs):
#         if not self.pk:
#             self.type = User.Types.MERCHANT
#         return super().save(*args, **kwargs)

# Shop Information
class ShopInformation(BaseAccountsModel):
    user = models.OneToOneField(
        User, related_name="shop_info", on_delete=models.CASCADE
    )
    shop_name = models.CharField(_("shop name"), null=True, max_length=30)
    shop_contact_number = models.CharField(_("contact number"), max_length=25, null=True)
    shop_username = models.CharField(_("shop username"), 
        max_length=150, 
        null=True, 
        unique=True, 
        help_text=_(
            "This will be used for your order form link (quicklink.ph/username)."
        ),
        error_messages={"unique": _("A user with that username already exists."),},
    )
    shop_email = models.EmailField(_("email address"), 
        max_length=150, 
        null=True,
        help_text=_(
            "Your customers will contact you through this email address."
        ),
    )
    shop_delivery_fees = models.DecimalField(null=True, blank=False, max_digits=5, decimal_places=2, default=0.00)

    # Shop slug
    # shop_slug = models.SlugField(null=True, blank=True)

    # Check if new update can be sent to customers (under Orders Page)
    can_send_updates = models.BooleanField(null=True, default=False)
    update_status_changed = MonitorField(monitor='can_send_updates')

    def __str__(self):
        return f"{self.shop_username} - {self.shop_name} ({self.shop_contact_number})"

    # https://www.peterbe.com/plog/fastest-python-function-to-slugify-a-string
    @property
    def slugify_name(self):
        non_url_safe = ['"', '#', '$', '%', '&', '+',
                ',', '/', ':', ';', '=', '?',
                '@', '[', '\\', ']', '^', '`',
                '{', '|', '}', '~', "'"]

        translate_table = {ord(char): u'' for char in non_url_safe}
        non_url_safe_regex = re.compile(
            r'[{}]'.format(''.join(re.escape(x) for x in non_url_safe)))

        name = self.shop_username
        name = name.translate(translate_table)
        name = u'_'.join(name.split())
        name = str(name.lower())
        return name

    @property
    def get_email(self):
        email = self.shop_email
        if email:
            return f"{email}"
        else:
            return "No email available."

    @property
    def get_formatted_number(self):
        number = self.shop_contact_number
        if number:
            prefix = ''.join(number[:3])
            first = ''.join(number[3:6])
            middle = ''.join(number[6:9])
            last = ''.join(number[9:])
            return f"({prefix}) {first} {middle} {last}"
        else:
            return "No number available."

# Available Delivery Schedule
# See more information on how to do it here: https://stackoverflow.com/questions/3663898/representing-a-multi-select-field-for-weekdays-in-a-django-model

class BitChoices(object):
  def __init__(self, choices):
    self._choices = []
    self._lookup = {}
    for index, (key, val) in enumerate(choices):
      index = 2**index
      self._choices.append((index, val))
      self._lookup[key] = index

  def __iter__(self):
    return iter(self._choices)

  def __len__(self):
    return len(self._choices)

  def __getattr__(self, attr):
    try:
      return self._lookup[attr]
    except KeyError:
      raise AttributeError(attr)

  def get_selected_keys(self, selection):
    # Return a list of keys for the given selection
    return [ k for k,b in self._lookup.iteritems() if b & selection]

  def get_selected_values(self, selection):
    # Return a list of values for the given selection
    return [ v for b,v in self._choices if b & selection]

WEEKDAYS = BitChoices((
    ('Mon', 'Monday'), ('Tues', 'Tuesday'), ('Wed', 'Wednesday'),
    ('Thurs', 'Thursday'), ('Fri', 'Friday'), ('Sat', 'Saturday'),
    ('Sun', 'Sunday')
))

# General Shop Settings
class ShopGeneralSettings(BaseAccountsModel):
    HOUR_OF_DAY_24 = [
        (i,f"{i}:00 AM") for i in range(8,12)
    ]

    HOUR_OF_DAY_24 += [
        (12, "12:00 PM"),
    ]

    HOUR_OF_DAY_24 += [
        (i,f"{i-12}:00 PM") for i in range(13,24)
    ]

    HOUR_OF_DAY_24 += [
        (0, "12:00 AM"),
    ]

    HOUR_OF_DAY_24 += [
        (i,f"{i}:00 AM") for i in range(1,7)
    ]

    shop = models.OneToOneField(ShopInformation, related_name='shop_general_settings', on_delete=models.CASCADE, null=True, blank=True)
    shop_customer_fees = models.BooleanField(_("shop customer fees"), null=True, blank=True, default=False)

    # Order cut-off
    cutoff_days = models.IntegerField(null=True, blank=False, default=0)
    cutoff_time = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24, null=True, blank=False, default=None,)

    # Delivery days
    delivery_days = models.PositiveIntegerField(null=True, blank=False)
    delivery_everyday = models.BooleanField(_("everyday delivery"), null=True, default=False)

    # Delivery hours
    delivery_from_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24, null=True, default=None)
    delivery_to_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24, null=True, default=None)

    def __str__(self):
        return f"{self.shop.shop_name}"

    @property
    def get_delivery_days(self):
        single_days = [1, 2, 4, 8, 16, 32, 64]
        if self.delivery_days in single_days:
            days = WEEKDAYS.get_selected_values(127)[single_days.index(self.delivery_days)]
            return days
        else:
            days = WEEKDAYS.get_selected_values(self.delivery_days)
        # days = days.split(', ')
        shortcut = []
        if self.delivery_everyday == True:
            return "Everyday"
        else:
            if len(days) > 4:
                for day in days:
                    shortcut.append(day[:3])

                all_days = 'Mon, Tue, Wed, Thu, Fri, Sat, Sun'
                shortcut_str = ', '.join(shortcut)
                if shortcut_str == all_days:
                    return "Everyday"
                elif len(shortcut) == 6:
                    for day in all_days:
                        if day not in shortcut:
                            exception = day
                    return "Everyday except " + str(exception)
                else:
                    straight = True
                    for index, day in enumerate(shortcut):
                        # print(shortcut)
                        # print(index, day)
                        if day != shortcut[index]: straight = False
                    if straight == True: return f"{shortcut[0]} - {shortcut[-1]}"
                    else: return shortcut_str
            else:
                for day in days:
                    shortcut.append(day)
                
                shortcut_str = ', '.join(shortcut)
                if shortcut_str == 'Saturday, Sunday':
                    return "Weekends Only"
                else:
                    straight = True
                    for day in shortcut:
                        index = shortcut.index(day)
                        if day[index] != shortcut[index]: straight = False
                    if straight == True: return f"{shortcut[0]} - {shortcut[-1]}"
                    else: return shortcut_str

    @property
    def get_available_days(self):
        single_days = [1, 2, 4, 8, 16, 32, 64]
        if self.delivery_days in single_days:
            delivery_schedule = WEEKDAYS.get_selected_values(127)[single_days.index(self.delivery_days)]
            available_days = []
            available_days.append(delivery_schedule)
        else:
            delivery_schedule = WEEKDAYS.get_selected_values(self.delivery_days)
            delivery_schedule = ', '.join(delivery_schedule)

            # Getting available days for datepicker
            available_days = []
            if 'Sunday' in delivery_schedule: available_days.append(0)
            if 'Monday' in delivery_schedule: available_days.append(1)
            if 'Tuesday' in delivery_schedule: available_days.append(2)
            if 'Wednesday' in delivery_schedule: available_days.append(3)
            if 'Thursday' in delivery_schedule: available_days.append(4)
            if 'Friday' in delivery_schedule: available_days.append(5)
            if 'Saturday' in delivery_schedule: available_days.append(6)
        return available_days

    @property
    def get_min_delivery_date(self):
        # If shop used cutoff days
        if self.cutoff_days != None:
            # Getting the minimum date available for delivery
            highest_days = self.cutoff_days
            initial_date = datetime.today() + timedelta(days=highest_days)
            
            if self.cutoff_time != None:
                now = datetime.now().hour
                if now > self.cutoff_time:
                    try: initial_date = initial_date + timedelta(days=1)
                    except: initial_date = initial_date

            from_hour = self.delivery_from_hour
            min_date = datetime(initial_date.year, initial_date.month, initial_date.day, from_hour, 0, 0)
        # If shop used cutoff time
        elif self.cutoff_days == None and self.cutoff_time != None:
            # Getting the minimum date available for delivery
            # Get current time
            now = datetime.now()
            current_hour = int(now.hour) + 1
            # If current time is greater than cutoff time
            if current_hour > self.cutoff_time:
                initial_date = datetime.today() + timedelta(days=1)
                from_hour = self.delivery_from_hour
                min_date = datetime(initial_date.year, initial_date.month, initial_date.day, from_hour, 0, 0)
            # Else, turn from hour into the current datetime + time today
            else:
                initial_date = datetime.today()
                # If current time is greater than the delivery_from_hour:
                from_hour = self.delivery_from_hour
                if current_hour > from_hour:
                    min_date = datetime(initial_date.year, initial_date.month, initial_date.day, current_hour, 0, 0)
                else:
                    min_date = datetime(initial_date.year, initial_date.month, initial_date.day, from_hour, 0, 0)
        return min_date

    @property
    def get_opening_time(self):
        value = [hour for hour in self.HOUR_OF_DAY_24 if hour[0] == self.delivery_from_hour][0][1]
        return value

    @property
    def get_cutoff_time(self):
        value = [hour for hour in self.HOUR_OF_DAY_24 if hour[0] == self.delivery_to_hour][0][1]
        return value

# General Shop Settings
class ShopDeliverySettings(BaseAccountsModel):
    shop = models.OneToOneField(ShopInformation, related_name='shop_delivery_settings', on_delete=models.CASCADE, null=True, blank=True)
    seller_books = models.BooleanField(_("seller books"), blank=True, null=True, default=False)
    buyer_books = models.BooleanField(_("buyer books"), blank=True, null=True, default=False)
    buyer_picks_up = models.BooleanField(_("buyer picks up"), blank=True, null=True, default=False)

    line1 = models.CharField(_("delivery line1"), null=True, blank=True, max_length=155, default="Address Line 1")
    line2 = models.CharField(_("delivery line2"), null=True, blank=True, max_length=155, default="Address Line 2")
    city = models.CharField(_("delivery city"), choices=Address.CITIES, null=True, blank=True, max_length=55, default=Address.CITIES[0])
    province = models.CharField(_("delivery province"), choices=Address.PROVINCES, null=True, blank=True, max_length=55, default=Address.PROVINCES[0])
    postal_code = models.CharField(_("delivery postal code"), null=True, max_length=4, blank=True, validators=[only_int], default="0000")

    mobile_number = models.BooleanField(_("request customer mobile number"), blank=True, null=True, default=True)
    facebook = models.BooleanField(_("request customer mobile number"), blank=True, null=True, default=False)
    instagram = models.BooleanField(_("request customer mobile number"), blank=True, null=True, default=False)

    def __str__(self):
        return f"{self.shop.shop_name}"

    @property
    def get_delivery_options(self):
        delivery_options = []
        if self.seller_books == True:
            delivery_options.append(("Merchant Booking", _("Seller books the courier")))
        if self.buyer_books == True:
            delivery_options.append(("Customer Booking", _("I will book the courier")))
        if self.buyer_picks_up == True:
            delivery_options.append(("Customer Pick Up", _("Pick-up from shop address")))
        return delivery_options

# Shop Logo
class AvatarSmall(ImageSpec):
    processors = [ResizeToFit(24, 24)]
    format = 'JPEG'
    options = {'quality': 80}

class AvatarMedium(ImageSpec):
    processors = [ResizeToFit(64, 64)]
    format = 'JPEG'
    options = {'quality': 80}

class AvatarLarge(ImageSpec):
    processors = [ResizeToFit(80, 80)]
    format = 'JPEG'
    options = {'quality': 80}

class ShopLogo(ImageSpec):
    processors = [ResizeToFit(120, 120)]
    format = 'JPEG'
    options = {'quality': 100}

class ShopThumbnail(ImageSpec):
    processors = [ResizeToFill(1200, 500)]
    form = 'JPEG'
    options = {'quality': 80}

class ShopFormHeader(ImageSpec):
    processors = [ResizeToFill(1000, 240)]
    form = 'JPEG'
    options = {'quality': 100}

register.generator('account:small_avatar', AvatarSmall)
register.generator('account:medium_avatar', AvatarMedium)
register.generator('account:large_avatar', AvatarLarge)
register.generator('account:shop_logo', ShopLogo)
register.generator('account:shop_thumbnail', ShopThumbnail)
register.generator('account:shop_header', ShopFormHeader)

class ShopLogo(BaseAccountsModel):
    shop = models.OneToOneField(ShopInformation, related_name='shop_logo', on_delete=models.CASCADE, null=True, blank=True)
    logo = models.ImageField(upload_to='%Y/%m/%d/shop_logos', null=True, blank=True)

    def __str__(self):
        return f"URL: {self.shop.shop_name}"

class ShopBranding(BaseAccountsModel):
    shop = models.OneToOneField(ShopInformation, related_name='shop_branding', on_delete=models.CASCADE, null=True, blank=True)
    thumbnail = models.ImageField(upload_to='%Y/%m/%d/shop_thumbnails', null=True, blank=True)
    description = models.CharField(_("shop brand description"), max_length=140, null=True, blank=True)

    def __str__(self):
        return f"{self.shop.shop_name}"

# CUSTOMERS
# class CustomerManager(models.Manager):
#     def get_queryset(self, *args, **kwargs):
#         return super().get_queryset(*args, **kwargs).filter(type=User.Types.CUSTOMER)

# class Customer(User):
#     base_type = User.Types.CUSTOMER
#     objects = CustomerManager()

#     class Meta:
#         proxy = True

#     def save(self, *args, **kwargs):
#         if not self.pk:
#             self.type = User.Types.CUSTOMER
#         return super().save(*args, **kwargs)

class CustomerInformation(BaseAccountsModel):
    customer = models.ForeignKey(
        User, related_name="customer_info", on_delete=models.CASCADE, null=True
    )

    customer_name = models.CharField(_("customer name"), max_length=150, null=True, blank=False)
    customer_email = models.EmailField(_("email"), 
        max_length=150, 
        null=True,
        blank=True
    )
    customer_contact_number = models.CharField(_("mobile number"), max_length=15, null=True, blank=False, validators=[only_int])
    customer_username = models.CharField(_("customer username"), 
        max_length=150, 
        null=True, 
        unique=True, 
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits, and @/./+/-/_ only."
        ),
        error_messages={"unique": _("A user with that username already exists."),},
    )
    xendit_customer_id = models.CharField(_("xendit customer id"), max_length=155, null=True, blank=True)
    customer_profile = models.BooleanField(_('customer profile'), null=True, default=False)
    
    instagram = models.CharField(_("instagram link"), null=True, blank=True, default="", max_length=255)
    facebook = models.CharField(_("facebook link"), null=True, blank=True, default="", max_length=255)

    def __str__(self):
        return f"{self.customer_username} - {self.customer_name} ({self.customer_contact_number})"

class CustomerProfilePic(BaseAccountsModel):
    customer = models.OneToOneField(CustomerInformation, related_name='customer_dp', on_delete=models.CASCADE, null=True, blank=True)
    profile_pic = models.ImageField(upload_to='%Y/%m/%d/customer_profile_pics', null=True, blank=True)

    def __str__(self):
        return f"URL: {self.logo.url}"