from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from django.forms import ValidationError as formError
from django.utils.translation import gettext_lazy as _
 
from django.core.validators import URLValidator
from pyisemail import is_email
from ph_geography.models import Province, Municipality
# from ph_geography.fixtures import municipalities.json, provinces.json

from accounts.models import *
from accounts.validators import *

from django.contrib.auth.hashers import make_password

# Create User
class CreateUserForm(UserCreationForm):
    password1=forms.CharField(widget=forms.PasswordInput(attrs={
                            'class': 'input default', 
                            'placeholder': 'Enter Password'}))

    password2=forms.CharField(widget=forms.PasswordInput(attrs={
                            'class': 'input default', 
                            'placeholder': 'Confirm Password'}))

    class Meta: 
        model = User
        fields = ('email',)

        widgets = {
            'email': forms.fields.EmailInput(attrs={
                'class':'input default',
                'placeholder': 'Email'})
        }

        def __init__(self, *args, **kwargs):
            super(CreateUserForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        cleaned_data = super().clean()
        email = cleaned_data.get("email")
        if not is_email(email):
            raise formError(_('Invalid Email.'), code='invalid_email')
        return email

    def validate_password(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise formError(
                self.error_messages['Passwords do not match.'],
                code='password_mismatch',
            )
        return True

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class NewPasswordForm(forms.Form):
    old_password=forms.CharField(widget=forms.PasswordInput(attrs={
                            'class': 'input default subtitle', 
                            'placeholder': 'Enter Old Password'}))

    new_password1=forms.CharField(widget=forms.PasswordInput(attrs={
                            'class': 'input default subtitle', 
                            'placeholder': 'Enter New Password'}))

    new_password2=forms.CharField(widget=forms.PasswordInput(attrs={
                            'class': 'input default subtitle', 
                            'placeholder': 'Confirm New Password'}))

    def validate_old_pass(self, user):
        old_pass = self.cleaned_data.get("old_password")
        new_pass_1 = self.cleaned_data.get("new_password1")

        if not user.check_password(old_pass):
            self.add_error('old_password', _('Old password is incorrect.'))
            return False

        if old_pass == new_pass_1:
            self.add_error('old_password', _("You're already using this password."))
            return False
        return True

    def validate_new_pass(self):
        new_pass_1 = self.cleaned_data.get("new_password1")
        new_pass_2 = self.cleaned_data.get("new_password2")

        if new_pass_1 and new_pass_2 and new_pass_1 != new_pass_2:
            self.add_error('new_password1', _("New passwords do not match."))
            return False
        return True

# For Delivery Dates
# See here for more information: https://stackoverflow.com/questions/19645227/django-create-multiselect-checkbox-input
# https://stackoverflow.com/questions/3663898/representing-a-multi-select-field-for-weekdays-in-a-django-model
class BinaryWidget(forms.CheckboxSelectMultiple):
    def value_from_datadict(self, data, files, name):
        value = super(BinaryWidget, self).value_from_datadict(data, files, name)
        if name == 'delivery_days':
            value = sum([2**(int(x)-1) for x in value])
        return value

class ShopInformationForm(forms.Form):
    # Shop Details
    shop_name = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Shop Name'}),)
    shop_username = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Shop Quicklink Username'}),)
    shop_contact_number = forms.CharField(label='',
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Contact Number'}),)
    shop_email = forms.EmailField(label='',
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Display Email Address'}),)

    # shop_address
    line1=forms.CharField(label='', 
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Address Line 1',
        'autocomplete': 'off'}))
    line2=forms.CharField(label='', 
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Address Line 2',
        'autocomplete': 'off'}))
    city=forms.ChoiceField(label='', 
        required=False,
        widget=forms.Select(attrs={
        'class': 'wide input default subtitle', 
        'placeholder': 'Province',
        'autocomplete': 'off'}),
        choices=[(x.name.title(), x.name.title()) for x in Municipality.objects.filter(province=Province.objects.get(name="METRO MANILA")).order_by('name')])
    province=forms.ChoiceField(label='',
        required=False,
        widget=forms.Select(attrs={
        'class': 'wide input default subtitle', 
        'placeholder': 'Province',
        'autocomplete': 'off'}),
        choices=[(x.name.title(), x.name.title()) for x in Province.objects.all().order_by('name')])
    postal_code=forms.CharField(label='',
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Postal Code',
        'autocomplete': 'off'}))
    
    # shop_links
    instagram=forms.URLField(label='',
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Instagram Link'}),
        required=False)
    facebook=forms.URLField(label='',
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Facebook Link'}),
        required=False)
    twitter=forms.URLField(label='',
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Twitter Link'}),
        required=False)

    def __init__(self, *args, **kwargs):
        shop = kwargs.pop("shop", None)
        self.shop = shop
        super(ShopInformationForm, self).__init__(*args, **kwargs)
        self.fields['province'].initial = Province.objects.get(name="METRO MANILA").name.title()
        if hasattr(shop, 'shop_info'):
            self.fields['shop_name'].initial = shop.shop_info.shop_name
            self.fields['shop_contact_number'].initial = shop.shop_info.shop_contact_number[3:]
            self.fields['shop_username'].initial = shop.shop_info.shop_username
            self.fields['shop_email'].initial = shop.shop_info.shop_email
        if Address.objects.filter(user=shop, default=True).exists():
            shop_address = Address.objects.filter(user=shop, default=True).first()
            self.fields['line1'].initial = shop_address.line1
            self.fields['line2'].initial = shop_address.line2
            self.fields['province'].initial = shop_address.province
            self.fields['city'].initial = shop_address.city
            self.fields['postal_code'].initial = shop_address.postal_code
        if hasattr(shop, 'user_links'):
            self.fields['instagram'].initial = shop.user_links.instagram
            self.fields['facebook'].initial = shop.user_links.facebook
            self.fields['twitter'].initial = shop.user_links.twitter


    def clean_shop_name(self):
        cleaned_data = super().clean()
        name = cleaned_data.get("shop_name")
        if len(list(name)) > 30:
            raise formError(_('Shop name should be no more than 30 characters.'), code='character_length')
        return name

    def clean_shop_contact_number(self):
        cleaned_data = super().clean()
        contact_number = cleaned_data.get("shop_contact_number")
        if not contact_number:
            return None
        else:
            contact_number = "+63" + contact_number.replace(" ", "")
            contact_number_list = contact_number.strip().split()
            if len(contact_number_list) > 12:
                raise formError(_('Please input a valid contact number.'), code='character_length')
            return contact_number

    def clean_shop_username(self):
        cleaned_data = super().clean()
        username = cleaned_data.get("shop_username")
        url_form_field = forms.URLField()
        dev_url = "http://shops.quicklink.local:8000/" + str(username) + "/"
        prod_url = "https://shops.quicklink.ph/" + str(username) + "/"
        try:
            try:
                url = url_form_field.clean(dev_url)
            except:
                url = url_form_field.clean(prod_url)
        except:
            raise formError(_('Only digits (0-9), letters (A-Z, a-z), and a few special characters ("-", ".", "_", "~") are accepted for shop usernames.'), code='invalid_format')
        
        if ShopInformation.objects.filter(shop_username=username).exclude(user=self.shop).exists():
            raise formError(_('A shop with that username already exists'), code='unique')
        return username

    def clean_shop_email(self):
        cleaned_data = super().clean()
        email = cleaned_data.get("shop_email")
        if not email:
            return None
        elif not is_email(email):
            raise formError(_('Email not found.'), code='invalid_email')
        return email

# class ShopLogoForm(forms.ModelForm):
#     class Meta:
#         model = ShopLogo
#         fields = ['logo']
#         required_fields = ['logo']

# class ShopBrandingForm(forms.ModelForm):
#     class Meta:
#         model = ShopBranding
#         fields = ['thumbnail', 'description']
#         required_fields = ['thumbnail' 'description']

#         widgets = {
#             'description': forms.Textarea(attrs={
#                 'class':'input large-input default subtitle',
#                 'placeholder': 'Describe your shop in 1-2 sentences!'}),
#         }

class ShopBrandingForm(forms.Form):
    logo = forms.ImageField(label='', required=False)
    thumbnail = forms.ImageField(label='', required=False)
    description = forms.CharField(label='',
        required=False,
        widget=forms.Textarea(attrs={
            'class':'input large-input default subtitle',
            'placeholder': 'Describe your shop in 1-2 sentences!'}),
        )

    def __init__(self, *args, **kwargs):
        shop = kwargs.pop("shop", None)
        super(ShopBrandingForm, self).__init__(*args, **kwargs)
        if hasattr(shop, 'shop_logo'):
            self.fields['logo'].initial = shop.shop_logo.logo
        if hasattr(shop, 'shop_branding'):
            self.fields['thumbnail'].initial = shop.shop_branding.thumbnail
            self.fields['description'].initial = shop.shop_branding.description

class ShopSettingsForm(forms.Form):
    ## Customer Offset
    # shop.shop_info.shop_general_settings.shop_customer_fees
    shop_customer_fees = forms.BooleanField(required=False, widget=forms.CheckboxInput())

    ## Order Cut-Off
    # shop.shop_general_settings.cutoff_days
    cutoff_days = forms.IntegerField(label='',
        required=False,
        widget=forms.fields.NumberInput(attrs={
        'class':'input default disabled subtitle',
        'placeholder': '0'}),
    )
    
    # shop.shop_general_settings.cutoff_time
    cutoff_time = forms.ChoiceField(widget=forms.Select(
        attrs={'class': 'wide input default subtitle'}),
        choices=ShopGeneralSettings.HOUR_OF_DAY_24,
        required=False
    )

    ## Delivery Schedule
    # shop.shop_general_settings.delivery_days
    delivery_days = forms.IntegerField(label='',
        widget=BinaryWidget(choices=(
        ('1', 'Monday'), ('2', 'Tuesday'), ('3', 'Wednesday'),
        ('4', 'Thursday'), ('5', 'Friday'), ('6', 'Saturday'),
        ('7', 'Sunday')
    )))

    # shop.shop_general_settings.delivery_from_hour
    from_hour = forms.ChoiceField(widget=forms.Select(
        attrs={'class': 'wide input default subtitle'}),
        choices=ShopGeneralSettings.HOUR_OF_DAY_24,
        initial=ShopGeneralSettings.HOUR_OF_DAY_24[0]
    )

    # shop.shop_general_settings.delivery_from_hour
    to_hour = forms.ChoiceField(widget=forms.Select(
        attrs={'class': 'wide input default subtitle'}),
        choices=ShopGeneralSettings.HOUR_OF_DAY_24,
        initial=ShopGeneralSettings.HOUR_OF_DAY_24[6]
    )

    def __init__(self, *args, **kwargs):
        request = kwargs.pop("request", None)
        super(ShopSettingsForm, self).__init__(*args, **kwargs)
        self.fields['cutoff_days'].widget.attrs.update({'min': 0})
        if request: shop = request.user
        else: shop = None
        if hasattr(shop, "shop_info"):
            shop_info = shop.shop_info
            if hasattr(shop_info, "shop_general_settings"):
                shop_settings = shop_info.shop_general_settings
                self.fields['shop_customer_fees'].initial = shop_settings.shop_customer_fees
                self.fields['delivery_days'].initial = shop_settings.delivery_days
                self.fields['from_hour'].initial = shop_settings.delivery_from_hour
                self.fields['to_hour'].initial = shop_settings.delivery_to_hour
                self.fields['cutoff_days'].initial = shop_settings.cutoff_days
                self.fields['cutoff_time'].initial = shop_settings.cutoff_time

class DeliverySettingsForm(forms.Form):
    # shop.shop_delivery_settings
    seller_books=forms.BooleanField(required=False,
    widget=forms.CheckboxInput(attrs={'class':'js-toggle-btns'}))
    buyer_books=forms.BooleanField(required=False,
    widget=forms.CheckboxInput(attrs={'class':'js-toggle-btns'}))
    buyer_picks_up=forms.BooleanField(required=False,
    widget=forms.CheckboxInput(attrs={'class':'js-toggle-btns'}))

    # shop.shop_info.shop_delivery_fees
    # shop_delivery_fees = forms.CharField(label='',
    #     required=False,
    #     widget=forms.fields.TextInput(attrs={
    #     'class':'input default subtitle',
    #     'placeholder': 'Fixed Delivery Fees'}),
    # )

    # shop.shop_delivery_settings
    line1=forms.CharField(label='', required=False,
                        widget=forms.fields.TextInput(attrs={
                        'class': 'input default subtitle', 
                        'placeholder': 'Address Line 1'}))
    line2=forms.CharField(label='', required=False,
                        widget=forms.fields.TextInput(attrs={
                        'class': 'input default subtitle', 
                        'placeholder': 'Address Line 2'}))
    city=forms.ChoiceField(label='', 
                        widget=forms.Select(attrs={
                        'class': 'wide input default subtitle', 
                        'placeholder': 'Province',
                        'autocomplete': 'off'}),
                        choices=[(x.name.title(), x.name.title()) for x in Municipality.objects.filter(province=Province.objects.get(name="METRO MANILA")).order_by('name')])
    province=forms.ChoiceField(label='', 
                        widget=forms.Select(attrs={
                        'class': 'wide input default subtitle', 
                        'placeholder': 'Province',
                        'autocomplete': 'off'}),
                        choices=[(x.name.title(), x.name.title()) for x in Province.objects.all().order_by('name')])
    postal_code=forms.CharField(label='', required=False,
                        widget=forms.fields.TextInput(attrs={
                        'class': 'input default subtitle', 
                        'placeholder': 'Postal Code'}))

    mobile_number=forms.BooleanField(required=False,
    widget=forms.CheckboxInput(attrs={'class':'js-toggle-btns'}))
    facebook=forms.BooleanField(required=False,
    widget=forms.CheckboxInput(attrs={'class':'js-toggle-btns'}))
    instagram=forms.BooleanField(required=False,
    widget=forms.CheckboxInput(attrs={'class':'js-toggle-btns'}))

    def clean_delivery_fees(self):
        fees = self.cleaned_data['shop_delivery_fees']
        try:
            response = fees.split()
            numbers = []
            for num in response:
                try:
                    if num.isdigit() or isinstance(float(num), float):
                        numbers.append(num)
                except: continue
            fees = float(''.join(numbers))
        except:    
            raise formError(_('Invalid format in delivery fees.'), code='invalid_format')
        return fees

    def __init__(self, *args, **kwargs):
        shop = kwargs.pop("shop", None)
        super(DeliverySettingsForm, self).__init__(*args, **kwargs)
        if hasattr(shop, 'shop_info'):
            shop_info = shop.shop_info
            if hasattr(shop_info, 'shop_delivery_settings'):
                delivery_settings = shop_info.shop_delivery_settings
                self.fields['seller_books'].initial = delivery_settings.seller_books
                self.fields['buyer_books'].initial = delivery_settings.buyer_books
                self.fields['buyer_picks_up'].initial = delivery_settings.buyer_picks_up
                self.fields['line1'].initial = delivery_settings.line1
                self.fields['line2'].initial = delivery_settings.line2
                self.fields['province'].initial = delivery_settings.province
                self.fields['city'].initial = delivery_settings.city
                self.fields['postal_code'].initial = delivery_settings.postal_code
                self.fields['mobile_number'].initial = delivery_settings.mobile_number
                self.fields['facebook'].initial = delivery_settings.facebook
                self.fields['instagram'].initial = delivery_settings.instagram

class ShopAccountForm(forms.ModelForm):
    class Meta:
        model = BankAccount
        fields = ['bank_name', 'cardholder_name', 'account_number']
        required_fields = ['bank_name', 'cardholder_name', 'account_number']

        widgets = {
            'bank_name': forms.Select(attrs={
                'class':'wide input default subtitle'}),
            'cardholder_name': forms.fields.TextInput(attrs={
                'class':'input default subtitle js-display-inputs',
                'placeholder': 'Account Name'}),
            'account_number': forms.fields.TextInput(attrs={
                'class':'input default subtitle js-display-inputs',
                'placeholder': 'Account Number'}),
        }

    def __init__(self, *args, **kwargs):
        super(ShopAccountForm, self).__init__(*args, **kwargs)
        selected_choices = [
            "DC/CC"
        ]
        self.fields['bank_name'].choices = [(k, v) for k, v in BankAccount.BANKS
                                                if k not in selected_choices]

class ShopFeedbackForm(forms.ModelForm):
    class Meta:
        model = MerchantFeedback
        fields = ['name', 'email', 'shop_name', 'feedback']
        required_fields = ['name', 'email', 'shop_name', 'feedback']

        widgets = {
            'name': forms.fields.TextInput(attrs={
                'class':'input default subtitle',
                'placeholder': 'Name'}),
            'email': forms.fields.TextInput(attrs={
                'class':'input default subtitle',
                'placeholder': 'Email Address'}),
            'shop_name': forms.fields.TextInput(attrs={
                'class':'input default subtitle',
                'placeholder': 'Shop Name'}),
            'feedback': forms.Textarea(attrs={
                'class':'large-input default subtitle',
                'placeholder': 'Type your feedback here'}),
        }

    def __init__(self, *args, **kwargs):
        super(ShopFeedbackForm, self).__init__(*args, **kwargs)

class CustomerInformationForm(forms.Form):
    # Shop Details
    customer_name = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Name'}),)
    customer_contact_number = forms.CharField(label='',  
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Mobile Number'}),)
    customer_username = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Quicklink Username'}),)
    
    # shop_links
    instagram=forms.URLField(label='',
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Instagram'}),
        required = False)
    facebook=forms.URLField(label='',
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Facebook'}),
        required = False)
    twitter=forms.URLField(label='',
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Twitter'}),
        required = False)

    def validate_name(self):
        cleaned_data = super().clean()
        name = cleaned_data.get("customer_name")
        if len(list(name)) > 30:
            raise formError(_('Shop name should be no more than 30 characters.'), code='character_length')
        return name

    def validate_contact_number(self):
        cleaned_data = super().clean()
        contact_number = cleaned_data.get("customer_contact_number")
        contact_number = "+63" + contact_number.replace(" ", "")
        contact_number_list = contact_number.strip().split()
        if len(contact_number_list) > 12:
            raise formError(_('Please input a valid contact number.'), code='character_length')
        return contact_number

    def validate_username(self):
        cleaned_data = super().clean()
        username = cleaned_data.get("customer_username")
        url_form_field = forms.URLField()
        dev_url = "http://127.0.0.1:8000/" + str(username) + "/"
        prod_url = "https://shops.quicklink.ph/" + str(username) + "/"
        try:
            try:
                url = url_form_field.clean(dev_url)
            except:
                url = url_form_field.clean(prod_url)
        except:
            raise formError(_('Only digits (0-9), letters (A-Z, a-z), and a few special characters ("-", ".", "_", "~") are accepted for shop usernames.'), code='invalid_format')

        try:
            customer_username_exists = CustomerInformation.objects.get(customer_username=username)
        except:
            customer_username_exists = False

        if customer_username_exists != False:
            raise formError(_("A user with that username already exists."))
        return username

    def get_validated_data(self):
        return {
            'customer_name': self.validate_name(),
            'customer_contact_number': self.validate_contact_number(),
            'customer_username': self.validate_username()
        }

class CustomerAddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = ["line1",
                "line2",
                "city",
                "province",
                "postal_code"]
        required_fields = ["line1",
                "city",
                "province",
                "postal_code"]

        widgets = {
            'line1': forms.fields.TextInput(attrs={
                'class':'input default subtitle',
                'placeholder': 'Address Line 1'}),
            'line2': forms.fields.TextInput(attrs={
                    'class':'input default subtitle',
                    'placeholder': 'Address Line 2'}),
            'city': forms.Select(attrs={
                    'class':'wide input default subtitle',
                    'placeholder': 'City'}),
            'province': forms.Select(attrs={
                    'class':'wide input default subtitle',
                    'placeholder': 'Province'}),
            'postal_code': forms.fields.TextInput(attrs={
                    'class':'input default subtitle',
                    'placeholder': 'Postal Code'}),
        }

        choices = {
            'city': Address.CITIES,
            'province': Address.PROVINCES
        }

class CustomerAccountForm(forms.ModelForm):
    class Meta:
        model = BankAccount
        fields = [
            "bank_name",
            "cardholder_name",
            "account_number",
            "exp_date",
            "cvv"
        ]
        required_fields = ["cardholder_name", "account_number"]

        widgets = {
            'bank_name': forms.Select(attrs={
                'class':'wide input default subtitle'}),
            'cardholder_name': forms.fields.TextInput(attrs={
                'class':'input default subtitle',
                'placeholder': 'Ex. Juan Dela Cruz'}),
            'account_number': forms.fields.TextInput(attrs={
                'class':'input default subtitle',
                'placeholder': 'Ex. 1234 5678 9876 5432'}),
            'exp_date': forms.fields.TextInput(attrs={
                    'class':'input default subtitle',
                    'placeholder': 'MM/YY'}),
            'cvv': forms.PasswordInput(attrs={
                    'class':'input default subtitle',
                    'placeholder': '***'}),
        }

class NotificationsForm(forms.ModelForm):
    class Meta:
        model = CustomerNotification
        fields = ['sms', 'email']
        widgets = {
            'sms':forms.CheckboxInput(),
            'email':forms.CheckboxInput()
        }
        labels = {
            'sms': _('Receive SMS updates'),
            'email': _('Receive email updates')
        }