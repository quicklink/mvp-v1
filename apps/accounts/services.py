import json
from django.contrib import messages

from sendgrid import SendGridAPIClient
from .selectors import get_sendgrid_api_key

def send_confirmation_email(to_email, user_id, link, request):
    sg_data = {
        "from": {
            "email":"josh@quicklink.ph"
        },
        "personalizations": [
            {
                "to":[
                    {
                        "email":to_email
                    },
                ],
                "dynamic_template_data": {
                    "user_id":user_id,
                    "link":link
                }
            }
        ],
        "template_id":"d-a22e00ee23694faeae56b21d10afbf7a"
    }
    sg_data = json.loads(json.dumps(sg_data))
    sg_api_key = get_sendgrid_api_key()
    try:
        sg = SendGridAPIClient(api_key=sg_api_key)
        response = sg.client.mail.send.post(request_body=sg_data)
    except Exception as e:
        response = None
        messages.error(request, "Can't send verification email. Send an email to no-reply@quicklink.ph")
    return response