from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField, UserChangeForm
# from hijack_admin.admin import HijackUserAdminMixin
from .models import *
import nested_admin

class ShopLogoInline(nested_admin.NestedStackedInline):
    model = ShopLogo

class ShopBrandingInline(nested_admin.NestedStackedInline):
    model = ShopBranding

class ShopGeneralSettingsInline(nested_admin.NestedStackedInline):
    model = ShopGeneralSettings

class ShopDeliverySettingsInline(nested_admin.NestedStackedInline):
    model = ShopDeliverySettings

class AddressInline(admin.StackedInline):
    model = Address

class BankAccountInline(admin.StackedInline):
    model = BankAccount

class SocialMediaLinkInline(admin.StackedInline):
    model = SocialMediaLink

class ShopInformationInline(nested_admin.NestedStackedInline):
    model = ShopInformation
    inlines = (ShopLogoInline, ShopBrandingInline, ShopGeneralSettingsInline, ShopDeliverySettingsInline)

class ShopInformationAdmin(nested_admin.NestedModelAdmin):
    model = ShopInformation
    inlines = (ShopLogoInline, ShopBrandingInline, ShopGeneralSettingsInline, ShopDeliverySettingsInline)

    ordering = ('shop_name', 'shop_username')
    list_display = (
        'shop_name', 
        'shop_username', 
        'shop_contact_number', 
        'shop_email',
        'get_bank_account'
        # "hijack_field"
    )
    list_filter = ("shop_name", )
    search_fields = ('shop_name', 'shop_username')
    
    def get_bank_account(self, obj):
        if BankAccount.objects.filter(user=obj.user).exists():
            bank_account = BankAccount.objects.get(user=obj.user, saved=True)
            return f"[{bank_account.bank_name}] {bank_account.account_number}"
        # elif hasattr(obj, 'customer_info') and obj.customer_info.customer_name:
        #     return obj.customer_info.customer_name
        else:
            return "No information available."

    get_bank_account.short_description = 'Bank Account'
    get_bank_account.admin_order_field = 'shop.user_account'

# class MyUserChangeForm(UserChangeForm):
#     class Meta(UserChangeForm.Meta):
#         model = User

class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'role')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'role', 'is_active', 'is_staff')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

# class CustomUserAdmin(UserAdmin, HijackUserAdminMixin):
class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    inlines = (ShopInformationInline, AddressInline, BankAccountInline, SocialMediaLinkInline)
    ordering = ('email', 'device_id', 'role')
    list_display = (
        "email",
        "device_id",
        "get_name",
        "role",
        "is_active",
        "is_staff",
        # "hijack_field"
    )
    list_filter = ("role", "is_active", "is_staff", "is_superuser")
    
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Roles', {'fields': ('role','groups')}),
        ('Permissions', {'fields': ('is_staff', 'is_superuser')})
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('role', 'email', 'password1', 'password2'),
        }),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

    def get_name(self, obj):
        if hasattr(obj, 'shop_info') and obj.shop_info.shop_name:
            return obj.shop_info.shop_name
        # elif hasattr(obj, 'customer_info') and obj.customer_info.customer_name:
        #     return obj.customer_info.customer_name
        else:
            return "No information available."

    get_name.short_description = 'Name'
    get_name.admin_order_field = 'shop__info.shop_name'


admin.site.register(User, CustomUserAdmin)

admin.site.register(Address)
admin.site.register(BankAccount)
admin.site.register(SocialMediaLink)

admin.site.register(ShopInformation, ShopInformationAdmin)
admin.site.register(ShopLogo)
admin.site.register(ShopBranding)
admin.site.register(ShopGeneralSettings)
admin.site.register(ShopDeliverySettings)
admin.site.register(MerchantNotification)
admin.site.register(MerchantFeedback)

admin.site.register(CustomerInformation)
admin.site.register(CustomerProfilePic)
admin.site.register(CustomerNotification)