import os
import environ
from django.conf import settings

import shortuuid
from accounts.models import User, BankAccount

if not settings.LOCAL_DEV:
    import subprocess
    import ast
    
    def get_environ_vars():
        completed_process = subprocess.run(
            ['/opt/elasticbeanstalk/bin/get-config', 'environment'],
            stdout=subprocess.PIPE,
            text=True,
            check=True
        )
        return ast.literal_eval(completed_process.stdout)
        
def get_sendgrid_api_key():
    if not settings.LOCAL_DEV:
        if "SENDGRID_API_KEY" in os.environ:
            sg_api_key = os.environ.get("SENDGRID_API_KEY")
        else:
            env_vars = get_environ_vars()
            sg_api_key = env_vars.get("SENDGRID_API_KEY")
    else:
        env = environ.Env()
        env.read_env()
        sg_api_key = env("SENDGRID_API_KEY")
    return sg_api_key