from django import template
from accounts.models import User, ShopInformation, CustomerInformation

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_username(user):
    # Check if Shop Information query exists
    try: shop = ShopInformation.objects.get(user=user)
    except: shop = None

    # Check if Customer Information query exists
    try: customer = CustomerInformation.objects.get(customer=user, customer_profile=True)
    except: customer = None

    if user.is_authenticated:
        if shop != None:
            return shop.shop_username
        elif customer != None:
            return customer.customer_username
    else:
        return False

@register.filter
def get_delivery_options(user):
    # Check if Shop Information query exists
    try: shop = ShopInformation.objects.get(user=user)
    except: shop = None

    if shop and hasattr(shop, "shop_delivery_settings"):
        settings = shop.shop_delivery_settings
        if settings.seller_books and settings.buyer_books:
            return True
        elif settings.seller_books and settings.buyer_picks_up:
            return True
        elif settings.buyer_books and settings.buyer_picks_up:
            return True
        else: 
            return False
    else: 
        return False