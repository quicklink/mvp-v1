from django import template

register = template.Library()

@register.filter
def growth_rate(value):
    growth_rate = "{:.0%}".format(value)
    return growth_rate