from django.test import TestCase
from ..models import User

class AccountCreationTestCase(TestCase):
    with open("../fixtures/users.json") as data:
        fixtures = [json.load(data)]