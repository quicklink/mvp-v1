from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('welcome/', views.verified_merchant, name='verified-invite'),
    path('invalid-invite/', views.invalid_invite_merchant, name='invalid-invite'),
    path('uninvited/', views.uninvited_merchant, name='uninvited'),
    path('register/', views.register_merchant, name='register'),
    path('login/', views.login_merchant, name='login'),
    path('logout/', views.logout_merchant, name='logout'),
    path('register/verification/<str:user_id>/', views.email_verification, name='email-verification'),
    path('register/confirmation/<uidb64>/<token>/', views.email_confirmation, name='email-confirmation'),
    path('register/shop/', views.register_shop_information, name='merchant-add-shop'),
    path('register/logo/', views.register_shop_logo, name='merchant-add-logo'),
    path('register/settings/', views.register_shop_settings, name='merchant-add-settings'),
    path('register/deliveries/', views.register_shop_deliveries, name='merchant-add-delivery'),
    path('register/payment/', views.register_shop_account, name='merchant-add-payment'),
    path('shop/', views.edit_shop, name='shop'),
    path('shop/profile/', views.edit_shop_information, name='shop-profile'),
    path('shop/branding/', views.edit_shop_branding, name='shop-branding'),
    path('shop/general/', views.edit_shop_settings, name='shop-settings'),
    path('shop/deliveries/', views.edit_shop_deliveries, name='shop-delivery'),
    path('shop/payment/', views.edit_shop_account, name='shop-account'),
    path('account/', views.account_settings, name='account-settings'),
    path('notifications/', views.notification_settings, name='notification-settings'),
    path('notifications/save/', views.save_notification_settings, name='save-notification-settings'),
    path('resources/', views.resource_settings_faqs, name='resource-settings-faqs'),
    path('resources/feedback/', views.resource_settings_feedback, name='resource-settings-feedback'),
    path('resources/privacy-policy/', views.resource_settings_privacy_policy, name='resource-settings-privacy-policy'),
    path('resources/agreement/', views.resource_settings_agreement, name='resource-settings-agreement'),
    path('load_cities/', views.load_cities, name='load-cities'),
    path('load_times/', views.load_times, name='load-times'),
    path('load_pending_items', views.load_pending_items, name="load-items"),
    path('error-form/', views.error_form_submission, name='error-form'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)