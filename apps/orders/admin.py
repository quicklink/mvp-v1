from django.contrib import admin
from .models import *

# Register your models here.
class OrderItemInline(admin.StackedInline):
    model = OrderItem

class OrderInformationInline(admin.StackedInline):
    model = OrderInformation

class OrderPaymentInline(admin.StackedInline):
    model = OrderPayment

class CustomOrderAdmin(admin.ModelAdmin):
    inlines = (OrderItemInline, OrderInformationInline, OrderPaymentInline)
    ordering = ("id", "user", "shop", "total", "order_status", "payment_status", "complete")
    list_display = (
        "id",
        "user",
        "shop",
        "total",
        "order_status",
        "payment_status", 
        "complete"
    )
    list_filter = ("complete", "order_status", "payment_status", "shop")
    
    fieldsets = (
        (None, {'fields': ('user', 'shop', 'order_quantity', 'delivery_date', 'complete')}),
        ('Total', {'fields': ('subtotal','fees', 'total')}),
        ('Status', {'fields': ('order_status', 'payment_status')})
    )

    search_fields = ('id', 'user', 'shop')
    filter_horizontal = ()

admin.site.register(Order, CustomOrderAdmin)
admin.site.register(OrderItem)
admin.site.register(OrderInformation)
admin.site.register(OrderPayment)