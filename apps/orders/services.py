from django.contrib import messages
import json
from .selectors import *

import vonage
from sendgrid import SendGridAPIClient

def send_sms_order_updates(order):
    vonage_api_key = get_vonage_api_key()
    vonage_secret_key = get_vonage_secret_key()

    client = vonage.Client(key=vonage_api_key, secret=vonage_secret_key)
    sms = vonage.Sms(client)

    contact_number = order.order_information.session_sender.customer_contact_number
    contact_number = contact_number.split("+")[1]
    
    name = order.order_information.session_sender.customer_name
    name = name.split(" ")[0]

    order_id = order.get_id_with_leading_zeroes
    
    if order.order_status == "PREPARING":
        text = f"Hey, {name}! Your order (#{order_id}) from {order.shop.shop_info.shop_name} has been acknowledged and is being prepared. You can monitor its status through this link: {order.shortlink}."
    elif order.order_status == "DELIVERING":
        text = f"Hey, {name}! Your order (#{order_id}) from {order.shop.shop_info.shop_name} has been sent out for delivery. You can monitor its status through this link: {order.shortlink}."
    elif order.order_status == "COMPLETED":
        text = f"Hey, {name}! Your order (#{order_id}) from {order.shop.shop_info.shop_name} has been marked as received. Enjoy! You can view a summary of your order through this link: {order.shortlink}."
    elif order.order_status == "CANCELLED":
        text = f"Hey, {name}! Your order (#{order_id}) was marked as cancelled by {order.shop.shop_info.shop_name}. If you think there was a mistake, please contact the shop."

    if not order.order_status == "PENDING":
        responseData = sms.send_message(
            {
                "from": "Vonage APIs",
                "to": contact_number,
                "text": text,
            }
        )
        if responseData["messages"][0]["status"] == "0": return True
        else: return False
    else:
        return True

def send_email_order_updates(order):
    sg_api_key = get_sendgrid_api_key()

    order_info = order.order_information
    email = order_info.session_sender.customer_email
    name = order_info.session_sender.customer_name
    first_name = name.split(" ")[0]
    order_id = order.get_id_with_leading_zeroes

    if order.order_status == "PREPARING":
        email_message = f"Your order (#{order_id}) from {order.shop.shop_info.shop_name} has been acknowledged and is being prepared. You can monitor its status through this link: {order.shortlink}."
    if order.order_status == "DELIVERING":
        email_message = f"Your order (#{order_id}) from {order.shop.shop_info.shop_name} has been sent out for delivery. You can monitor its status through this link: {order.shortlink}."
    elif order.order_status == "COMPLETED":
        email_message = f"Your order (#{order_id}) from {order.shop.shop_info.shop_name} has been marked as received. Enjoy! You can view a summary of your order through this link: {order.shortlink}."
    elif order.order_status == "CANCELLED":
        email_message = f"Your order (#{order_id}) was marked as cancelled by {order.shop.shop_info.shop_name}. If you think there was a mistake, please contact the shop."

    sg_data = {
        "from": {
            "email":"josh@quicklink.ph"
        },
        "personalizations": [
            {
                "to":[
                    {
                        "email":email
                    },
                ],
                "dynamic_template_data": {
                    "subject": f"Checkout Order #{order_id}",
                    "first_name": first_name,
                    "body": email_message,
                    "link": order.shortlink
                }
            }
        ],
        "template_id":"d-4b20ca3dbb524161bbd00601d7c6ed69"
    }
    sg_data = json.loads(json.dumps(sg_data))

    try:
        sg = SendGridAPIClient(api_key=sg_api_key)
        response = sg.client.mail.send.post(request_body=sg_data)
        return True
    except Exception as e:
        return False
            
def send_customer_updates(request, orders):
    successful_updates = []
    failed_updates = []

    for order in orders:
        if hasattr(order, "order_information"):
            if order.order_information.session_notifications.sms:
                sms_update = send_sms_order_updates(order)
                if sms_update: successful_updates.append(order.get_id_with_leading_zeroes)
                else: failed_updates.append(order.get_id_with_leading_zeroes)
            elif order.order_information.session_notifications.email:
                email_update = send_email_order_updates(order)
                if email_update: successful_updates.append(order.get_id_with_leading_zeroes)
                else: failed_updates.append(order.get_id_with_leading_zeroes)
        else:
            messages.error(request, "An error occurred while sending updates. Please try again later or contact us at customers@quicklink.ph.")
    
    # Tentative fix while SMS is not available yet.
    successful_updates = list(dict.fromkeys(successful_updates))
    failed_updates = list(dict.fromkeys(failed_updates))

    if len(successful_updates) >= 1:
        order_numbers = ', '.join(successful_updates)
        messages.success(request, f"Update for Order #{order_numbers} successfully sent!")
    if len(failed_updates) >= 1:
        order_numbers = ', '.join(failed_updates)
        messages.error(request, f"An error occured while sending an update for Order #{order_numbers}. Please try again later or contact us at customers@quicklink.ph.")