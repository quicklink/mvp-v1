from django import template
from orders.models import OrderItem
import datetime
from django.db.models import Q

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_sizes(size, date):
    earliest_time = datetime.datetime.strptime(date + " 00:00:00", '%Y-%m-%d %H:%M:%S')
    latest_time = earliest_time + datetime.timedelta(days=1)
    items = OrderItem.objects.filter(
        order__complete=True,
        order__delivery_date__gte=earliest_time, 
        order__delivery_date__lte=latest_time, 
        size=size
    )
    items = items.filter(Q(order_status="PENDING") | Q(order_status="PREPARING"))

    quantity = 0
    for item in items:
        quantity += item.quantity

    return quantity
    # except:
    #     return 0
