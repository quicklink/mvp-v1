from django import template
from products.models import Addon

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_addon_price(addon, quantity):
    addon = Addon.objects.get(pk=addon)
    addon_price = addon.price_addon * quantity
    return addon_price
    # except:
    #     return 0
