from django import forms
from django.forms import ModelForm
from accounts.models import *
# from products.models import Product, Addon
from orders.models import OrderItem, OrderInformation
from django.utils.translation import ugettext_lazy as _
# from decimal import Decimal
from ph_geography.models import Province, Municipality

class PreviewOrderForm(ModelForm):
    class Meta: 
        model = OrderItem
        fields = [ 
            'product',
            'quantity',
        ]
        widgets = {
            'product': forms.HiddenInput(),
            'quantity': forms.HiddenInput()
            # 'quantity': forms.fields.NumberInput(attrs={
            #     'class':'quantity',
            #     'placeholder': '0'}),
        }
        labels = {
            'product': '',
            'quantity' : '',
        }

class OrderForm(ModelForm):
    class Meta: 
        model = OrderItem
        fields = [
            'quantity',
            'instructions',
        ]
        error_messages={'max': "You've reached the product's limited number of stocks."}

        required_fields = [
            'quantity',
        ]

        widgets = {
            'quantity': forms.HiddenInput(),
            # 'quantity': forms.fields.NumberInput(attrs={
            #     'class':'subheader bold order__quantity',
            #     'placeholder': '0'}),
            'instructions': forms.Textarea(attrs={
                'class':'large-input default subtitle',
                'placeholder': 'Leave us a note! (Optional)'}),
        }
    
    def __init__(self, *args, **kwargs):
        product_stock = kwargs.pop('product_stock', None)
        super(OrderForm, self).__init__(*args, **kwargs)
        if product_stock != None:
            self.fields['quantity'].widget.attrs.update({'min': 0, 'max': product_stock})

class CheckoutForm(forms.Form):
    BANKS = [
        ("Bank Transfer", _("Bank Transfer (BPI, UnionBank)")),
        ("Debit/Credit Card", _("Debit/Credit Card")),
        ("eWallet", _("eWallet (GCash, PayMaya, GrabPay)")),
    ]

    # Personal Details
    name = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Name'}),)
    email = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Email'}),)
    contact_number = forms.CharField(label='',
        required=False,  
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Mobile Number'}),)
    facebook = forms.CharField(label='',  
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Facebook Link (i.e. facebook.com/quicklinkph)'}),)
    instagram = forms.CharField(label='',  
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class':'input default subtitle',
        'placeholder': 'Instagram Link (i.e. instagram.com/quicklink.ph)'}),)

    # Shipping Address
    line1 = forms.CharField(label='', 
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Address Line 1'}))
    line2 = forms.CharField(label='', required=False,
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Address Line 2'}))
    city = forms.ChoiceField(label='',
        required=False, 
        widget=forms.Select(attrs={
        'class': 'wide input default subtitle', 
        'placeholder': 'City'}),
        choices=[(x.name.title(), x.name.title()) for x in Municipality.objects.filter(province=Province.objects.get(name="METRO MANILA")).order_by('name')])
    province = forms.ChoiceField(label='',
        required=False, 
        widget=forms.Select(attrs={
        'class': 'wide input default subtitle', 
        'placeholder': 'Province'}),
        choices=[(x.name.title(), x.name.title()) for x in Province.objects.all().order_by('name')])
    postal_code = forms.CharField(label='', 
        required=False,
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Postal Code'}))

    # Preferred Delivery Date
    delivery_date = forms.DateTimeField(label='',
        input_formats=['%B %d, %Y - %H:%M%p', '%Y-%m-%d, %H:%M:%S %p', '%Y-%m-%d, %h:%M%p', '%Y-%m-%d, %H:%M', ],
        widget=forms.widgets.DateTimeInput(attrs={
            'class': 'date-input-content subtitle',
        })
    )

    # Preferred Delivery Option
    delivery_options = forms.ChoiceField(label='', 
        widget=forms.RadioSelect(attrs={
        'class': 'default subtitle'}))

    # Mode of Payment
    bank_name = forms.ChoiceField(label='', 
        widget=forms.RadioSelect(attrs={
        'class': 'default subtitle'}),
        choices=BANKS)

    # Updates
    notif_sms = forms.BooleanField(required=False, label='Receive SMS updates on order status',
        widget=forms.fields.CheckboxInput())
    notif_email = forms.BooleanField(required=False, label='Receive email updates on order status',
        widget=forms.fields.CheckboxInput())

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        shop = kwargs.pop('shop', None)
        order = kwargs.pop('order', None)
        date = kwargs.pop('date', None)
        super(CheckoutForm, self).__init__(*args, **kwargs)

        self.fields['delivery_date'].initial = date
        self.fields['province'].initial = Province.objects.get(name="METRO MANILA").name.title()
        self.fields['notif_sms'].initial = False
        self.fields['notif_email'].initial = True

        if shop:
            choices = shop.shop_delivery_settings.get_delivery_options
            self.fields['delivery_options'].choices = choices
        else:
            choices = None

        if order and hasattr(order, 'order_information'):
            session_sender = order.order_information.session_sender
            session_address = order.order_information.session_address
            session_notifications = order.order_information.session_notifications
            delivery_date = order.delivery_date
            self.fields['name'].initial = session_sender.customer_name
            self.fields['email'].initial = session_sender.customer_email
            if session_sender.customer_contact_number: self.fields['contact_number'].initial = session_sender.customer_contact_number[3:]
            self.fields['line1'].initial = session_address.line1
            self.fields['line2'].initial = session_address.line2
            self.fields['city'].initial = session_address.city
            self.fields['province'].initial = session_address.province
            self.fields['postal_code'].initial = session_address.postal_code
            self.fields['delivery_date'].initial = delivery_date
            self.fields['notif_sms'].initial = session_notifications.sms
            self.fields['notif_email'].initial = session_notifications.email
        elif user and user.is_authenticated:
            if user.role == "CUSTOMER" and CustomerInformation.objects.filter(customer=user, customer_profile=True).exists():
                customer_info = CustomerInformation.objects.get(customer=user, customer_profile=True)
                self.fields['name'].initial = customer_info.customer_name
                self.fields['email'].initial = customer_info.customer_email
                self.fields['contact_number'].initial = customer_info.customer_contact_number[2:]
            elif user.role == "MERCHANT" and ShopInformation.objects.filter(user=user).exists():
                shop_info = ShopInformation.objects.get(user=user)
                self.fields['name'].initial = shop_info.shop_name
                self.fields['email'].initial = shop_info.shop_email
                self.fields['contact_number'].initial = shop_info.shop_contact_number[2:]

            # Saved Customer Address
            try: user_address = Address.objects.get(user=user, default=True)
            except: user_address = None

            if user_address:
                self.fields['line1'].initial = user_address.line1
                self.fields['line2'].initial = user_address.line2
                self.fields['city'].initial = user_address.city
                self.fields['province'].initial = user_address.province
                self.fields['postal_code'].initial = user_address.postal_code

            # Default Customer Notifications
            try: user_notifications = CustomerNotification.objects.get(user=user, default=True)
            except: user_notifications = None

            if user_notifications:
                self.fields['notif_sms'].initial = user_notifications.sms
                self.fields['notif_email'].initial = user_notifications.email

            # Preferred Delivery Date
            self.fields['delivery_date'].initial = date

    # def save(self, *args, **kwargs):
    #     # Pop extra arguments
    #     customer = kwargs.pop('customer', None)
    #     sms = kwargs.pop('sms', None)
    #     email = kwargs.pop('email', None)
    #     order = kwargs.pop('order', None)
    #     shop = kwargs.pop('shop', None)
    #     items = kwargs.pop('items', None)

    #     contact_number = ("+63" + self.cleaned_data.get('contact_number')).replace(' ', '')

    #     session_sender, created = CustomerInformation.objects.get_or_create(
    #         customer=customer,
    #         customer_name=self.cleaned_data.get('name'),
    #         customer_email=self.cleaned_data.get('email'),
    #         customer_contact_number=contact_number,
    #     )
    #     session_sender.save()

    #     session_address, created = Address.objects.get_or_create(
    #         user=customer,
    #         line1=self.cleaned_data.get('line1'),
    #         line2=self.cleaned_data.get('line2'),                
    #         city=self.cleaned_data.get('city'),
    #         province=self.cleaned_data.get('province'),
    #         postal_code=self.cleaned_data.get('postal_code'),
    #     )
    #     session_address.save()

    #     if sms != None or sms != False: sms = True
    #     if email != None or email != False: email = True

    #     session_notifications, created = CustomerNotification.objects.get_or_create(
    #         user=customer,
    #         sms=sms,
    #         email=email
    #     )
    #     session_notifications.save()

    #     order_info, created = OrderInformation.objects.get_or_create(
    #         order=order,
    #         session_sender=session_sender,
    #         session_address=session_address,
    #         session_notifications=session_notifications,
    #     )
    #     order_info.save()

    #     if shop.shop_general_settings.shop_customer_fees == True:
    #         order.fees = order.subtotal * Decimal(0.04)

    #     order.total = order.subtotal + order.fees
    #     order.save()

    #     order.delivery_date = self.cleaned_data.get('delivery_date')
    #     order.delivery_option = self.cleaned_data.get('delivery_options')
    #     order.save()

    #     for item in items:
    #         item.delivery_date = order.delivery_date.date()
    #         item.save()
        
    #     return order

# Direct Debit / Bank Transfer Form
class BankTransferForm(forms.Form):
    BANKS = [
        ("BA_BPI", _("Bank of the Philippine Islands (BPI)")),
        # ("BA_BDO", _("Banco de Oro (BDO)")),
        ("BA_UBP", _("Unionbank Philippines (UBP)")),
    ]

    bank_name = forms.ChoiceField(label='', 
        widget=forms.Select(attrs={
        'class': 'wide input default subtitle'}),
        choices=BANKS)

# Payment Details for Debit / Credit
class CardForm(forms.Form):
    cardholder_name = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Ex. John Smith'}))
    account_number = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'Ex. 4000 0000 0000 0001'}))
    exp_date = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': 'MM/YY'}))
    cvv = forms.CharField(label='', 
        widget=forms.fields.TextInput(attrs={
        'class': 'input default subtitle', 
        'placeholder': '***'}))

class eWalletForm(forms.Form):
    EWALLET = [
        ("PH_GCASH", _("GCash")),
        ("PH_PAYMAYA", _("PayMaya")),
        ("PH_GRABPAY", _("GrabPay"))
    ]

    bank_name = forms.ChoiceField(label='', 
        widget=forms.Select(attrs={
        'class': 'wide input default subtitle'}),
        choices=EWALLET)