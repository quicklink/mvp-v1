from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields import IntegerField
from django.utils.timezone import datetime, timedelta

from accounts.models import *
from products.models import *
import uuid
from django.utils.translation import ugettext_lazy as _

from model_utils.fields import StatusField, MonitorField
from model_utils import Choices

class BaseOrdersModel(models.Model):
    class Meta:
        abstract = True
        app_label = 'orders'

# Create your models here.
class Order(BaseOrdersModel):
    # User manipulating the product (request.user — whoever is logged in)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE, related_name="order_sender")

    # Shop getting the order
    shop = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="order_receiver")

    # Order quantity is different from product quantity -- sum of all products na kasama
    order_quantity = models.IntegerField(null=True, default=1)
    
    # Subtotal for the order
    subtotal = models.DecimalField(null=True, default=0.00, max_digits=10, decimal_places=2)

    # Convenience fees for the order
    fees = models.DecimalField(null=True, default=0.00, max_digits=8, decimal_places=2)

    # Total fees for the order
    total = models.DecimalField(null=True, default=0.00, max_digits=12, decimal_places=2)

    # Order Status
    ORDER_STATUS = Choices('PENDING', 'PREPARING', 'DELIVERING', 'COMPLETED', 'CANCELLED', 'FAILED')
    order_status = StatusField(choices_name='ORDER_STATUS')
    order_status_changed = MonitorField(monitor='order_status')
    
    # Payment Status
    PAYMENT_STATUS = Choices('PENDING', 'PROCESSING', 'RECEIVED')
    payment_status = StatusField(choices_name='PAYMENT_STATUS')
    payment_status_changed = MonitorField(monitor='payment_status')
    
    # Date of when the order was published (Auto-filled)
    order_date = models.DateTimeField(auto_now_add=True, null=True)

    # Preferred delivery date of when the product will be delivered (Auto-filled)
    delivery_date = models.DateTimeField(null=True)

    # Preferred delivery option
    delivery_option = models.CharField(max_length=55, null=True)

    # Check if order is complete
    complete = models.BooleanField(default=False, null=True, blank=False)

    # Order invoice short URL
    shortlink = models.CharField(max_length=255, null=True, blank=False)

    notes = models.CharField(max_length=255, null=True, blank=True, default="")

    def __str__(self):
        if hasattr(self.shop, "shop_info"):
            try: account = BankAccount.objects.get(user=self.shop)
            except: account = False
            if account: return f"[{self.shop.shop_info.shop_name} — {account.account_number} ({account.bank_name})] Order #{self.id} - Total of PHP {self.total} (Convenience Fee: PHP {self.fees})"
            else: return f"[{self.shop.shop_info.shop_name}] Order #{self.id} - Total of PHP {self.total} (Convenience Fee: PHP {self.fees})"
        else:
            return f"Order #{self.id} - Total of PHP {self.total} (Convenience Fee: PHP {self.fees})"

    @property
    def get_table_id(self):
        # Get Initials
        shop = self.shop
        initials = []
        if hasattr(shop, 'shop_info'):
            for name in list(shop.shop_info.shop_name.split(' ')):
                initials.append(name[0])
        
        padded_value = []
        for i in initials:
            padded_value.append(str(i))

        # Get Leading Zeroes
        desired_digits = 3
        value = self.id
        num_zeros = int(desired_digits) - len(str(value))
        while num_zeros >= 1:
            padded_value.append("0") 
            num_zeros = num_zeros - 1
        padded_value.append(str(value))

        return "".join(padded_value)

    @property
    def get_id_with_leading_zeroes(self):
        desired_digits = 4
        value = self.id
        num_zeros = int(desired_digits) - len(str(value))
        padded_value = []
        while num_zeros >= 1:
            padded_value.append("0") 
            num_zeros = num_zeros - 1
        padded_value.append(str(value))
        return "".join(padded_value)

    @property
    def get_delivery_date(self):
        orderItems = self.orderitem_set.all()
        highest_days = 0
        for item in orderItems:
            if item.product.days > highest_days: 
                highest_days = item.product.days
        min_date = datetime.today() + timedelta(days=highest_days+1)
        return min_date

    @property
    def get_cart_quantity(self):
        order_items = self.orderitem_set.all()
        quantity_list = [item.quantity for item in order_items]
        quantity = sum(quantity_list)
        return quantity

    @property
    def get_cart_total(self):
        order_items = self.orderitem_set.all()
        total = ([item.get_total for item in order_items])
        return total

    @property
    def get_cart_items(self):
        order_items = self.orderitem_set.all()
        total = ([item.quantity for item in order_items])
        return total

class OrderInformation(BaseOrdersModel):
    # User session id if user attribute does not exist (if user isn't logged in, use a session id instead)
    session_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    order = models.OneToOneField(
        Order, 
        related_name="order_information",
        null=True, 
        on_delete=models.SET_NULL
    )

    # Guest session user info
    session_sender = models.ForeignKey(
        CustomerInformation, 
        related_name="sender_information", 
        on_delete=models.SET_NULL, 
        null=True
    )
    
    # Guest session user address
    session_address = models.ForeignKey(
        Address, 
        related_name="sender_address", 
        on_delete=models.SET_NULL, 
        null=True
    )

    # Guest session user payment details
    session_payment = models.ForeignKey(
        BankAccount, 
        related_name="sender_address", 
        on_delete=models.SET_NULL, 
        null=True
    )

    # Guest session user payment details
    session_notifications = models.ForeignKey(
        CustomerNotification, 
        related_name="sender_address", 
        on_delete=models.SET_NULL, 
        null=True,
    )

    # # JWT token ID (Credit Card)
    # token_jwt_id = models.CharField(max_length=999, null=True, blank=True)
    
    # # For encoding token to jwt
    # token_private_key = models.CharField(max_length=255, null=True, blank=True)

    # # For decoding token to jwt
    # token_public_key = models.CharField(max_length=255, null=True, blank=True)

    # # JWT customer ID (eWallet)
    # customer_jwt_id = models.CharField(max_length=999, null=True, blank=True)
    # customer_private_key = models.CharField(max_length=255, null=True, blank=True)
    # customer_public_key = models.CharField(max_length=255, null=True, blank=True)

    # def __str__(self):
    #     return f"Order #{self.order.id}: {self.session_sender.customer_name}"

    def __str__(self):
        if hasattr(self.order, "id"):
            if hasattr(self.session_sender, "customer_name"):
                return f"Order #{self.order.id} — {self.session_sender.customer_name}"
            else:
                return f"Order #{self.order.id} - No Customer Name"
        else:
            if hasattr(self.session_sender, "customer_name"):
                return f"Incomplete Order — {self.session_sender.customer_name}"
            else:
                return f"Incomplete Order - No Customer Name"

    @property
    def get_sender_details(self):
        customer_info = self.session_sender
        email = customer_info.customer_email
        name = customer_info.customer_name
        given_names = ' '.join([name for name in name.split(' ')[:-1]])
        surname = name.split(' ')[-1]
        phone_number = customer_info.customer_contact_number
        return {
            "email": email,
            "given_names": given_names,
            "surname": surname,
            "phone_number": phone_number
        }

    @property
    def get_given_names(self):
        name = self.session_sender.customer_name
        given_name = ' '.join([name for name in name.split(' ')[:-1]])
        return given_name

    @property
    def get_surname(self):
        name = self.session_sender.customer_name
        surname = name.split(' ')[-1]
        return surname

class OrderPayment(BaseOrdersModel):
    order = models.OneToOneField(Order, related_name='order_payment', null=True, on_delete=models.SET_NULL)
    xendit_charge_id = models.CharField(max_length=100, null=True, blank=True)
    payment_for = models.CharField(max_length=100, null=True, blank=True)
    amount = models.DecimalField(null=True, default=0.00, max_digits=10, decimal_places=2)
    paid = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Order #{self.order.id}: {self.paid}"

class OrderItem(BaseOrdersModel):
    # Order cart
    order = models.ForeignKey(Order, null=True, blank=True, on_delete=models.SET_NULL) 

    # Product ordered
    product = models.ForeignKey(Product, related_name="order_item_product", null=True, on_delete=models.SET_NULL)

    # Product size
    size = models.ForeignKey(Size, related_name="order_item_size", null=True, on_delete=models.SET_NULL)
    
    # Product addons
    addons = models.ManyToManyField(Addon, related_name="order_item_addons", blank=True)  
    
    # Product quantity ordered
    quantity = models.PositiveSmallIntegerField(null=True, default=1)

    # Special instructions per order
    instructions = models.CharField(max_length=140, null=True, blank=True)
    
    # Total product fees (Auto-filled in views.py under updateItem, line 265)
    total = models.DecimalField(null=True, default=0, max_digits=8, decimal_places=2)

    # Preferred delivery date of when the product will be delivered (Auto-filled)
    delivery_date = models.DateField(null=True)

    # Date of when the order was published (Auto-filled)
    order_date = models.DateTimeField(auto_now_add=True, null=True)
    
    def __str__(self):
        if hasattr(self.order, "id"):
            return f"Order #{self.order.id} — {self.quantity} {self.product} (Total of PHP {self.total})"
        else:
            if self.order and self.order.complete:
                return f"Complete Order — {self.quantity} {self.product} (Total of PHP {self.total}"
            return f"Incomplete Order — {self.quantity} {self.product} (Total of PHP {self.total})"

    @property
    def get_delivery_date(self):
        if self.order.delivery_date != None:
            delivery_date = self.order.delivery_date
        else:
            delivery_date = None
        return delivery_date

    @property
    def get_subtotal(self):
        return self.size.price_size * self.quantity

    @property
    def get_total(self):
        addons_total = 0
        try:
            if self.addons:
                for i in self.addons.all(): addons_total += (self.quantity * i.price_addon)
        except: pass
        total = (self.size.price_size * self.quantity) + addons_total
        return total