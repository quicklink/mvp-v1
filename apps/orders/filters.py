import django_filters as filters
from django.db import models
from django import forms

from products.models import Product
from orders.models import Order, OrderItem
from django.utils.translation import gettext as _

def get_product_choices(request):
    if request is None:
        user = None
    else:
        user = request.user
        products = Product.objects.filter(user=user).order_by('name')
        return products

class PendingOrderFilter(filters.FilterSet):
    delivery_date = filters.DateFilter(widget=forms.DateTimeInput(attrs={'placeholder': 'Full History'}))
    product = filters.ModelChoiceFilter(queryset=get_product_choices, empty_label=_("All Products"))

    class Meta:
        model = OrderItem
        fields = [
            'product', 
            'delivery_date',
        ]
        labels = {
            'product': '',
            'delivery_date': '',
        }
        empty_labels = {
            'delivery_date': 'All Dates'
        }

    # Filter product owners to only those that are owned by the logged-in user
    @property
    def qs(self):
        parent = super().qs
        user = getattr(self.request, 'user', None)

        return parent.filter(order__shop=user) \
            | parent.filter(order__order_status="PENDING") \
            | parent.filter(order__complete=True)

    @property
    def get_product(self):
        if self.data['product']:
            return self.data['product']
        else:
            return False
        
    def __init__(self, data=None, queryset=None, request=None, prefix=None):
        # Initialize Filter
        super(PendingOrderFilter, self).__init__(data=data, queryset=queryset, request=request, prefix=prefix)
        self.filters['product'].label=''
        self.filters['product'].field.widget.attrs.update({
            'class':'subtitle bold',
        })
        self.filters['delivery_date'].label=''
        self.filters['delivery_date'].field.widget.attrs.update({
            'class':'date-input-content subtitle',
        })

class OrderFilter(filters.FilterSet):
    delivery_date = filters.DateFilter(widget=forms.DateTimeInput(attrs={'placeholder': 'Full History'}))
    class Meta:
        model = Order
        fields = [
            'delivery_date',
        ]
        labels = {
            'delivery_date': '',
        }

    # Filter product owners to only those that are owned by the logged-in user
    @property
    def qs(self):
        parent = super().qs
        user = getattr(self.request, 'user', None)

        return parent.filter(shop=user, complete=True).order_by('delivery_date')
        
    def __init__(self, data=None, queryset=None, request=None, prefix=None):
        # Initialize Filter
        super(OrderFilter, self).__init__(data=data, queryset=queryset, request=request, prefix=prefix)
        self.filters['delivery_date'].label=''
        self.filters['delivery_date'].field.widget.attrs.update({
            'class':'date-input-content subtitle',
        })
