import os
import environ
from django.conf import settings

from django.shortcuts import render, redirect
from django.utils import timezone
from django.utils.timezone import datetime, timedelta
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page
from django.views.decorators.cache import cache_control

from .services import *
from .forms import PreviewOrderForm, OrderForm
from .filters import OrderFilter, PendingOrderFilter
from products.models import Product, Size, Addon
from orders.models import Order, OrderItem
from accounts.decorators import setup_required, allowed_users
from accounts.models import *

from django.http import HttpResponse, JsonResponse
from decimal import Decimal
import json

import openpyxl

from os import path
from datetime import datetime

from django.core.paginator import Paginator
from django.core import serializers

import vonage

# Create your views here.

# Change Order Status
def order_status_change(request):
    data = request.GET.copy()
    order_ids = request.GET.getlist("orders[]")
    status = data.get("status", "")
    orders = Order.objects.filter(id__in=order_ids)
    orders.update(order_status=status)

    if len(order_ids) > 1:
        shop_info = User.objects.select_related("shop_info").get(pk=request.user.pk)
        shop_info.can_send_updates = True
        shop_info.save()

    res = serializers.serialize('json', list(orders), fields=("id", "order_status"))
    return JsonResponse(data=res, safe=False)

# def send_update_change(request):
#     shop_info = User.objects.select_related("shop_info").get(pk=request.user.pk)
#     shop_info.can_send_updates = True
#     shop_info.save()
#     res = serializers.serialize('json', [shop_info, ])
#     return JsonResponse(data=res, safe=False)

# Order Summary
@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(30)
@cache_control(private=True)
def orders(request):
    user = request.user
    can_send_updates = user.shop_info.can_send_updates
    data = request.GET.copy()
    orderFilter = OrderFilter(data, request=request)
    orders = orderFilter.qs

    search_query = request.GET.get('search', None)

    if search_query:
        orders = orders.filter( 
            Q(id__icontains=search_query) |
            Q(order_information__session_sender__customer_name__icontains=search_query) |
            Q(order_status__icontains=search_query) |
            Q(payment_status__icontains=search_query)
        )

    current_time = datetime.now()
    file_date = current_time.strftime('%m%d%y')

    shop_name = user.shop_info.slugify_name
    file_name = "QL_" + shop_name + "_order_summary_" + str(file_date)

    if request.method == "POST":
        if "send_customer_updates" in request.POST:
            last_update = user.shop_info.update_status_changed
            update_orders = Order.objects.filter(shop=user, complete=True, order_status_changed__gte=last_update)

            send_customer_updates(request, update_orders)

            shop_info = user.shop_info
            shop_info.can_send_updates = False
            shop_info.save()

    context = {'orders':orders, 'orderFilter':orderFilter, "file_name":file_name, "can_send_updates": can_send_updates, "search_query":search_query}
    return render(request, 'orders/order_summary.html', context)

# Orders -- Order Filter (shop=request.user)

# Total Expected Revenue
# total_revenue = 0
# for order in orders:
#   total_revenue += order.total

# Payments Received
# received_orders = Order.objects.filter(shop=request.user, payment_status="RECEIVED")
# payments_received = 0
# for order in received_orders
#   payments_received += order.total

# Payments Receivables
# receivable_orders = Order.objects.filter(shop=request.user).exclude(payment_status="RECEIVED")
# receivables = 0
# for order in receivable_orders
#   receivables += order.total

# context = {"orders":orders, "total_revenue":total_revenue, "payments_received":payments_received, "receivables":receivables}
# {% for order in orders %}
# {{ total_revenue }}
# {{ payments_received }}
# {{ receivables }}

# Pending Orders
@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(30)
@cache_control(private=True)
def pending_orders(request):
    # Get merchant user
    user = request.user

    # Get initial value for filter set (Alphabetical Order)
    productChoices = Product.objects.filter(user=user).order_by('name')
    if len(productChoices) > 0:
        product = Product.objects.filter(user=user).first()
    else:
        product = None
    
    # Set initial data
    delivery_date = timezone.localdate()
    data = request.GET.copy()
    data.setdefault('product', None)
    data.setdefault('delivery_date', delivery_date)

    # Get pending orders
    orders = Order.objects.filter(shop=user, complete=True)
    orders = orders.filter(Q(order_status="PENDING") | Q(order_status="PREPARING"))
    
    # Get pending product orders
    order_items = OrderItem.objects.filter(order__in=orders).order_by('product')

    # Product filter form
    pendingFilter = PendingOrderFilter(data, request=request, queryset=order_items)
    items = pendingFilter.qs

    if pendingFilter.get_product:
        sizes = Size.objects.filter(product=pendingFilter.get_product)
        addons = Addon.objects.filter(product=pendingFilter.get_product)
    else:
        sizes = None
        addons = None

    if data.get("delivery_date"):
        try:
            date = data.get("delivery_date").strftime('%m%d%y')
        except:
            date = datetime.strptime(data.get("delivery_date"), '%Y-%m-%d').strftime('%m%d%y')
    else:
        date = datetime.now().strftime('%m%d%y')

    shop_name = user.shop_info.slugify_name

    if data.get("product"):
        id = data.get("product")
        product_name = Product.objects.get(id=id)
        product_name = product_name.name
        product_name = product_name.replace(" ", "_").lower()
        file_name = "QL_" + shop_name + "_pending_" + product_name + "_" + str(date)
    else:
        file_name = "QL_" + shop_name + "_pending_orders_" + str(date)
    
    context = {
        'product':product,
        'orders':orders,
        'order_items':order_items,
        'pendingFilter':pendingFilter,
        'items':items,
        'sizes':sizes, 
        'addons':addons,
        "file_name":file_name
    }
    return render(request, 'orders/pending_orders.html', context)

# Deleting Orders (For Testing)
# @login_required(login_url='accounts:login')
# @setup_required
# @allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
# def delete_order(request, order_pk):
#     order = Order.objects.get(id=order_pk)
#     if request.method == "POST":
#         order.delete()
#         return redirect('/shop/orders/')

#     context = {'order':order}
#     return render(request, 'orders/delete_order.html', context)