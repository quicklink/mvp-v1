from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.orders, name='order-summary'),
    # path('delete-order/<str:order_pk>/', views.delete_order, name='delete-order'),
    path('pending/', views.pending_orders, name='pending-orders'),
    path('status-change/', views.order_status_change, name='order-status-change'),
    # path('update-change/', views.send_update_change, name="send-update-change")
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)