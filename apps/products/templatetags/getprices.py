from django import template
from products.models import Size

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_min_price(product):
    sizes = Size.objects.filter(product=product)
    size_prices = []
    for size in sizes:
        size_prices.append(size.price_size)
    return min(size_prices)

@register.filter
def get_max_price(product):
    sizes = Size.objects.filter(product=product)
    size_prices = []
    for size in sizes:
        size_prices.append(size.price_size)
    return max(size_prices)