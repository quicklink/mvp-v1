# from django.forms.models import inlineformset_factory
from accounts.decorators import allowed_users, setup_required
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.views.decorators.cache import cache_page
from django.views.decorators.cache import cache_control

# from django.views.generic import FormView
# from django.views.generic.detail import SingleObjectMixin
import json

from products.models import *
from accounts.models import User
from .forms import *
from .validators import *
from django.db.models import Q

from django_hosts.resolvers import reverse

# Create your views here.
@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
# @cache_page(5)
@cache_page(60*60*24)
@cache_control(private=True)
def products(request):
    user = request.user
    slug = user.shop_info.shop_username
    form_link = request.build_absolute_uri(reverse('view-products', host='shops', args=(str(slug),)))
    search_query = request.GET.get('search', '')

    if search_query:
        products = Product.objects.filter(Q(user=user) & 
            Q(name__icontains=search_query) |
            Q(stock__icontains=search_query)
        )
    else:
        products = Product.objects.filter(user=user)

    context = {'products':products, 'form_link':form_link, 'search_query':search_query}
    return render(request, 'products/products.html', context)

# @login_required(login_url='accounts:login')
# @setup_required
# @allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
# def product_details(request, product_pk):
#     product = Product.objects.get(id=product_pk)
#     return render(request, 'products/product_modal.html', {'product':product})

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60*60*24)
@cache_control(private=True)
def add_product(request):
    user = request.user
    form = ProductForm()
    imageFormset = ImageFormset()
    sizeFormset = SizeFormset()
    addonFormset = AddonFormset()

    if request.method == 'POST':
        form = ProductForm(request.POST)
        
        if form.is_valid():        
            # Create product in database
            product = Product.objects.create(
                user=user,
                name=form.cleaned_data.get('name'),
                description=form.cleaned_data.get('description'),
                instructions=form.cleaned_data.get('instructions'),
            )

            # Insert if radio is checked for stock
            if 'stocks-input-select' in request.POST:
                if request.POST.get('stocks') == 0:
                    messages.info(request, 'You placed 0 as your total number of stocks. If this is a mistake, please edit your product.')
                product.stock = form.cleaned_data.get('stock')
                product.made_to_order = False
            else:
                product.made_to_order = True
                product.stock = 0

            # Insert if radio is checked for order
            if 'orders-input-select' in request.POST:
                if request.POST.get('orders') == 0:
                    messages.info(request, 'You placed 0 as your maximum order limit per day. If this is a mistake, please edit your product.')
                product.orders = form.cleaned_data.get('orders')
                product.no_order_limit = False
            else:
                product.no_order_limit = True
                product.orders = 0

            product.save()
            
            imageFormset = ImageFormset(request.POST, request.FILES, instance=product)

            # Save Formsets
            if imageFormset.is_valid():
                images = imageFormset.save()
                count = 0
                for image in images:
                    image.product = product
                    image.active = True
                    if count == 0: image.default = True
                    count += 1
                    image.save()
                    
                    name = image.get_product_name
                    image.name = name
                    image.save()
            else:
                product.delete()

            sizeFormset = SizeFormset(request.POST, instance=product)
            addonFormset = AddonFormset(request.POST, instance=product)

            if sizeFormset.is_valid():
                sizes = sizeFormset.save(commit=False)
                for size in sizes:
                    size.product = product
                    size.save()
            else:
                product.delete()

            if addonFormset.is_valid():
                addonInputs = request.POST.get('addonForm-TOTAL_FORMS')
                if int(addonInputs) > 1:
                    addons = addonFormset.save(commit=False)
                    for addon in addons:
                        addon.product = product
                        addon.save()
                elif int(addonInputs) == 1:
                    if request.POST.get('addon_set-0-addon') == '':
                        pass
                    else:
                        addons = addonFormset.save(commit=False)
                        for addon in addons:
                            addon.product = product
                            addon.save()
            messages.success(request, "Successfully added product!")
            return redirect('/products/')

    context = {'form':form, 'imageFormset':imageFormset, 'sizeFormset':sizeFormset, 'addonFormset':addonFormset}
    return render(request, 'products/product_form.html', context)

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
@cache_page(60)
@cache_control(private=True)
def update_product(request, product_pk):
    product = Product.objects.get(id=product_pk)
    form = ProductForm(instance=product)
    images = Image.objects.filter(product=product, active=True).order_by('id')
    max_images = range(0,3)

    imageFormset = ImageFormset(instance=product)
    sizeFormset = SizeFormset(instance=product)
    addonFormset = AddonFormset(instance=product)
    
    if request.method == 'POST':
        form = ProductForm(request.POST, instance=product)
        if form.is_valid():
            product.name = form.cleaned_data.get('name')
            product.description = form.cleaned_data.get('description')
            product.instructions = form.cleaned_data.get('instructions')

            # Insert if radio is checked for stock
            if 'stocks-input-select' in request.POST:
                product.stock = form.cleaned_data.get('stock')
                product.made_to_order = False
            else:
                product.made_to_order = True
                product.stock = 0

            # Insert if radio is checked for order
            if 'orders-input-select' in request.POST:
                product.orders = form.cleaned_data.get('orders')
                product.no_order_limit = False
            else:
                product.no_order_limit = True
                product.orders = 0
            
            # Save Product
            product.save()

            imageFormset = ImageFormset(request.POST, request.FILES, instance=product)
            sizeFormset = SizeFormset(request.POST, instance=product)
            addonFormset = AddonFormset(request.POST, instance=product)
            
            # Save Formsets
            currentImages = images
            if imageFormset.is_valid():
                images = imageFormset.save(commit=False)
                print(len(images) + len(currentImages))
                if len(images) > 0:
                    if len(images) + len(currentImages) > 3:
                        for image in currentImages:
                            image.active = False
                            image.save()
                        for image in images:
                            image.product = product
                            image.active = True
                            image.save()
                            name = image.get_product_name
                            image.name = name
                            image.save()
                    else:
                        for image in images:
                            image.product = product
                            image.active = True
                            image.save()
                            name = image.get_product_name
                            image.name = name
                            image.save()
            else:
                print("Image Errors: ",imageFormset.errors)

            if sizeFormset.is_valid():
                sizes = sizeFormset.save(commit=False)
                for size in sizes:
                    size.product = product
                    size.save()
            else:
                print("Size Errors: ", sizeFormset.errors)

            if addonFormset.is_valid():
                addonInputs = request.POST.get('addonForm-TOTAL_FORMS')
                if int(addonInputs) > 1:
                    addons = addonFormset.save(commit=False)
                    for addon in addons:
                        addon.product = product
                        addon.save()
                elif int(addonInputs) == 1:
                    if request.POST.get('addon_set-0-addon') == '':
                        pass
                    else:
                        addons = addonFormset.save(commit=False)
                        for addon in addons:
                            addon.product = product
                            addon.save()

            messages.success(request, "Successfully updated product!")
            return redirect('/products/')
        else:
            print(form.errors)

    context = {
        'form':form, 
        'product':product, 
        'images':images, 
        'imageFormset':imageFormset, 
        'sizeFormset':sizeFormset, 
        'addonFormset':addonFormset, 
        'max_images':max_images
    }
    return render(request, 'products/edit_product.html', context)

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
def set_products_as_active(request):
    if request.method == "POST" and request.is_ajax:
        Product.objects.filter(pk=request.POST.get('products')).update(active=True)
        return redirect('/products/')
    return JsonResponse(request.POST, safe=False)

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
def set_products_as_inactive(request):
    if request.method == "POST" and request.is_ajax:
        productsList = json.loads(request.POST.get('products'))
        Product.objects.filter(id__in=productsList).update(active=False)
        return redirect('/products/')
    return JsonResponse(request.POST, safe=False)

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
def delete_selected_products(request):
    if request.method == "POST" and request.is_ajax:
        productsList = json.loads(request.POST.get('products'))
        Product.objects.filter(id__in=productsList).delete()
        messages.success(request, "Successfully deleted products!")
        return redirect('/products/')
    return JsonResponse(request.POST, safe=False)

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
def duplicate_product(request, product_pk):
    product = Product.objects.get(id=product_pk)
    images = Image.objects.filter(product=product)
    sizes = Size.objects.filter(product=product)
    addons = Addon.objects.filter(product=product)
    product.id = None
    product.save()

    for image in images:
        image.id = None
        image.product = product
        image.save()

    for size in sizes:
        size.id = None
        size.product = product
        size.save()

    for addon in addons:
        addon.id = None
        addon.product = product
        addon.save()

    return redirect('/products/')

@login_required(login_url='accounts:login')
@setup_required
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
def delete_product(request, product_pk):
    product = Product.objects.get(id=product_pk)
    product.delete()
    return redirect('/products/')