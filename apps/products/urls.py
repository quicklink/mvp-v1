from django.urls import path
from . import views

urlpatterns = [
    path('', views.products, name='products'),
    path('add/', views.add_product, name='add-product'),
    path('set-active/', views.set_products_as_active, name='set-product-active'),
    path('set-inactive/', views.set_products_as_inactive, name='set-products-inactive'),
    path('delete-selected/', views.delete_selected_products, name='delete-selected-products'),
    # path('product/<str:product_pk>/', views.product_details, name='product'),
    path('update/<str:product_pk>/', views.update_product, name='update-product'),
    path('duplicate/<str:product_pk>/', views.duplicate_product, name='duplicate-product'),
    path('delete/<str:product_pk>/', views.delete_product, name='delete-product')
]