import os
from django.db import models
from django.db.models.fields import IntegerField
from django.core.validators import DecimalValidator
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from accounts.models import User

import shortuuid
from shortuuidfield import ShortUUIDField

from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFill, ResizeToFit
import pdb

# Create your models here.
class BaseProductsModel(models.Model):
    class Meta:
        abstract = True
        app_label = 'products'

# class Tag(BaseProductsModel):
#     name = models.CharField(max_length=55, null=True, blank=True)

#     def __str__(self):
#         return self.name

class Product(BaseProductsModel):
    # User manipulating the product (request.user — whoever is logged in)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE, related_name="shop_product")

    # Product ID (According to Shop)
    # product_id = models.PositiveIntegerField(null=True, default=0)

    # Product name and description
    name = models.CharField(max_length=30, null=True, blank=False)
    description = models.TextField(max_length=140, null=True, blank=True)

    # Stocks available
    made_to_order = models.BooleanField(null=True, blank=True, default=False)
    stock = models.PositiveIntegerField(null=True, blank=True, default=0)

    # Maximum number of orders
    no_order_limit = models.BooleanField(null=True, blank=True, default=False)
    orders = models.PositiveIntegerField(null=True, blank=True, default=0)

    instructions = models.CharField(max_length=140, null=True, blank=True, default="")

    # Product is active / available
    active = models.BooleanField(null=True, blank=True, default=True)

    # Number of orders sold
    sold = models.PositiveIntegerField(null=True, blank=True, default=0)

    def __str__(self):
        # if hasattr(self.user, "shop_info"):
        #     return f"{self.user.shop_info.shop_name} — {self.name}"
        # else:
        return f"{self.name}"

    # def save(self, *args, **kwargs):
    #     self.product_list = Product.objects.all().order_by('product_id')
    #     if len(self.product_list) == 0:
    #         self.product_id = 1
    #     else:
    #         self.product_id = self.product_list.last().product_id + 1
    #     super(Product, self).save()

    @property
    def is_made_to_order(self):
        return self.made_to_order is True

    @property
    def has_no_order_limit(self):
        return self.no_order_limit is True
    
    @property
    def get_images(self):
        return self.product_image.filter(active=True)

    @property
    def get_sizes(self):
        return self.product_sizes.all()
    
    @property
    def get_addons(self):
        return self.product_addons.all()

# Product Thumbnails
class ProductThumbnail(ImageSpec):
    processors = [ResizeToFill(1200, 500)]
    form = 'JPEG'
    options = {'quality': 80}

register.generator('products:thumbnail', ProductThumbnail)

# Carousel / Slider Images
class Carousel(ImageSpec):
    processors = [ResizeToFill(600, 176)]
    format = 'JPEG'
    options = {'quality': 80}

register.generator('products:carousel', Carousel)

def get_image_filename(instance, filename):
    path = "%Y/%m/%d/products/"
    name = instance.product.user.shop_info.shop_username
    return os.path.join(path, name)

# Class Image for implementation of inline formsets
class Image(BaseProductsModel):
    product = models.ForeignKey(Product, related_name="product_image", null=True, on_delete=models.CASCADE)
    name = models.CharField(_('image name'), max_length=80, null=True, blank=True)
    image = models.ImageField(_('image file'), upload_to="%Y/%m/%d/products/", null=True, blank=True)
    default = models.BooleanField(_('default image'), null=True, default=False)
    active = models.BooleanField(_('active image'), null=True, default=False)

    def __str__(self):
        if self.name and self.image:
            return f"[{self.product.name}] {self.name}"
        elif self.name:
            return f"{self.name}"
        else:
            return "No Images Found"

    @property
    def get_product_name(self):
        name = self.product.user.shop_info.shop_username + "—" + self.product.name
        name = name.lower()
        name = name.replace(" ", "_")
        name = name + f"_{self.id}"
        return name

    @property
    def get_default_image(self, product):
        return self.image_set.get(product=product, default=True)

# Class Size for implementation of inline formsets
class Size(BaseProductsModel):
    product = models.ForeignKey(Product, null=True, related_name="product_sizes", on_delete=models.CASCADE)
    size = models.CharField(max_length=125, null=True, blank=False)
    price_size = models.DecimalField(null=True, blank=False, max_digits=6, decimal_places=2)

    @property
    def size_to_json(self):
        return {
            "product": self.product,
            "size": self.size,
            "price_size": self.price_size,
        }

    @property
    def get_price_size(self):
        return float(self.price_size)

    def __str__(self):
        return f"[{self.product.name}] {self.size} (PHP {self.price_size})"

# Class Addon for implementation of inline formsets
class Addon(BaseProductsModel):
    product = models.ForeignKey(Product, null=True, related_name="product_addons", on_delete=models.CASCADE)
    addon = models.CharField(max_length=125, null=True, blank=False)
    price_addon = models.DecimalField(null=True, blank=False, max_digits=5, decimal_places=2)

    @property
    def addon_to_json(self):
        return {
            "product": self.product,
            "addon": self.addon,
            "price_addon": self.price_addon,
        }

    def __str__(self):
        return f"[{self.product.name}] {self.addon} (+ PHP {self.price_addon})"