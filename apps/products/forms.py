from django import forms
from django.forms import ModelForm, inlineformset_factory, BaseInlineFormSet
from django.forms.widgets import Textarea
from products.models import *
from django.forms import ValidationError as formError
from django.utils.translation import ugettext_lazy as _
from django.core.validators import DecimalValidator

# Initial Product Form
class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = [
            'name', 
            'description',
            'stock',
            'made_to_order',
            'orders',
            'no_order_limit',
            'instructions',
        ]

        widgets = {
            'name': forms.fields.TextInput(attrs={
                'class':'input default subtitle',
                'placeholder': 'Ex. Chocolate Chip Cookies'}),
            
            'description': forms.Textarea(attrs={
                'class':'input large-input default subtitle',
                'placeholder': 'Describe your product for your customers to see!'}),

            'made_to_order': forms.CheckboxInput(attrs={
                'class':'js-radio-input'}),

            'no_order_limit': forms.CheckboxInput(attrs={
                'class':'js-radio-input'}),

            'stock': forms.fields.NumberInput(attrs={
                'class':'input default disabled subtitle',
                'placeholder': '100'}),

            'orders': forms.fields.NumberInput(attrs={
                'class':'input default disabled subtitle',
                'placeholder': '10'}),
            
            'instructions': forms.Textarea(attrs={
                'class':'input large-input default subtitle',
                'placeholder': 'List them down here!'}),
        }

# FORMSET DRAFTS
# The formset for placing images
ImageFormset = inlineformset_factory(
                    Product,
                    Image,
                    can_delete=True,
                    fields=('image',),
                    widgets={
                        'image': forms.fields.ClearableFileInput(attrs={
                            'class':'js-image'})
                    },
                    extra=3,
                    max_num=3,
                )

# The formset for editing the size and prices that belong to a product
# Base formset for Size Formset
# class BaseSizeFormSet(BaseInlineFormSet):
#     def clean(self):
#         for form in self.forms:
#             # Validate Size
#             size = form.cleaned_data.get("size")
#             if not size:
#                 raise formError(_('You must enter a size name.'), code='blank_name')
#             # Validate Size Price
#             price = form.cleaned_data.get("price_size")

#             if price > 10000:
#                 raise formError(_('Price should not exceed PHP 10,000.'), code='max_range')
#             cleaned_data["price_size"] = price
#             return cleaned_data

SizeFormset = inlineformset_factory(
                    Product,
                    Size,
                    # formset=BaseSizeFormSet,
                    can_delete=True,
                    fields=('size', 'price_size'),
                    widgets={
                        'size': forms.fields.TextInput(attrs={
                            'class':'input default subtitle js-size-input',
                            'placeholder': 'Ex. 1 Dozen'}),

                        'price_size': forms.fields.TextInput(attrs={
                            'class':'input default subtitle js-size-price-input',
                            'placeholder': '400'}),
                    },
                    labels={
                        'size':'',
                        'price_size':'',
                    },
                    extra=1
                )

# Base formset for Addon Formset
# class BaseAddonFormSet(BaseInlineFormSet):
#     def clean_addon(self):
#         cleaned_data = super().clean()
#         addon = cleaned_data.get("addon")
#         if len(addon) == 0:
#             raise formError(_('Addon name is not filled.'), code='blank_name')
#         return addon

#     def clean_price_addon(self):
#         cleaned_data = super().clean()
#         price_addon = cleaned_data.get("price_addon")
#         if not DecimalValidator(price_addon):
#             raise formError(_('Addon price should be a valid Decimal.'), code='invalid_format')
#         if price > 10000:
#             raise formError(_('Addon price should not exceed PHP 10,000.'), code='max_range')
#         return price

AddonFormset = inlineformset_factory(
                    Product,
                    Addon,
                    # formset=BaseAddonFormSet,
                    can_delete=True,
                    fields=('addon', 'price_addon'),
                    widgets={
                        'addon': forms.fields.TextInput(attrs={
                            'class':'input default subtitle js-addon-input',
                            'placeholder': 'Additional Chocolate Chip'}),

                        'price_addon': forms.fields.TextInput(attrs={
                            'class':'input default subtitle js-addon-price-input',
                            'placeholder': '40'}),
                    },
                    labels={
                        'addon':'',
                        'price_addon':'',
                    }, 
                    extra=1
                )