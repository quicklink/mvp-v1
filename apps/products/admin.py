from django import forms
from django.contrib import admin
from .models import *

# Register your models here.
# def get_shop_and_product(self):
#     return f"{self.user.shop_info.shop_name} — {self.name}"

# Product.add_to_class("__str__", get_shop_and_product)

class ProductImageInline(admin.StackedInline):
    model = Image

class ProductSizeInline(admin.StackedInline):
    model = Size

class ProductAddonInline(admin.StackedInline):
    model = Addon

class CustomProductAdmin(admin.ModelAdmin):
    inlines = (ProductImageInline, ProductSizeInline, ProductAddonInline)
    ordering = ("user", "name", "description", "active")
    list_display = (
        "user",
        "name",
        "description",
        "active",
    )
    list_filter = ("user", "name", "active")
    
    fieldsets = (
        (None, {'fields': ('user', 'name', 'description', 'instructions')}),
        ('Stocks Available', {'fields': ('made_to_order','stock')}),
        ('Max Orders', {'fields': ('no_order_limit', 'orders')})
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('role', 'email', 'password1', 'password2'),
        }),
    )
    search_fields = ('user', 'name')
    filter_horizontal = ()

admin.site.register(Product, CustomProductAdmin)
admin.site.register(Image)
admin.site.register(Size)
admin.site.register(Addon)