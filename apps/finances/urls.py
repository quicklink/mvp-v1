from django.urls import path
from . import views

urlpatterns = [
    path('revenues/', views.revenues, name='revenues'),
]