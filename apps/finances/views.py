from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from accounts.decorators import allowed_users, unauthenticated_merchant
from accounts.models import User, ShopGeneralSettings
from orders.models import Order
from .models import Summary
from decimal import Decimal

# Create your views here.
@login_required(login_url='accounts:login')
@allowed_users(allowed_roles=[User.Types.MERCHANT, User.Types.QUICKIES, User.Types.ADMIN])
def revenues(request):
    shop = request.user
    if hasattr(shop, "shop_info"): settings = ShopGeneralSettings.objects.get(shop=shop.shop_info)
    else: settings = None
    summary, created = Summary.objects.get_or_create(shop=shop)
    orders = Order.objects.filter(shop=shop, complete=True).order_by('order_date')

    # Total Expected Revenue
    total_revenue = Decimal(0)
    for order in orders:
        if order.fees != 0:
            total_revenue += order.subtotal
        else:
            total = order.total - (order.total * Decimal(0.04))
            total_revenue += total
    summary.total_revenue = total_revenue

    # Payments Received
    received_orders = orders.filter(payment_status="RECEIVED")
    payments_received = Decimal(0)
    for order in received_orders:
        if order.fees != 0:
            payments_received += order.subtotal
        else:
            total = order.total - (order.total * Decimal(0.04))
            payments_received += total
    summary.payments_received = payments_received

    # Payments Receivables
    receivables_orders = orders.all().exclude(payment_status="RECEIVED")
    receivables = Decimal(0)
    for order in receivables_orders:
        if order.fees != 0:
            receivables += order.subtotal
        else:
            total = order.total - (order.total * Decimal(0.04))
            receivables += total
    summary.total_receivables = receivables
    summary.save()

    context = {"orders":orders, "settings":settings, "total_revenue":total_revenue, "payments_received":payments_received, "receivables":receivables}
    return render(request, 'finances/revenue.html', context)