from django.db import models

from accounts.models import User

# Create your models here.
class BaseFinancesModel(models.Model):
    class Meta:
        abstract = True
        app_label = 'finances'

class Summary(BaseFinancesModel):
    shop = models.ForeignKey(User, related_name="shop_summary", on_delete=models.SET_NULL, null=True)
    # Total Expected Revenue
    total_revenue = models.DecimalField(null=True, default=0, max_digits=15, decimal_places=2)

    # Payments Received
    payments_received = models.DecimalField(null=True, default=0, max_digits=15, decimal_places=2)

    # Accounts Receivables
    total_receivables = models.DecimalField(null=True, default=0, max_digits=15, decimal_places=2)