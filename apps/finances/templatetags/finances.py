from django import template
from decimal import Decimal

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_checkout(order):
    shop = order.shop
    subtotal = order.subtotal
    fees = order.fees
    shop_info = None
    settings = None
    if hasattr(shop, "shop_info"): 
        shop_info = shop.shop_info
        if hasattr(shop_info, "shop_general_settings"): settings = shop_info.shop_general_settings
    if settings and settings.shop_customer_fees:
        return subtotal + fees
    else:
        return subtotal

@register.filter
def get_fees(order):
    shop = order.shop
    subtotal = order.subtotal
    fees = order.fees
    shop_info = None
    settings = None
    if hasattr(shop, "shop_info"): 
        shop_info = shop.shop_info
        if hasattr(shop_info, "shop_general_settings"): settings = shop_info.shop_general_settings

    if settings.shop_customer_fees:
        return fees
    else:
        return subtotal * Decimal(0.04)

@register.filter
def get_total(order):
    shop = order.shop
    subtotal = order.subtotal
    shop_info = None
    settings = None
    if hasattr(shop, "shop_info"): 
        shop_info = shop.shop_info
        if hasattr(shop_info, "shop_general_settings"): settings = shop_info.shop_general_settings
        
    if settings.shop_customer_fees:
        return subtotal
    else:
        return subtotal - (subtotal * Decimal(0.04))