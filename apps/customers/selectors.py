import os
import environ
from django.conf import settings

import json
import shortuuid
from xendit import Xendit, Balance, EWallet, DirectDebit, DirectDebitPaymentMethodType
from accounts.models import User, BankAccount
from orders.models import Order

if not settings.LOCAL_DEV:
    import subprocess
    import ast
    
    def get_environ_vars():
        completed_process = subprocess.run(
            ['/opt/elasticbeanstalk/bin/get-config', 'environment'],
            stdout=subprocess.PIPE,
            text=True,
            check=True
        )
        return ast.literal_eval(completed_process.stdout)

def get_device_id(cookies, session):
    if 'device' in cookies:
        device = cookies['device']
    else:
        if 'device' in session:
            device = session['device']
        else:
            device = shortuuid.uuid()
            session['device'] = device
    return device

def get_customer(user, cookies, session):
    if user.is_authenticated:
        customer = user
    else:
        device = get_device_id(cookies, session)
        customer, created_customer = User.objects.get_or_create(device_id=device, role=User.Types.CUSTOMER)
    return customer

def get_order(user, shop):
    order = Order.objects.filter(user=user, shop=shop, complete=False)
    if order.exists() and len(order) == 1: order = order.first() # If only one order exists, get it
    elif order.exists() and len(order) > 1: order = order.last() # If more than one order exists, get the last one
    else: order = Order.objects.create(user=user, shop=shop, complete=False) # If no order exists, create one
    return order

def get_module():
    if not settings.LOCAL_DEV:
        if "DJANGO_SETTINGS_MODULE" in os.environ:
            module = os.environ.get("DJANGO_SETTINGS_MODULE")
        else:
            env_vars = get_environ_vars()
            module = env_vars.get("DJANGO_SETTINGS_MODULE")
    else:
        env = environ.Env()
        env.read_env()
        module = env("DJANGO_SETTINGS_MODULE")
    return module

def get_rebrandly_api_key():
    if not settings.LOCAL_DEV:
        if "REBRANDLY_API_KEY" in os.environ:
            api_key = os.environ.get("REBRANDLY_API_KEY")
        else:
            env_vars = get_environ_vars()
            api_key = env_vars.get("REBRANDLY_API_KEY")
    else:
        env = environ.Env()
        env.read_env()
        api_key = env("REBRANDLY_API_KEY")
    return api_key

def get_rebrandly_shortlink_domain():
    if not settings.LOCAL_DEV:
        if "REBRANDLY_SUBDOMAIN" in os.environ:
            api_key = os.environ.get("REBRANDLY_SUBDOMAIN")
        else:
            env_vars = get_environ_vars()
            api_key = env_vars.get("REBRANDLY_SUBDOMAIN")
    else:
        env = environ.Env()
        env.read_env()
        api_key = env("REBRANDLY_SUBDOMAIN")
    return api_key

def get_vonage_api_key():
    if not settings.LOCAL_DEV:
        if "VONAGE_API_KEY" in os.environ:
            vonage_api_key = os.environ.get("VONAGE_API_KEY")
        else:
            env_vars = get_environ_vars()
            vonage_api_key = env_vars.get("VONAGE_API_KEY")
    else:
        env = environ.Env()
        env.read_env()
        vonage_api_key = env("VONAGE_API_KEY")
    return vonage_api_key

def get_vonage_secret_key():
    if not settings.LOCAL_DEV:
        if "VONAGE_SECRET_KEY" in os.environ:
            vonage_secret_key = os.environ.get("VONAGE_SECRET_KEY")
        else:
            env_vars = get_environ_vars()
            vonage_secret_key = env_vars.get("VONAGE_SECRET_KEY")
    else:
        env = environ.Env()
        env.read_env()
        vonage_secret_key = env("VONAGE_SECRET_KEY")
    return vonage_secret_key

def get_sendgrid_api_key():
    if not settings.LOCAL_DEV:
        if "SENDGRID_API_KEY" in os.environ:
            sg_api_key = os.environ.get("SENDGRID_API_KEY")
        else:
            env_vars = get_environ_vars()
            sg_api_key = env_vars.get("SENDGRID_API_KEY")
    else:
        env = environ.Env()
        env.read_env()
        sg_api_key = env("SENDGRID_API_KEY")
    return sg_api_key

def get_external_id(order):
    module = get_module()
    external_id_prefix = str(module).split('.')[-1]

    if str(module) == 'config.settings.dev':
        external_id = "ql_" + str(external_id_prefix) + "_card-charge_" + str(shortuuid.uuid())
    else:
        external_id = "ql_" + str(external_id_prefix) + "_card-charge_" + str(order.id)
    return external_id

def get_available_accounts(slug, customer):
    if slug == "card":
        available_banks = ["DC/CC"]
    elif slug == "bank":
        available_banks = [
            "BA_BPI",
            "BA_BDO",
            "BA_UBP",
        ]
    elif slug == "ewallet":
        available_banks = [
            "PH_GCASH",
            "PH_PAYMAYA",
            "PH_GRABPAY",
        ]
    available_accounts = BankAccount.objects.filter(user=customer, bank_name__in=available_banks).order_by('id')
    return available_accounts

def get_xendit_read_api_key():
    if not settings.LOCAL_DEV:
        if "XENDIT_READ_API_KEY" in os.environ:
            api_key = os.environ.get("XENDIT_READ_API_KEY")
        else:
            env_vars = get_environ_vars()
            api_key = env_vars.get("XENDIT_READ_API_KEY")
    else:
        env = environ.Env()
        env.read_env()
        api_key = env("XENDIT_READ_API_KEY")
    return api_key

def get_xendit_write_api_key():
    if not settings.LOCAL_DEV:
        if "XENDIT_WRITE_API_KEY" in os.environ:
            api_key = os.environ.get("XENDIT_WRITE_API_KEY")
        else:
            env_vars = get_environ_vars()
            api_key = env_vars.get("XENDIT_WRITE_API_KEY")
    else:
        env = environ.Env()
        env.read_env()
        api_key = env("XENDIT_WRITE_API_KEY")
    return api_key

def get_xendit_instance(read_or_write):
    if read_or_write == "read": api_key = get_xendit_read_api_key()
    elif read_or_write == "write": api_key = get_xendit_write_api_key()
    xendit_instance = Xendit(api_key=api_key)
    return xendit_instance

def get_xendit_linked_accounts(linked_account_token_id):
    xendit_instance = get_xendit_instance("read")
    DirectDebit = xendit_instance.DirectDebit

    accessible_accounts = DirectDebit.get_accessible_accounts_by_token(
        linked_account_token_id=linked_account_token_id
    )
    json_response = json.loads(str(accessible_accounts))
    return json_response