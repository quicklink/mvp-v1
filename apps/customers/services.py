import os
import environ
import requests
from django.conf import settings
from django.shortcuts import redirect
from django.contrib import messages

import json
import shortuuid
from django.http import JsonResponse, HttpResponseServerError
from django.core import serializers
from decimal import Decimal

from accounts.models import ShopInformation, CustomerInformation, Address, CustomerNotification, BankAccount
from products.models import Product, Size, Addon
from orders.models import OrderItem, OrderInformation

import vonage
from sendgrid import SendGridAPIClient
from xendit import DirectDebitPaymentMethodType
from .selectors import get_rebrandly_api_key, get_rebrandly_shortlink_domain, get_sendgrid_api_key, get_vonage_api_key, get_vonage_secret_key, get_xendit_instance, get_module

if not settings.LOCAL_DEV:
    import subprocess
    import ast
    
    def get_environ_vars():
        completed_process = subprocess.run(
            ['/opt/elasticbeanstalk/bin/get-config', 'environment'],
            stdout=subprocess.PIPE,
            text=True,
            check=True
        )
        return ast.literal_eval(completed_process.stdout)

def add_to_cart(shop, order, product, post_data, form_data):
    # Check if an order item with the same product already exists
    size = Size.objects.get(id=post_data.get('size'))
    if 'addons' in post_data: 
        addons_list = [addon for addon in post_data.getlist('addons')]
        addons = Addon.objects.filter(pk__in=addons_list)
        product_order, created = OrderItem.objects.get_or_create(
            order=order, 
            product=product, 
            size=size,
            addons__in=addons
        )
    else: 
        product_order, created = OrderItem.objects.get_or_create(
            order=order, 
            product=product, 
            size=size
        )

    if created:
        if 'addons' in post_data:
            for addon in addons:
                product_order.addons.add(addon)

        product_order.instructions = form_data.get('instructions')
        product_order.quantity = form_data.get('quantity')
        product_order.total = product_order.get_total
        product_order.save()

        order.subtotal += product_order.total
        if shop.shop_general_settings.shop_customer_fees == True:
            order.fees = order.subtotal * Decimal(0.04)
        else:
            order.fees = 0
    else:
        initial_total = product_order.get_total
        # Check if instructions are the same with another order
        if product_order.instructions == form_data.get('instructions'):
            product_order.quantity += form_data.get('quantity')
        else:
            product_order.id = None
            product_order.instructions = form_data.get('instructions')
            product_order.quantity = form_data.get('quantity')
            
        product_order.total = product_order.get_total
        product_order.save()

        order.subtotal -= initial_total
        order.subtotal += product_order.total
        if shop.shop_general_settings.shop_customer_fees == True:
            order.fees = order.subtotal * Decimal(0.04)
        else:
            order.fees = 0

    order.total = order.subtotal + order.fees
    order.save()
    return product_order

def edit_cart_item(shop, order, item, post_data, form_data):
    size_id = post_data.get('size')
    size = Size.objects.get(id=size_id)
    item.size = size

    if 'addons' in post_data:
        item.addons.clear()
        addons_list = [addon for addon in post_data.getlist('addons')]
        addons = Addon.objects.filter(id__in=addons_list)
        for addon in addons:
            item.addons.add(addon)
    else:
        item.addons.clear()

    instructions = form_data.get('instructions')
    item.instructions = instructions

    quantity = form_data.get('quantity')
    prev_total = item.total

    item.quantity = quantity
    item.total = item.get_total
    item.save()

    order.subtotal = order.subtotal - prev_total + item.total

    if shop.shop_general_settings.shop_customer_fees == True:
        order.fees = order.subtotal * Decimal(0.04)
    else:
        order.fees = 0

    order.total = order.subtotal + order.fees
    order.save()

def update_item(request):
    data = request.GET.copy()
    item = data.get("item", "")
    product = data.get("product", "")
    username = data.get("username", "")
    action = data.get("action", "")

    shop = ShopInformation.objects.get(shop_username=username)
    product_order = OrderItem.objects.get(id=item)
    product = Product.objects.get(id=product)
    order = product_order.order
    items = order.orderitem_set.all()

    if action == "increase":
        if product.made_to_order:
            product_order.quantity = (product_order.quantity + 1)
        else:
            if (product_order.quantity + 1) <= product.stock:
                product_order.quantity = (product_order.quantity + 1)
            else:
                res = json.loads(json.dumps({"success": False, "error": "You've reached the maximum amount of stocks available for this product."}))
                return JsonResponse(data=res, safe=False)
    elif action == "decrease":
        product_order.quantity = (product_order.quantity - 1)

    product_order.total = product_order.get_total
    product_order.save()

    if product_order.quantity <= 0:
        product_order.delete()
    
    order.subtotal = 0
    for item in items:
        order.subtotal += item.total

    if shop.shop_general_settings.shop_customer_fees == True:
        order.fees = order.subtotal * Decimal(0.04)
    else:
        order.fees = 0

    order.total = order.subtotal + order.fees
    order.save()

    res = serializers.serialize('json', [order, ])
    return JsonResponse(data=res, safe=False)

def save_checkout(form, customer, sms, email, order, items):
    if form.cleaned_data.get('contact_number'): contact_number = ("+63" + form.cleaned_data.get('contact_number')).replace(' ', '')
    else: contact_number = None
    
    if form.cleaned_data.get('facebook'): facebook = form.cleaned_data.get('facebook')
    else: facebook = None

    if form.cleaned_data.get('instagram'): instagram = form.cleaned_data.get('instagram')
    else: instagram = None
    
    session_sender, sender_created = CustomerInformation.objects.get_or_create(
        customer=customer,
        customer_name=form.cleaned_data.get('name'),
        customer_email=form.cleaned_data.get('email'),
        customer_contact_number=contact_number,
        facebook=facebook,
        instagram=instagram
    )
    if sender_created: session_sender.save()

    session_address, address_created = Address.objects.get_or_create(
        user=customer,
        line1=form.cleaned_data.get('line1'),
        line2=form.cleaned_data.get('line2'),                
        city=form.cleaned_data.get('city'),
        province=form.cleaned_data.get('province'),
        postal_code=form.cleaned_data.get('postal_code'),
    )
    if address_created: session_address.save()

    session_notifications, notifs_created = CustomerNotification.objects.get_or_create(
        user=customer,
        sms=sms,
        email=email
    )
    if notifs_created: session_notifications.save()

    order_info, created = OrderInformation.objects.get_or_create(order=order)
    order_info.session_sender = session_sender
    order_info.session_address = session_address
    order_info.session_notifications = session_notifications
    order_info.save()

    order.delivery_date = form.cleaned_data.get('delivery_date')
    order.delivery_option = form.cleaned_data.get('delivery_options')
    order.save()

    for item in items:
        item.delivery_date = order.delivery_date.date()
        item.save()

    res = serializers.serialize('json', [order, ])
    return JsonResponse(data=res, safe=False)

def save_ewallet_session_payment(form, customer, order_info, initial):
    if initial:
        session_payment = BankAccount.objects.filter(
            user=customer,
            bank_name=form.cleaned_data.get('bank_name'),
            cardholder_name=None,
            account_number=None,
            exp_date=None,
            cvv=None
        )
        if not session_payment.exists():
            session_payment = BankAccount.objects.create(
                user=customer,
                bank_name=form.cleaned_data.get('bank_name'),
                cardholder_name=None,
                account_number=None,
                exp_date=None,
                cvv=None
            )
            session_payment.save()
        else:
            session_payment = session_payment.first()
    else:
        print("Linked account!")
    order_info.session_payment = session_payment
    order_info.save()
    return session_payment

def save_bank_session_payment(form, customer, order_info, initial):
    if initial:
        session_payment = BankAccount.objects.filter(
            user=customer,
            bank_name=form.cleaned_data.get('bank_name'),
            cardholder_name=None,
            account_number=None,
            exp_date=None,
            cvv=None
        )
        if not session_payment.exists():
            session_payment = BankAccount.objects.create(
                user=customer,
                bank_name=form.cleaned_data.get('bank_name'),
                cardholder_name=None,
                account_number=None,
                exp_date=None,
                cvv=None
            )
            session_payment.save()
        else:
            session_payment = session_payment.first()
    else:
        print("Linked account!")
    order_info.session_payment = session_payment
    order_info.save()
    return session_payment

def save_card_session_payment(token_data, customer, order_info):
    cardholder_name = token_data['billing_details']['given_names'] + ' ' + token_data['billing_details']['surname']
    account_number = token_data['card_number']
    exp_month = token_data['card_exp_month']
    exp_year = token_data['card_exp_year']
    exp_date = exp_month + "/" + exp_year[:-2]
    cvv = token_data['card_cvn']

    session_payment, created = BankAccount.objects.get_or_create(
        user=customer,
        bank_name='DC/CC',
        cardholder_name=cardholder_name,
        account_number=account_number,
        exp_date=exp_date,
        cvv=cvv
    )
    order_info.session_payment = session_payment
    order_info.save()
    return session_payment

def create_xendit_customer(reference_id, order, customer, WriteCustomer):
    customer_info = order.get_sender_details
    email = customer_info.get("email", "no-reply@quicklink.ph")
    given_names = customer_info.get("given_names", "Quicklink")
    surname = customer_info.get("surname", "Philippines")
    phone_number = customer_info.get("phone_number", "+639166558865")
    phone_number = f"+(63)({phone_number[3:]})"
    
    try:
        xendit_customer = WriteCustomer.create_customer(
            reference_id=reference_id,
            email=email,
            given_names=given_names,
            surname=surname,
            phone_number=phone_number
        )
    except:
        xendit_customer = WriteCustomer.create_customer(
            reference_id=reference_id,
            email="no-reply@quicklink.ph",
            given_names="Quicklink",
            surname="Philippines",
            phone_number="+(63)(9166558865)"
        )

    json_response = json.loads(str(xendit_customer))
    customer.xendit_customer_id = json_response['id']
    customer.save()

    return xendit_customer

def get_xendit_customer(reference_id, order, customer, ReadCustomer, WriteCustomer):
    try:
        xendit_customer = ReadCustomer.get_customer_by_ref_id(reference_id=reference_id)
        if xendit_customer:
            try: xendit_customer = json.loads(str(xendit_customer[0]))
            except: xendit_customer = json.loads(str(xendit_customer))
            customer.xendit_customer_id = xendit_customer['id']
            customer.save()
        else:
            xendit_customer = create_xendit_customer(reference_id, order, customer, WriteCustomer)
    except:
        xendit_customer = create_xendit_customer(reference_id, order, customer, WriteCustomer)

    return xendit_customer

def get_or_create_xendit_customer(ReadCustomer, WriteCustomer, customer, order, customer_id, account_number_last_four):
    module = get_module()
    reference_id_prefix = str(module).split('.')[-1]
    if account_number_last_four != False:
        if str(module) == 'config.settings.dev': reference_id = "ql_" + str(reference_id_prefix) + "_customer_" + str(customer_id) + '_' + str(account_number_last_four)
        elif str(module) == 'config.settings.production': reference_id = "ql_customer_" + str(customer_id) + '_' + str(account_number_last_four)
        else: reference_id = "ql_" + str(reference_id_prefix) + "_customer_" + str(customer_id) + '_' + str(account_number_last_four)
    else: 
        if str(module) == 'config.settings.dev': reference_id = "ql_" + str(reference_id_prefix) + "_customer_" + str(customer_id)
        elif str(module) == 'config.settings.production': reference_id = "ql_customer_" + str(customer_id)
        else: reference_id = "ql_" + str(reference_id_prefix) + "_customer_" + str(customer_id)

    # Create Xendit Customer
    # API Reference: https://developers.xendit.co/api-reference/?python#customers
    try: xendit_customer = get_xendit_customer(reference_id, order, customer, ReadCustomer, WriteCustomer)
    except: xendit_customer = create_xendit_customer(reference_id, order, customer, WriteCustomer)
    return xendit_customer

def ewallet_linked_account_tokenization(DirectDebit, customer, payment, current_url, success_redirect_url):
    # Initialize Linked Account Tokenization
    # API Reference: https://developers.xendit.co/api-reference/#initialize-linked-account-tokenization
    linked_account = DirectDebit.initialize_tokenization(
        customer_id=customer.xendit_customer_id,
        channel_code=payment.bank_name,
        properties={
            'success_redirect_url':success_redirect_url,
            'failure_redirect_url':current_url
        },
    )   
    json_response = json.loads(str(linked_account))
    return json_response

def create_card_charge(CreditCard, token_id, auth_id, external_id, amount, cvv):
    # If capture = False, you need to capture charge (default: capture = True)
    # See more in Xendit Docs Here: https://developers.xendit.co/api-reference/#capture-charge
    charge = CreditCard.create_charge(
        token_id=token_id,
        authentication_id=auth_id,
        external_id=external_id,
        amount=amount,
        card_cvn=cvv,
        currency="PHP",
    )
    return charge

def raise_card_errors(request, payment_response):
    if payment_response == "EXPIRED_CARD":
        messages.error(request, "The card you used seems to have expired. Please try a different card.")
    elif payment_response == "CARD_DECLINED":
        messages.error(request, 'Your card was declined. Please try a different card.')
    elif payment_response == "INSUFFICIENT_BALANCE":
        messages.error(request, 'The card you used does not have enough balance to complete the payment. Please try a different card.')
    elif payment_response == "STOLEN_CARD":
        messages.error(request, 'The card you used has been marked as stolen.')
    elif payment_response == "INACTIVE_CARD":
        messages.error(request, 'The card you used is inactive. Please try a different card.')
    elif payment_response == "INVALID_CVN":
        messages.error(request, 'CVV is incorrect. Please check again, or use a different card.')
    elif payment_response == "BIN_BLOCK" or payment_response == "BLOCKED_BY_ACQUIRER":
        messages.error(request, 'Your card was blocked by its provider. Please try a different card.')
    elif payment_response == "PROCESSOR_ERROR":
        messages.error(request, 'There was a problem with our credit card service provider. Please try again later or try another payment method.')
    elif payment_response == "CAPTURE_AMOUNT_LIMIT_ERROR":
        messages.error(request, "The total amount exceeds the platform's limit of Php 700,000. Please make bawas.")
    elif payment_response == "TEMPORARY_SYSTEM_ERROR":
        return True
    else:
        messages.error(request, "Our credit card service is currently down. Please try another payment method.")
    return False

def create_ewallet_charge(EWallet, order, session_payment, success_redirect_url, current_url, customer_info):
    module = get_module()
    reference_id_prefix = str(module).split('.')[-1]
    if str(module) == 'config.settings.dev': reference_id = "ql_" + str(reference_id_prefix) + "_e-wallet_" + str(shortuuid.uuid())
    elif str(module) == 'config.settings.production': reference_id = "ql_e-wallet_" + str(order.id)
    else: reference_id = "ql_" + str(reference_id_prefix) + "_e-wallet_" + str(order.id)

    amount = float(order.total)
    channel_code = session_payment.bank_name
    customer_id = customer_info.xendit_customer_id

    if not settings.LOCAL_DEV:
        success_redirect_url = f"https:{success_redirect_url}"
        current_url = f"https:{current_url}"
    else:
        success_redirect_url = f"http:{success_redirect_url}"
        current_url = f"http:{current_url}"

    try:
        if channel_code == "PH_PAYMAYA":
            channel_properties = {
                "success_redirect_url": success_redirect_url,
                "failure_redirect_url": current_url,
                "cancel_redirect_url": current_url
            }
        else:
            channel_properties = {
                "success_redirect_url": success_redirect_url,
                "failure_redirect_url": current_url,
            }

        ewallet_charge = EWallet.create_ewallet_charge(
            reference_id=reference_id,
            currency="PHP",
            amount=amount,
            checkout_method="ONE_TIME_PAYMENT",
            channel_code=channel_code,
            channel_properties=channel_properties,
            customer_id=customer_id,
        )
        return ewallet_charge
    except Exception as e:
        print(e)
        return(str(e))

def raise_ewallet_errors(request, payment_response):
    if payment_response.lower() == "payment request failed because this specific payment channel has not been activated. please activate the payment channel via your dashboard or our customer service.":
        messages.error(request, "Our e-wallet service is currently down. Please try another payment method.")
    elif payment_response.lower() == "the payment channel requested is currently experiencing unexpected issues. the ewallet provider will be notified to resolve this issue." or payment_response.lower() == "an unexpected error occurred, our team has been notified and will troubleshoot the issue.":
        messages.error(request, 'Our payment service provider is experiencing some errors. We will immediately notify them regarding this error. Please try a different method.')
    elif payment_response.lower() == "customer's account has been restricted by ewallet provider" or payment_response.lower() == "invalid merchant credentials" :
        messages.error(request, 'Something went wrong with your e-wallet provider. Please use a different account, or try contacting your provider.')
    elif payment_response.lower() == "customer declined the payment request":
        messages.error(request, "Uh oh! The payment request was declined. Please try again!")
    elif payment_response.lower() == "customer's account detail is invalid":
        messages.error(request, "Uh oh! The information you provided seems to be invalid. Please try again, or use a different account!")
    elif payment_response.lower() == "customer's account has reached maximum transaction limit":
        messages.error(request, "This transaction is above the limit set on your account. Please try a different account.")
    elif payment_response.lower() == "customer's device cannot be reached.":
        messages.error(request, "Your e-wallet device cannot be reached at the moment. Please try again later, or use a different account.")
    elif payment_response.lower() == "ewallet provider service is experiencing unexpected issues":
        messages.error(request, "Something went wrong with your e-wallet provider. We will immediately notify them regarding this error. Please try a different payment method.")
    elif payment_response.lower() == "insufficient balance on customer's wallet":
        messages.error(request, "The account you used does not have enough balance to complete the payment. Please try a different account.")
    elif payment_response.lower() == "customer's account has not been activated":
        messages.error(request, "The account you used has not been activated. You may resolve this by heading to your e-wallet app, or by using a different account or payment method.")
    else:
        messages.error(request, payment_response)
    return False

def initialize_bank_transfer(DirectDebit, customer, payment, failure_redirect_url, success_redirect_url):
    customer_id = customer.xendit_customer_id
    channel_code = payment.bank_name
    linked_account = DirectDebit.initialize_tokenization(
        customer_id=customer_id,
        channel_code=channel_code,
        properties={
            'success_redirect_url':success_redirect_url,
            'failure_redirect_url':failure_redirect_url
        },
    )
    json_response = json.loads(str(linked_account))
    return json_response

def create_bank_transfer_payment(customer_info, ba_la_id, order):
    # try:
    #     payment_method = DirectDebit.get_payment_methods_by_customer_id(
    #         customer_id=customer_info.xendit_customer_id,
    #     )
    #     payment_method_response = json.loads(str(payment_method))
    #     payment_method_id = payment_method_response[select_saved_card[1]]['id']
    # except:
    #     payment_method = DirectDebit.create_payment_method(
    #         customer_id=customer_info.xendit_customer_id,
    #         type=DirectDebitPaymentMethodType.BANK_ACCOUNT,
    #         properties={'id': ba_la_id},
    #     )
    #     payment_method_response = json.loads(str(payment_method))
    #     payment_method_id = payment_method_response['id']

    # if order_info.session_payment.bank_namstr(e) == 'BA_BPI':
    #     enable_otp = True
    # elif order_info.session_payment.bank_namstr(e) == 'BA_UBP':
    #     enable_otp = False

    read_xendit_instance = get_xendit_instance("read")
    write_xendit_instance = get_xendit_instance("write")
    ReadCustomers = read_xendit_instance.DirectDebit
    DirectDebit = write_xendit_instance.DirectDebit
    bank_name = order.order_information.session_payment.bank_name

    payment_method = DirectDebit.create_payment_method(
        customer_id=customer_info.xendit_customer_id,
        type=DirectDebitPaymentMethodType.BANK_ACCOUNT,
        properties={'id': ba_la_id},
    )
    payment_method_response = json.loads(str(payment_method))
    payment_method_id = payment_method_response['id']
    # except:
    #     payment_methods = ReadCustomers.get_payment_methods_by_customer_id(
    #         customer_id=customer_info.xendit_customer_id
    #     )
    #     payment_methods_response = json.loads(str(payment_methods))
    #     for method in payment_methods_response:
    #         if method["properties"]["channel_code"] == bank_name and method["status"] == "ACTIVE":
    #             payment_method_id = method["id"]

    module = get_module()
    payment_reference_id_prefix = str(module).split('.')[-1]
    if str(module) == 'config.settings.dev': payment_reference_id = "ql_" + str(payment_reference_id_prefix) + "_bank-transfer_" + str(shortuuid.uuid())
    elif str(module) == 'config.settings.production': payment_reference_id = "ql_bank-transfer_" + str(order.id)
    else: payment_reference_id = "ql_" + str(payment_reference_id_prefix) + "_bank-transfer_" + str(order.id)

    try:
        idempotency_key = str(shortuuid.uuid())
        callback_url = "https://discord.com/api/webhooks/834197133728022559/IHymIcAJdY-OhrbJL-Siq4TYhfLzql51qTjZ1rSknMZyNXJx-MW50YBqSGM-2geb2Fsz"
        payment = DirectDebit.create_payment(
            idempotency_key=idempotency_key,
            reference_id=payment_reference_id,
            payment_method_id=payment_method_id,
            currency='PHP',
            amount=float(order.total),
            callback_url=callback_url
        )
        payment_response = json.loads(str(payment))
        return payment_response
    except Exception as e:
        print(e)
        return(str(e))

def raise_bank_transfer_errors(request, payment_response):
    if payment_response.lower() == "channel unavailable error":
        messages.error(request, 'Our bank account service is currently down. Please try another payment method.')
    elif payment_response.lower() == "transaction is above the maximum cumulative transaction limit set by the channel.":
        messages.error(request, 'This transaction is above the limit set on your account. Please try a different account.')
    elif payment_response.lower() == "there is insufficient balance in the end-customer’s account to complete the transaction.":
        messages.error(request, 'The account you used does not have enough balance to complete the payment. Please try a different account.')
    elif payment_response.lower() == "sending otp failed.":
        messages.error(request, "Your OTP failed to deliver. Please try again.")
    elif payment_response.lower() == "your card is blocked or disabled.":
        messages.error(request, "Your account is blocked. Please contact your bank, or try a different account.")
    elif payment_response.lower() == "payment method not found":
        messages.error(request, "The account you entered seems to be invalid. Please check again, or try a different account.")
    elif payment_response.lower() == "invalid payment method error":
        messages.error(request, "Your account seems to have expired. Please try a different account.")
    else:
        messages.error(request, "Couldn't process payment. Please try again.")
    return False

def validate_bank_transfer_otp(request, otp_inputs, payment):
    """ 
    RESPONSE CODES
    1 - True (Validated)
    2 - Can Repeat
    0 - False (Not Validated)
    """

    otp_code_list = []
    for i in otp_inputs:
        index = f'otp_code_{i}'
        otp_code_list.append(request.POST.get(index))
    direct_debit_id = str(payment['id'])
    otp_code = ''.join(otp_code_list)
    
    try:
        xendit_instance = get_xendit_instance("write")
        DirectDebit = xendit_instance.DirectDebit
        payment = DirectDebit.validate_payment_otp(
            direct_debit_id=direct_debit_id,
            otp_code=otp_code,
        )
        return 1
    except Exception as e:
        print(e)
        safe = [
            "OTP provided is invalid",
            "OTP provided has expired",
            "The source payment method doesn't have enough balance to complete the transaction"
        ]
        if str(e) in safe:
            if str(e) == "OTP provided is invalid":
                messages.error(request, 'Incorrect OTP! Please try again.')
            elif str(e) == "OTP provided has expired":
                messages.error(request, 'Your OTP has expired! Please click Resend OTP and try again.')
            elif str(e) == "The source payment method doesn't have enough balance to complete the transaction":
                messages.error(request, 'The account you used does not have enough balance to complete the payment. Please rebalance and try again.')
            return 2
        else:
            if str(e) == "Provided direct_debit_id does not exist":
                messages.error(request, "Couldn't process payment. Please try again.")
            elif str(e) == "Reached the channel’s allowed maximum attempts for OTP verification":
                messages.error(request, "You've reached the maximum OTP attempts. Please try again.")
            elif str(e) == "The source account is blocked and cannot be accessed.":
                messages.error(request, "The account you used is blocked and cannot be accessed. Please try using a different account.")
            elif str(e) == "The amount for the transaction exceeds the account's aggregated limits set by the partner bank.":
                messages.error(request, "The amount for the transaction exceeds your account's limits. Please try using a different account.")
            elif str(e) == "The request is a duplicate of an already processed linked account token that has been successfully completed.":
                return 1
            elif str(e) == "The request is a duplicate of an already processed linked account token that has failed.":
                messages.error(request, "Couldn't process payment. Please try again.")
            else:
                messages.error(request, "Couldn't process payment. Please try again.")
            return 0

def complete_order(request, order, items, url):
    for item in items:
        product = Product.objects.get(id=item.product.id)
        if not product.made_to_order:
            if item.quantity <= product.stock:
                product.stock = product.stock - item.quantity
            else:
                messages.request(f"You've ordered more than the stock remaining for {product.name}.")
                return False
        product.sold = product.sold + item.quantity
        product.save()

    order.order_quantity = items.count()

    if not order.shortlink:
        module = get_module()
        slashtag_prefix = str(module).split('.')[-1]
        if slashtag_prefix == 'production':
            slashtag = "ql_" + order.get_id_with_leading_zeroes
            title = "Quicklink Order Invoice #" + slashtag
        else:
            slashtag = "ql_" + slashtag_prefix + "_order_" + order.get_id_with_leading_zeroes
            title = f"Quicklink {str(module).capitalize()} Order Invoice #" + slashtag

        linkRequest = {
            "destination": url,
            "domain": { "fullName": get_rebrandly_shortlink_domain() },
            "slashtag": slashtag,
            "title": title
        }

        requestHeaders = {
            "Content-type": "application/json",
            "apikey": get_rebrandly_api_key(),
            "workspace": "e15cc4543df7480aa8b63f4ffb474316"
        }

        try:
            r = requests.post("https://api.rebrandly.com/v1/links", 
                data = json.dumps(linkRequest),
                headers=requestHeaders
            )
        except Exception as e:
            r = {"status_code":"ERROR", "message":e}
            print(e)

        if (r.status_code == requests.codes.ok):
            link = r.json()
            order.shortlink = link["shortUrl"]
        else:
            messages.error(request, "Shortlink cannot be saved. Please contact us at customers@quicklink.ph!")
            order.shortlink = url

    # SEND NOTIFICATIONS 
    vonage_api_key = get_vonage_api_key()
    vonage_secret_key = get_vonage_secret_key()
    sg_api_key = get_sendgrid_api_key()

    order_info = order.order_information
    name = order_info.session_sender.customer_name
    first_name = name.split(" ")[0]

    try: email = order.shop.shop_info.shop_email
    except: email = order.shop.email

    email_message = f"You've just received a new order from {name}. Your order has been noted down in your Quicklink dashboard! You can also view its details through this link: {order.shortlink}"

    sg_data = {
        "from": {
            "email":"josh@quicklink.ph"
        },
        "personalizations": [
            {
                "to":[
                    {
                        "email":email
                    },
                ],
                "dynamic_template_data": {
                    "subject": f"New Quicklink Order (#{order.get_id_with_leading_zeroes})",
                    "first_name": order.shop.shop_info.shop_name,
                    "body": email_message,
                    "link": order.shortlink
                }
            }
        ],
        "template_id":"d-4b20ca3dbb524161bbd00601d7c6ed69"
    }
    sg_data = json.loads(json.dumps(sg_data))

    try:
        sg = SendGridAPIClient(api_key=sg_api_key)
        response = sg.client.mail.send.post(request_body=sg_data)
        messages.success(request, f"Your order has been noted! Please save the link to stay updated on its status.")
    except Exception as e:
        messages.error(request, f"An error occurred while notifying the shop on your order. Please contact the shop to confirm your order.")
    try:
        order.complete = True
        order.save()
        return True
    except:
        messages.request(f"An error occurred during order completion. Please try again.")
        return False

    # order_info = order.order_information
    # name = order_info.session_sender.customer_name
    # first_name = name.split(" ")[0]
    # # contact_number = order_info.session_sender.customer_contact_number
    # # contact_number = contact_number.split("+")[1]
    # contact_number = "639166558865"
    # email = order.shop_info.customer_email
    # notifications = order_info.session_notifications
    # sms_message = f"Hey, {first_name}! Thank you for ordering from {order.shop.shop_info.shop_name}. Your order has been noted down, we'll make sure to update you on its status! You can also view its status through this link: {order.shortlink} "
    # email_message = f"Thank you for ordering from {order.shop.shop_info.shop_name}. Your order has been noted down, we'll make sure to update you on its status! You can also view its status through this link: {order.shortlink} " 

    # if notifications.sms:
    #     client = vonage.Client(key=vonage_api_key, secret=vonage_secret_key)
    #     sms = vonage.Sms(client)
    #     responseData = sms.send_message(
    #         {
    #             "from": "Vonage APIs",
    #             "to": contact_number,
    #             "text": sms_message,
    #         }
    #     )

    #     if responseData["messages"][0]["status"] == "0":
    #         messages.success(request, f"A SMS notification regarding the status for Order #{order.get_id_with_leading_zeroes} has successfully been sent to your mobile number.")
    #     else:
    #         messages.error(request, f"An error occurred while sending a SMS notification for Order #{order.get_id_with_leading_zeroes}. Please try again later or contact us at customers@quicklink.ph.")
    # elif notifications.email:
    #     sg_data = {
    #         "from": {
    #             "email":"josh@quicklink.ph"
    #         },
    #         "personalizations": [
    #             {
    #                 "to":[
    #                     {
    #                         "email":email
    #                     },
    #                 ],
    #                 "dynamic_template_data": {
    #                     "subject": f"New Quicklink Order (#{order.get_id_with_leading_zeroes})",
    #                     "first_name": first_name,
    #                     "body": email_message,
    #                     "link": order.shortlink
    #                 }
    #             }
    #         ],
    #         "template_id":"d-4b20ca3dbb524161bbd00601d7c6ed69"
    #     }
    #     sg_data = json.loads(json.dumps(sg_data))

    #     try:
    #         sg = SendGridAPIClient(api_key=sg_api_key)
    #         response = sg.client.mail.send.post(request_body=sg_data)
    #         messages.success(request, f"A notification regarding the status for Order #{order.get_id_with_leading_zeroes} has successfully been sent to your email.")
    #     except Exception as e:
    #         messages.error(request, f"An error occurred while sending an email notification for Order #{order.get_id_with_leading_zeroes}. Please try again later or contact us at customers@quicklink.ph.")
    # try:
    #     order.complete = True
    #     order.save()
    #     return True
    # except:
    #     messages.request(f"An error occurred during order completion. Please try again.")
    #     return False