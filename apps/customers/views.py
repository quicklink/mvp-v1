from math import e
import os
import environ
from django.conf import settings

from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_page
from django.views.decorators.cache import cache_control
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
import django_hosts

from django.contrib.auth.decorators import login_required
from accounts.decorators import allowed_users

from orders.models import Order, OrderItem, OrderInformation
from products.models import *
from accounts.models import *
from orders.forms import *

from .services import *
from .selectors import *

import json
import base64

# Create your views here.
@cache_page(60)
def marketplace(request):
    # all_shops = ShopInformation.objects.all()
    # shops_list = []
    # for shop in all_shops:
    #     products = Product.objects.filter(user=shop.user, active=True)
    #     if products.count() > 0: shops_list.append(shop.user)
    # shops = ShopInformation.objects.filter(user__in=shops_list)
    shops = ShopInformation.objects.filter(user__role="MERCHANT", user__shop_product__isnull=False).distinct().order_by("shop_name")
    context = {'shops':shops, 'marketplace':True}
    return render(request, 'quicklink/marketplace.html', context)

# View Products (Customers)
@cache_page(5)
@cache_control(private=True)
def view_products(request, username):
    shop = ShopInformation.objects.get(shop_username=username) # Get Shop Information
    shop_owner = shop.user # Get Shop User Profile
    products = Product.objects.filter(user=shop_owner, active=True).order_by("name") # Get Active Shop Products
    address = Address.objects.get(user=shop_owner, default=True) # Get Shop Settings
    customer = get_customer(request.user, request.COOKIES, request.session) # Get Customer
    order = get_order(customer, shop_owner) # Get or Create Current Order
    items = order.orderitem_set.all().order_by("id") # Get items under that order
    editable_cart = True # Cart is Editable
    context = {'products':products, 'shop':shop, 'items':items, 'order':order, 'address':address, 'editable_cart':editable_cart}
    return render(request, 'shops/products.html', context)

# Add Products to Cart (Customers)
@csrf_exempt
@cache_page(10)
@cache_control(private=True)
def add_order(request, username, product_pk):
    shop = ShopInformation.objects.get(shop_username=username) # Get Shop Information
    shop_owner = shop.user # Get Shop User Profile
    products = Product.objects.filter(user=shop_owner, active=True).order_by("name") # Get Active Shop Products
    editable_cart = True # Cart is Editable

    product = Product.objects.get(id=product_pk) # Get product according to url pk
    images = product.get_images # Get product images, see models for function
    sizes = product.get_sizes # Get product sizes, see models for function
    addons = product.get_addons # Get product addons, see models for function
    
    customer = get_customer(request.user, request.COOKIES, request.session) # Get Customer (function)
    order = get_order(customer, shop_owner) # Get Current Order
    items = order.orderitem_set.all().order_by("id") # Get items under that order
    
    if product.made_to_order: product_stock = None
    else: product_stock = product.stock
    form = OrderForm(request.POST or None, product_stock=product_stock)
    if request.method == 'POST':       
        # Save form if form is valid
        if form.is_valid():
            order_item = add_to_cart(shop, order, product, request.POST, form.cleaned_data) # Add to cart function
            messages.success(request, f'Successfully added {order_item.quantity} {order_item.product.name} to cart!')
        else:
            messages.error(request, f'An error occurred while adding a product to cart. Please try again later!')
        return redirect(django_hosts.resolvers.reverse('view-products', args=(shop.shop_username, ), host='shops'))

    context = {
        'shop':shop, 
        'products':products,
        'items':items,
        'order':order,
        'editable_cart':editable_cart,
        'form':form,
        'product':product,
        'images':images,
        'sizes':sizes, 
        'addons':addons
    }
    return render(request, 'shops/order_form.html', context)

# Edit Products in Cart (Customers)
@csrf_exempt
@cache_page(10)
@cache_control(private=True)
def edit_order(request, username, item_pk):
    shop = ShopInformation.objects.get(shop_username=username) # Get Shop Information
    shop_owner = shop.user # Get Shop User Profile
    products = Product.objects.filter(user=shop_owner, active=True) # Get Active Shop Products
    editable_cart = True # Cart is Editable
    product_order = OrderItem.objects.get(id=item_pk) # Get order item according to url pk
    product = product_order.product # Get product according to order item 
    images = product.get_images # Get product images, see models for function
    sizes = product.get_sizes # Get product sizes, see models for function
    addons = product.get_addons # Get product addons, see models for function
    size = product_order.size # Get selected size
    addon = product_order.addons # Get selected addons
    customer = get_customer(request.user, request.COOKIES, request.session) # Get Customer
    order = get_order(customer, shop_owner) # Get or Create Current Order
    items = order.orderitem_set.all().order_by("id") # Get items under that order

    if product.made_to_order: product_stock = None
    else: product_stock = product.stock
    form = OrderForm(request.POST or None, instance=product_order, product_stock=product_stock)

    if request.method == 'POST':
        # Save form if form is valid
        if form.is_valid():
            edit_cart_item(shop, order, product_order, request.POST, form.cleaned_data)
            messages.success(request, f'Successfully edited cart item!')
        else:
            messages.error(request, f'An error occurred while editing a cart item. Please try again later!')
        return redirect(django_hosts.resolvers.reverse('view-products', args=(shop.shop_username, ), host='shops'))

    context = {
        'shop':shop, 
        'products':products,
        'items':items,
        'order':order,
        'editable_cart':editable_cart,
        'form':form,
        'product':product,
        'images':images,
        'sizes':sizes,
        'selected_size':size, 
        'addons':addons,
        'selected_addon':addon
    }
    return render(request, 'shops/edit_order.html', context)

from ph_geography.models import Province, Municipality
def load_cities(request):
    province = request.GET.get('province_pk')
    province = Province.objects.get(name=province.upper())
    # municipalities = Province.objects.select_related("municipality").get(name=province.upper())
    municipalities = Municipality.objects.filter(province=province).order_by('name')
    return render(request, 'dropdowns/cities.html', {'municipalities':municipalities})

from discord_webhook import DiscordWebhook, DiscordEmbed

def ewallet_callback(request):
    user = request.user
    icon_url = "https://ik.imagekit.io/ripzjge77zz/Quicklink_App/Marketplace/Settings/Group_358_s1885d_rX-X.png"

    print(request.GET)

    event = request.GET.get('event') # Identifies the event triggering a notification to merchant. ewallet.capture occurs when eWallet issuer confirms the payment status of a transaction
    business_id = request.GET.get('business_id') # Merchant Business ID.
    created = request.GET.get('created') # ISO 8601 Timestamp for callback notification creation. Timezone UTC+0.
    time_created = datetime.datetime.strptime(created, "%c")
    data = request.GET.get('data') # eWallets charge object will be nested in this parameter. See here for full definitions.
    payment_status = data.status # Status of charge request - SUCCEEDED, PENDING, FAILED, VOIDED
    currency = data.currency
    charge_amount = data.charge_amount # Requested charge amount from merchant
    capture_amount = data.capture_amount # Requested capture amount from merchant. In requests where checkout_method = ONE_TIME_PAYMENT, capture_amount will always be the same as charge_amount
    channel_code = data.channel_code # Channel Code specifies which eWallet will be used to process the transaction - PH_PAYMAYA, PH_GCASH, PH_GRABPAY

    if settings.LOCAL_DEV == False and user.is_authenticated:
        if hasattr(user, 'shop_info'):
            if hasattr(user.shop_info, "shop_logo"):
                icon_url = user.shop_info.shop_logo.logo.url
        else:
            if hasattr(user, 'customer_info'):
                if hasattr(user.customer_info, "customer_dp"):
                    icon_url = user.customer_info.customer_dp.profile_pic.url

    if request.method == 'POST':
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        summary = request.POST.get('summary', '')
        description = request.POST.get('description', '')

        discord_url = "https://discord.com/api/webhooks/863015986626166815/wFaRNNHI9hWK7inujBOEMnc-ZS-BYidORpoCI6kJJJeB63NK9-KQLMqCx5C3ReDLdQhF"
        webhook = DiscordWebhook(url=discord_url)
        embed = DiscordEmbed(
            title="New eWallet Payment!", description="MONEEEEEH", color='f8f8f8'
        )
        try:
            embed.set_author(
                name=name,
                icon_url=icon_url,
            )
        except:
            embed.set_author(
                name=name
            )
        embed.add_embed_field(name="Event", value=f"{event} (Business ID: {business_id})", inline=False)
        embed.add_embed_field(name="Channel", value=f"{channel_code}", inline=False)
        embed.add_embed_field(name="Payment Status", value=f"{payment_status}", inline=False)
        embed.add_embed_field(name="Total Amount", value=f"{currency} {charge_amount} (Captured: {currency} {capture_amount}", inline=False)
        embed.add_embed_field(name="Paid on", value=f"{time_created}", inline=False)
        webhook.add_embed(embed)
        response = webhook.execute()
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=500)

@csrf_exempt
@cache_page(10)
# @cache_page(60)
@cache_control(private=True)
def checkout(request, order_id):  
    customer = get_customer(request.user, request.COOKIES, request.session) # Get Customer
    
    province = Province.objects.get(name="METRO MANILA")
    cities = Municipality.objects.filter(province=province)

    order = Order.objects.get(id=order_id)
    shop = order.shop.shop_info
    items = order.orderitem_set.all().order_by("id")
    editable_cart = False

    order.quantity = order.get_cart_quantity
    order.save()
    delivery_option = order.delivery_option
    shop_settings = shop.shop_general_settings

    min_date = shop_settings.get_min_delivery_date # Getting the minimum available delivery date
    min_year = min_date.year # Getting the minimum available delivery date year
    min_month = min_date.month # Getting the minimum available delivery date year
    min_day = min_date.day # Getting the minimum available delivery date year
    min_hour = min_date.hour
    delivery_from = f"{shop_settings.delivery_from_hour}:00"
    delivery_to = f"{shop_settings.delivery_to_hour}:00"
    delivery_schedule = shop_settings.get_delivery_days # Getting the delivery schedule
    available_days = shop_settings.get_available_days # Getting available days for datepicker

    form = CheckoutForm(request.POST or None, user=customer, shop=shop, order=order, date=min_date)    

    if request.method == "POST":
        if form.is_valid():           
            sms = request.POST.get("notif_sms", False)
            email = request.POST.get("notif_email", False)
            if sms != False: sms = True
            if email != False: email = True
            save_checkout(form, customer, sms, email, order, items)
            bank_account = form.cleaned_data.get('bank_name')
            if bank_account in CheckoutForm.BANKS[0]: return redirect(django_hosts.resolvers.reverse('payment', args=(str(order.pk), 'bank', ), host='shops')) # if Bank Transfer
            elif bank_account in CheckoutForm.BANKS[1]: return redirect(django_hosts.resolvers.reverse('payment', args=(str(order.pk), 'card', ), host='shops')) # If Debit/Credit
            elif bank_account in CheckoutForm.BANKS[2]: return redirect(django_hosts.resolvers.reverse('payment', args=(str(order.pk), 'ewallet', ), host='shops')) # If eWallet
        else:
            # messages.error(request, "Could not proceed to payment. Please try again later.")
            for e in form.errors.as_data():
                messages.error(request, e)

    context = {
        'items':items, 
        'shop':shop, 
        'order':order, 
        'form':form,
        'province':province,
        'cities':cities, 
        'editable_cart':editable_cart, 
        'available_days':available_days, 
        'delivery_schedule':delivery_schedule,
        'min_date':min_date,
        'min_year':min_year,
        'min_month':min_month,
        'min_day':min_day,
        'min_hour':min_hour,
        'delivery_from':delivery_from,
        'delivery_to':delivery_to,
        'delivery_option':delivery_option
    }
    return render(request, 'shops/checkout.html', context)

@cache_page(60)
@cache_control(private=True)
def payment(request, order_id, slug):
    customer = get_customer(request.user, request.COOKIES, request.session) # Get Customer
    order = Order.objects.get(id=order_id)
    order_info = OrderInformation.objects.get(order=order)
    items = order.orderitem_set.all().order_by("id")
    payment_methods = CheckoutForm.BANKS

    # if Bank Transfer
    if slug == 'bank':
        available_accounts = get_available_accounts(slug, customer)
        form = BankTransferForm()
        read_xendit_instance = get_xendit_instance("read")
        write_xendit_instance = get_xendit_instance("write")
        XenditCustomer = read_xendit_instance.DirectDebit
        DirectDebit = write_xendit_instance.DirectDebit

        if request.method == 'POST':
            form = BankTransferForm(request.POST)
            if form.is_valid():
                session_payment = save_bank_session_payment(form, customer, order_info, True)
                # account_number_last_four = session_payment.get_last_four

                # Create Xendit Customer
                # API Reference: https://developers.xendit.co/api-reference/?python#customers
                customer_info = order_info.session_sender
                xendit_customer = get_or_create_xendit_customer(XenditCustomer, DirectDebit, customer_info, order_info, customer.pk, False)
                if not xendit_customer:
                    messages.error(request, "An error occurred on our payment service provider. Please try again later or use a different payment method.")
                    return redirect(request.META.get("HTTP_REFERER"))

                # Initialize Linked Account Tokenization
                # API Reference: https://developers.xendit.co/api-reference/#initialize-linked-account-tokenization
                failure_redirect_url = request.build_absolute_uri(django_hosts.resolvers.reverse('failed-bank-linking', args=(str(order.pk), ), host='shops'))
                success_redirect_url = request.build_absolute_uri(django_hosts.resolvers.reverse('bank-account-linking', args=(str(order.pk), ), host='shops'))
                linked_account = initialize_bank_transfer(DirectDebit, customer_info, session_payment, failure_redirect_url, success_redirect_url)
                return redirect(linked_account['authorizer_url'])
            else:
                messages.error(request, "Invalid form submitted.")
    # If Debit/Credit
    elif slug == 'card':
        api_key = get_xendit_write_api_key()
        xendit_instance = get_xendit_instance("write")
        CreditCard = xendit_instance.CreditCard

        if 'HTTP_AUTHORIZATION' not in request.META:
            byte = bytes(api_key, 'utf-8')
            username = base64.b64encode(byte)
            request.META['HTTP_AUTHORIZATION'] = username

        available_accounts = get_available_accounts(slug, customer)
        form = CardForm()

        if request.method == 'POST' and request.is_ajax:
            token_data = json.loads(request.POST.get('data'))
            session_payment = save_card_session_payment(token_data, customer, order_info)
            token_id = request.POST.get('token_id')
            auth_id = request.POST.get('auth_id')
            external_id = get_external_id(order)
            amount = float(order.total)
            cvv = session_payment.cvv
            charge = create_card_charge(CreditCard, token_id, auth_id, external_id, amount, cvv)
            if str(charge.status) == 'CAPTURED':
                url = django_hosts.resolvers.reverse('order-completion', args=(str(order.pk), ), host='shops')
                return JsonResponse({"url":url}, safe=False)
                # return redirect(django_hosts.resolvers.reverse('order-completion', args=(str(order.pk), ), host='shops'))
            elif str(charge.status) == 'FAILED':
                url = request.META.get("HTTP_REFERER", django_hosts.resolvers.reverse('payment', args=(str(order.pk), "card", ), host='shops'))
                is_server_error = raise_card_errors(request, charge.failure_reason)
                if is_server_error: return HttpResponse(status=500)
                else: return JsonResponse({"url":url}, safe=False)
    # If eWallet
    elif slug == 'ewallet':
        api_key = get_xendit_write_api_key()
        read_xendit_instance = get_xendit_instance("read")
        write_xendit_instance = get_xendit_instance("write")
        XenditCustomer = read_xendit_instance.DirectDebit
        DirectDebit = write_xendit_instance.DirectDebit
        EWallet = write_xendit_instance.EWallet

        if 'HTTP_AUTHORIZATION' not in request.META:
            byte = bytes(api_key, 'utf-8')
            username = base64.b64encode(byte)
            request.META['HTTP_AUTHORIZATION'] = username

        available_accounts = get_available_accounts(slug, customer)
        form = eWalletForm()
        if request.method == 'POST':
            form = eWalletForm(request.POST)
            if form.is_valid():
                session_payment = save_ewallet_session_payment(form, customer, order_info, True)
                # account_number_last_four = session_payment.get_last_four

                # Create Xendit Customer
                # API Reference: https://developers.xendit.co/api-reference/?python#customers
                customer_info = order_info.session_sender
                xendit_customer = get_or_create_xendit_customer(XenditCustomer, DirectDebit, customer_info, order_info, customer.pk, False)
                if not xendit_customer:
                    messages.error(request, "An error occurred on our payment service provider. Please try again later or use a different payment method.")
                    return redirect(request.META.get("HTTP_REFERER"))

                # Create eWallet Charge
                # Add Basket Items in v2
                current_url = django_hosts.resolvers.reverse('payment', args=(str(order.pk), "ewallet", ), host='shops')
                success_redirect_url = django_hosts.resolvers.reverse('order-completion', args=(str(order.pk), ), host='shops')
                ewallet_charge = create_ewallet_charge(EWallet, order, session_payment, success_redirect_url, current_url, customer_info)

                if isinstance(ewallet_charge, str):
                    raise_ewallet_errors(request, ewallet_charge)
                    return redirect(request.build_absolute_uri(django_hosts.resolvers.reverse('payment', args=(str(order.pk), "ewallet"), host='shops')))
                else:
                    if ewallet_charge.status == 'SUBMITTED':
                        return redirect(django_hosts.resolvers.reverse('order-completion', args=(str(order.pk), ), host='shops'))
                    elif ewallet_charge.status == 'PENDING':
                        if ewallet_charge.is_redirect_required == True:
                            if request.user_agent.is_mobile or request.user_agent.is_tablet:
                                try: return redirect(ewallet_charge.actions['mobile_deeplink_checkout_url'])
                                except: return redirect(ewallet_charge.actions['mobile_web_checkout_url'])
                            else:
                                return redirect(ewallet_charge.actions['desktop_web_checkout_url'])
                    elif ewallet_charge.status == 'FAILED' or ewallet_charge.status == 'VOIDED':
                        messages.error(request, ewallet_charge.status)
            else:
                messages.error(request, "Please choose an e-wallet provider.")

    context = {'items':items, 'form':form, 'slug':slug, 'order':order, 'payment_methods':payment_methods, 'available_accounts':available_accounts}
    return render(request, 'shops/payment.html', context)

def failed_bank_linking(request, order_id):
    messages.error(request, "Failed to authorize bank account. Please try again.")
    return redirect(django_hosts.resolvers.reverse('payment', args=(str(order_id), "bank"), host='shops'))

@cache_page(60*60)
@cache_control(private=True)
def bank_account_linking(request, order_id):
    order = Order.objects.get(id=order_id)
    order_info = OrderInformation.objects.get(order=order)
    # customer_info = order_info.session_sender
    payment_info = order_info.session_payment
    linked_account_token_id = request.GET.get('linked_account_token_id', '')

    accessible_accounts = get_xendit_linked_accounts(linked_account_token_id)
    if request.method == "POST":
        selected_account = request.POST.get("bank_account")
        for account in accessible_accounts:
            try:
                if account["id"] == selected_account: 
                    payment_info.account_number = account["properties"]["account_details"]
            except: pass
        payment_info.linked_account_token_id = selected_account
        payment_info.save()
        return redirect(request.build_absolute_uri(django_hosts.resolvers.reverse('bank-payment', args=(str(order.pk), ), host='shops')))

    context = {"order":order, "payment_info": payment_info, "accessible_accounts":accessible_accounts}
    return render(request, "shops/bank_accounts.html", context)

@cache_page(60)
@cache_control(private=True)
def bank_transfer_payment(request, order_id):
    order = Order.objects.get(id=order_id)
    order_info = OrderInformation.objects.get(order=order)
    customer_info = order_info.session_sender
    payment_info = order_info.session_payment
    ba_la_id = payment_info.linked_account_token_id

    payment_response = create_bank_transfer_payment(customer_info, ba_la_id, order)

    if isinstance(payment_response, str):
        raise_bank_transfer_errors(request, payment_response)
        return redirect(request.build_absolute_uri(django_hosts.resolvers.reverse('payment', args=(str(order.pk), "bank"), host='shops')))
    else:
        request.session['bank_transfer_payment'] = json.dumps(payment_response)
        enable_otp = payment_response["is_otp_required"]

        if enable_otp == True and payment_response['status'] == 'PENDING':
            return redirect(django_hosts.resolvers.reverse('payment-otp', args=(str(order.pk), ), host='shops'))
        elif payment_response['status'] == 'COMPLETED' or (enable_otp == False and payment_response['status'] == 'PENDING'):
            return redirect(django_hosts.resolvers.reverse('order-completion', args=(str(order.pk), ), host='shops'))
        elif payment_response['status'] == 'FAILED':
            messages.error(request, "Failed to create payment. Please try again later or contact us at customers@quicklink.ph!")
            return redirect(django_hosts.resolvers.reverse('payment', args=(str(order_id), "bank"), host='shops'))
        else:
            messages.error(request, "An error occurred. Please try again later or contact us at customers@quicklink.ph!")
            return redirect(django_hosts.resolvers.reverse('payment', args=(str(order_id), "bank"), host='shops'))

@cache_page(60)
@cache_control(private=True)
def bank_transfer_otp(request, order_id):
    order = Order.objects.get(id=order_id)
    payment = json.loads(request.session['bank_transfer_payment'])
    otp_mobile_number = payment["otp_mobile_number"]
    otp_inputs = range(0, 6)

    if request.method == 'POST':
        validated = validate_bank_transfer_otp(request, otp_inputs, payment)
        if validated == 1: return redirect(request.build_absolute_uri(django_hosts.resolvers.reverse('order-completion', args=(str(order.pk), ), host='shops')))
        elif validated == 2: return redirect(request.META.get("HTTP_REFERER"))
        elif validated == 0: return redirect(request.build_absolute_uri(django_hosts.resolvers.reverse('payment', args=(str(order.pk), "bank"), host='shops')))

    context = {'otp_inputs':otp_inputs, 'otp_mobile_number':otp_mobile_number}
    return render(request, 'shops/payment_otp.html', context)

@cache_page(60)
@cache_control(private=True)
def order_completion(request, order_id):
    order = Order.objects.get(id=order_id)
    items = order.orderitem_set.all().order_by("id")
    invoice_url = request.build_absolute_uri(django_hosts.resolvers.reverse('order-invoice', args=(order.pk, ), host='shops'))
    completed = complete_order(request, order, items, invoice_url)
    if completed: return redirect(invoice_url)
    else: return redirect(request.build_absolute_uri(django_hosts.resolvers.reverse('payment', args=(str(order.pk), "bank"), host='shops')))
    return render(request, 'shops/processing.html', {})

@cache_page(60)
@cache_control(private=True)
def order_invoice(request, order_id):
    order = Order.objects.get(id=order_id)
    shop = order.shop.shop_info
    order_info = OrderInformation.objects.get(order=order)
    order_status = order.order_status

    items = order.orderitem_set.all().order_by("id")
    banks = ['BA_BPI', 'BA_BDO', 'BA_UBP']
    e_wallets = ['PH_GCASH', 'PH_PAYMAYA', 'PH_GRABPAY']

    context = {
        'order':order, 
        'info':order_info, 
        'items':items, 
        'shop':shop, 
        'banks':banks,
        'e_wallets':e_wallets,
        'order_status':order_status
    }
    return render(request, 'shops/invoice.html', context)

# Email Verification
from weasyprint import HTML, CSS
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from accounts.decorators import unauthenticated_customer, allowed_users
from django.contrib.auth.models import Group
from accounts.forms import *

# To see how to set environment variables, see https://github.com/sendgrid/sendgrid-python

@unauthenticated_customer
@cache_page(60 * 60)
@cache_control(private=True)
def account_creation(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid() and form.validate_password():
            user = User.objects._create_user(
                email=form.cleaned_data.get("email"),
                password=form.cleaned_data.get("password1")
            )
            user.role = User.Types.CUSTOMER
            user.is_active = False
            customer_group, created = Group.objects.get_or_create(name='Customer')
            user.groups.add(customer_group)
            user.save()

            to_email = user.email
            user_id = user.pk
            uid = urlsafe_base64_encode(force_bytes(user.pk))
            token = default_token_generator.make_token(user)
            link = request.build_absolute_uri(django_hosts.resolvers.reverse('email-confirmation', args=(uid, token), host='shops'))

            sg_data = {
                "from": {
                    "email":"josh@quicklink.ph"
                },
                "personalizations": [
                    {
                        "to":[
                            {
                                "email":to_email
                            },
                        ],
                        "dynamic_template_data": {
                            "user_id":user_id,
                            "link":link
                        }
                    }
                ],
                "template_id":"d-a22e00ee23694faeae56b21d10afbf7a"
            }
            sg_data = json.loads(json.dumps(sg_data))

            if not settings.LOCAL_DEV:
                if "SENDGRID_API_KEY" in os.environ:
                    sg_api_key = os.environ.get("SENDGRID_API_KEY")
                else:
                    env_vars = get_environ_vars()
                    sg_api_key = env_vars.get("SENDGRID_API_KEY")
            else:
                env = environ.Env()
                env.read_env()
                sg_api_key = env("SENDGRID_API_KEY")

            try:
                sg = SendGridAPIClient(api_key=sg_api_key)
                response = sg.client.mail.send.post(request_body=sg_data)
            except Exception as e:
                messages.error(request, "Can't send verification email. Send an email to no-reply@quicklink.ph")
            return redirect(django_hosts.resolvers.reverse('email-verification', args=(str(user.pk), ), host='shops'))
    context = {'form':form, 'customer':True}
    return render(request, 'accounts/merchant-register.html', context)

@csrf_exempt
@unauthenticated_customer
@cache_page(60 * 60)
@cache_control(private=True)
def email_verification(request, user_id):
    user = User.objects.get(id=user_id)
    to_email = user.email
    mail_subject = 'Activate your Quicklink Account!'
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token = default_token_generator.make_token(user)
    link = request.build_absolute_uri(django_hosts.resolvers.reverse('email-confirmation', args=(uid, token), host='shops'))

    if request.is_ajax and request.method == "POST":
        sg_data = {
            "from": {
                "email":"josh@quicklink.ph"
            },
             "personalizations": [
                {
                    "to":[
                        {
                            "email":to_email
                        },
                    ],
                    "dynamic_template_data": {
                        "user_id":user_id,
                        "link":link
                    }
                }
            ],
            "template_id":"d-a22e00ee23694faeae56b21d10afbf7a"
        }
        sg_data = json.loads(json.dumps(sg_data))

        if not settings.LOCAL_DEV:
            if "SENDGRID_API_KEY" in os.environ:
                sg_api_key = os.environ.get("SENDGRID_API_KEY")
            else:
                env_vars = get_environ_vars()
                sg_api_key = env_vars.get("SENDGRID_API_KEY")
        else:
            env = environ.Env()
            env.read_env()
            sg_api_key = env("SENDGRID_API_KEY")

        try:
            sg = SendGridAPIClient(api_key=sg_api_key)
            response = sg.client.mail.send.post(request_body=sg_data)
        except Exception as e:
            messages.error(request, "Can't send verification email. Send an email to no-reply@quicklink.ph")

    context = {'user': user}
    return render(request, 'accounts/email-verification.html', context)

@csrf_exempt
@unauthenticated_customer
@cache_page(60 * 60)
@cache_control(private=True)
def email_confirmation(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        success = True
    else:
        success = False
    context = {"success":success, 'customer':True}
    return render(request, 'accounts/email-confirmation.html', context)

@unauthenticated_customer
@cache_page(60 * 60)
@cache_control(private=True)
def customer_login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(request, email=email, password=password)

        if user is not None:
            login(request, user)
            return redirect(django_hosts.resolvers.reverse('marketplace', host='shops'))
        else:
            messages.info(request, "Email or password is incorrect.")
    
    context = {'customer':True}
    return render(request, 'accounts/merchant-login.html', context)

def customer_logout(request):
    logout(request)
    return redirect(django_hosts.resolvers.reverse('marketplace', host='shops'))

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60 * 60)
@cache_control(private=True)
def register_customer_information(request):
    user = request.user
    if request.method == 'POST':
        form = CustomerInformationForm(request.POST)
        if form.is_valid():
            validated_data = form.get_validated_data()
            customer_info, created = CustomerInformation.objects.get_or_create(customer=user, customer_profile=True)

            customer_contact_number = validated_data.get('customer_contact_number')
            customer_contact_number = customer_contact_number.replace(' ', '')

            customer_info.customer_name = validated_data.get("customer_name")
            customer_info.customer_contact_number = customer_contact_number
            customer_info.customer_username = validated_data.get("customer_username")
            customer_info.save()

            social_links, created = SocialMediaLink.objects.get_or_create(user=user)
            social_links.instagram = form.cleaned_data.get("instagram")
            social_links.facebook = form.cleaned_data.get("facebook")
            social_links.twitter = form.cleaned_data.get("twitter")
            social_links.save()
            return redirect(django_hosts.resolvers.reverse('register-address', host='shops'))
    else:
        try: customer = CustomerInformation.objects.get(customer=user, customer_profile=True)
        except: customer = None
        
        if customer != None:
            if hasattr(customer, 'user_links'):
                form = CustomerInformationForm(initial={
                    'customer_name': customer.customer_name,
                    'customer_contact_number': customer.customer_contact_number[2:],
                    'customer_username': customer.customer_username,
                    'instagram' : user.user_links.instagram,
                    'facebook' : user.user_links.facebook,
                    'twitter' : user.user_links.twitter
                })
            else:
                form = CustomerInformationForm(initial={
                    'customer_name': customer.customer_name,
                    'customer_contact_number': customer.customer_contact_number[2:],
                    'customer_username': customer.customer_username
                })
        else:
            form = CustomerInformationForm()

    context = {'form':form}
    return render(request, 'accounts/add-customer-information.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60 * 60)
@cache_control(private=True)
def register_customer_address(request):
    user = request.user
    if request.method == 'POST':
        form = CustomerAddressForm(request.POST)
        if form.is_valid():
            customer_address, created = Address.objects.get_or_create(user=user, default=True)
            customer_address.line1 = form.cleaned_data.get("line1")
            customer_address.line2 = form.cleaned_data.get("line2")
            customer_address.city = form.cleaned_data.get("city")
            customer_address.province = form.cleaned_data.get("province")
            customer_address.postal_code = form.cleaned_data.get("postal_code")
            customer_address.save()
            return redirect(django_hosts.resolvers.reverse('register-payment', host='shops'))
    else:
        try: address = Address.objects.get(user=user, default=True)
        except: address = None
        
        if address != None:
            form = CustomerAddressForm(initial={
                'line1': address.line1,
                'line2': address.line2,
                'city': address.city,
                'province': address.province,
                'postal_code': address.postal_code
            })
        else:
            form = CustomerAddressForm()
    
    context = {'form':form}
    return render(request, 'accounts/add-customer-address.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60 * 60)
@cache_control(private=True)
def register_customer_payment(request):
    user = request.user
    choices = [
        ('bank', _('Add a bank account')),
        ('card', _('Add a debit/credit card')),
        ('ewallet', _('Add an e-wallet')),
    ]
    context = {'choices':choices}
    return render(request, 'accounts/add-customer-account.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60 * 60)
@cache_control(private=True)
def register_customer_payment_method(request, slug):
    user = request.user
    if request.method == 'POST':
        form = CustomerAccountForm(request.POST)
        if form.is_valid():
            if slug == 'card':
                customer_account, created = BankAccount.objects.get_or_create(
                    user=user,
                    cardholder_name=form.cleaned_data.get("cardholder_name"),
                    account_number=form.cleaned_data.get("account_number"),
                    exp_date=form.cleaned_data.get("exp_date"),
                    cvv=form.cleaned_data.get("cvv"),
                    saved=True
                )
            else:
                customer_account, created = BankAccount.objects.get_or_create(
                    user=user,
                    bank_name=form.cleaned_data.get("bank_name"),
                    cardholder_name=form.cleaned_data.get("cardholder_name"),
                    account_number=form.cleaned_data.get("account_number"),
                    saved=True
                )
            customer_account.save()
            return redirect(django_hosts.resolvers.reverse('register-notifications', host='shops'))
    else:
        try: account = BankAccount.objects.get(user=user, saved=True)
        except: account = None
        
        if account != None:
            form = CustomerAccountForm(initial={
                'bank_name': account.bank_name,
                'cardholder_name': account.cardholder_name,
                'account_number': account.account_number,
                'exp_date': account.exp_date,
                'cvv': account.cvv
            })
        else:
            form = CustomerAccountForm()
        
        if slug == 'bank':
            selected_banks = ['',
                "BA_BPI",
                "BA_BDO",
                "BA_UBP",
            ]
            form.fields['bank_name'].choices = [(k, v) for k, v in BankAccount.BANKS if k in selected_banks]
        elif slug == 'ewallet':
            selected_banks = ['',
                "PH_GCASH",
                "PH_PAYMAYA",
                "PH_GRABPAY",
            ]
            form.fields['bank_name'].choices = [(k, v) for k, v in BankAccount.BANKS if k in selected_banks]
    
    context = {'slug':slug, 'form':form}
    return render(request, 'accounts/add-payment-method.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60 * 60)
@cache_control(private=True)
def register_customer_notifications(request):
    user = request.user
    if request.method == 'POST':
        form = NotificationsForm(request.POST)
        if form.is_valid():
            customer_notifications, created = CustomerNotification.objects.get_or_create(
                user=user,
                default=True,
            )
            customer_notifications.sms = form.cleaned_data.get('sms')
            customer_notifications.email = form.cleaned_data.get('email')
            customer_notifications.save()
            return redirect(django_hosts.resolvers.reverse('register-complete', host='shops'))
    else:
        try: notifications = CustomerNotification.objects.get(user=user, default=True)
        except: notifications = None
        
        if notifications != None:
            form = NotificationsForm(initial={
                'sms': notifications.sms,
                'email': notifications.email,
            })
        else:
            form = NotificationsForm()
    
    context = {'form':form}
    return render(request, 'accounts/add-customer-notifications.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60 * 60)
def register_complete(request):
    return render(request, 'accounts/customer-registration-success.html', {})

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60)
@cache_control(private=True)
def account_orders(request):
    user = request.user
    try: 
        customer = CustomerInformation.objects.get(customer=user, customer_profile=True)
    except:
        if hasattr(user, "shop_info"): 
            customer = ShopInformation.objects.get(user=user)
        else:
            customer = None
    active_orders = Order.objects.filter(user=user, complete=True).exclude(order_status='RECEIVED')
    order_history = Order.objects.filter(user=user, complete=True, order_status='RECEIVED')
    context = {'customer':customer, 'active_orders':active_orders, 'order_history':order_history}
    return render(request, 'profile/customer_orders.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60)
@cache_control(private=True)
def account_profile(request):
    user = request.user
    customer = CustomerInformation.objects.get(customer=user, customer_profile=True)
    address = Address.objects.get(user=user, default=True)
    if request.method == "POST":
        form = CustomerProfileForm(request.POST)
        if form.is_valid():
            customer.customer_name = form.cleaned_data.get('customer_name')
            # continue
            customer.save()

            address.line1 = form.cleaned_data.get('line1')
            # continue
            address.save()
            messages.success(request, 'Successfully updated customer profile!')
    else:
        try: 
            form = CustomerProfileForm(initial={
                'customer_name': customer.customer_name,
                # continue
                'line1': address.line1
                # continue
            })
        except:
            form = CustomerProfileForm()

@login_required(login_url='login')
@allowed_users(allowed_roles=[User.Types.CUSTOMER, User.Types.ADMIN])
@cache_page(60 * 60)
@cache_control(private=True)
def reorder(request, order_id):
    user = request.user
    order = Order.objects.get(id=order_id)
    try: current_order = Order.objects.get(user=user, complete=False)
    except: current_order = None
    if current_order != None: current_order.delete()

    order_items = OrderItem.objects.filter(order=order)
    order.pk = None
    order.order_info = None
    order.order_status = 'PENDING'
    order.payment_status = 'TO_RECEIVE'
    order.complete = False
    order.save()

    for item in order_items:
        product = OrderItem.objects.get(id=item.pk)
        product.pk = None
        product.order = order
        product.save()

    return redirect(django_hosts.resolvers.reverse('checkout', host='shops'))