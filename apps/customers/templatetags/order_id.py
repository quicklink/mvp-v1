from django import template
from accounts.models import User, ShopInformation, CustomerInformation

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_order_id(value, desired_digits):
  num_zeros = int(desired_digits) - len(str(value))
  padded_value = []
  while num_zeros >= 1:
    padded_value.append("0") 
    num_zeros = num_zeros - 1
  padded_value.append(str(value))
  return "".join(padded_value)