from django import template
from products.models import Product, Image

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_product_image(product_id):
    try:
        product = Product.objects.get(id=product_id)
        image = Image.objects.get(product=product, default=True, active=True)
        return image
    except:
        return False