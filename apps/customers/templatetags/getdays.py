from django import template
from accounts.models import WEEKDAYS, Address, ShopDeliverySettings

# Get Default Product Image Template Tag
register = template.Library()

@register.filter
def get_selected_days(days):
    # selected_days = WEEKDAYS.get_selected_values(shop.shop_general_settings.delivery_days)
    selected_days = WEEKDAYS.get_selected_values(days)
    print(selected_days)

    if len(selected_days) == 0:
        return "No days available"

    days_list = []
    for day in selected_days:
        if day == "Thursday": days_list.append("Th")
        elif day == "Saturday": days_list.append("S")
        elif day == "Sunday": days_list.append("Su")
        else: days_list.append(day[0].upper())
    days = '-'.join(days_list)

    if days == "M-T-W-Th-F":
        return "Weekdays"
    elif days == "S-Su":
        return "Weekends"
    else:
        return days

@register.filter
def get_address(shop):
    shop_address = Address.objects.get(user=shop.user, default=True)
    delivery_address = ShopDeliverySettings.objects.get(shop=shop)
    
    if delivery_address.buyer_books or delivery_address.buyer_picks_up:
        if shop_address.line1 == delivery_address.line1 and (shop_address.line1 != None or delivery_address.line1 != None):
            if shop_address.city[-1] == "y":
                return f"{shop_address.city}"
            else:
                return f"{shop_address.city} City"
        elif shop_address.line1 != delivery_address.line1:
            if delivery_address.city[-1] == "y":
                if delivery_address.line1 and delivery_address.line2:
                    return f"{delivery_address.line1}, {delivery_address.line2}, {delivery_address.city}"
                elif delivery_address.line1:
                    return f"{delivery_address.line1}, {delivery_address.city}"
            else:
                if delivery_address.line1 and delivery_address.line2:
                    return f"{delivery_address.line1}, {delivery_address.line2}, {delivery_address.city} City"
                elif delivery_address.line1:
                    return f"{delivery_address.line1}, {delivery_address.city} City"
        else:
            return "No information available."
    else:
        if shop_address.line1 and shop_address.city:
            if shop_address.city[-1] == "y":
                return f"{shop_address.city}"
            else:
                return f"{shop_address.city} City"
        else:
            return "No information available."