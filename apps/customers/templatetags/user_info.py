from django import template
from accounts.models import User, ShopInformation, CustomerInformation

# Get Username Template Tag
register = template.Library()

@register.filter
def get_username(user):
    if user.is_authenticated:
        if hasattr(user,'shop_info'):
            return user.shop_info.shop_username
        elif hasattr(user, 'customer_info'):
            return user.customer_info.customer_username
    else:
        return False

@register.filter
def get_option(option):
    new_option = option.replace(" ", "_")
    new_option = new_option.lower()
    return new_option