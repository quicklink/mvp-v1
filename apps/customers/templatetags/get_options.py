from django import template

# Get Username Template Tag
register = template.Library()

@register.filter
def get_option(option):
    new_option = option.replace(" ", "_")
    new_option = new_option.lower()
    return new_option