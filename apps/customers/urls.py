from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.marketplace, name='marketplace'),
    path('shop/<str:username>/', views.view_products, name='view-products'),
    path('shop/<str:username>/<str:product_pk>/', views.add_order, name='add-order'),
    path('shop/<str:username>/<str:item_pk>/edit/', views.edit_order, name='edit-order'),
    path('update-cart-item/', views.update_item, name='update-item'),
    path('checkout/<str:order_id>/', views.checkout, name='checkout'),
    path('checkout/<str:order_id>/payment/<str:slug>/', views.payment, name='payment'),
    path('checkout/<str:order_id>/payment/bank/failed-linking/', views.failed_bank_linking, name='failed-bank-linking'),
    path('checkout/<str:order_id>/payment/bank/account-linking/', views.bank_account_linking, name='bank-account-linking'),
    path('checkout/<str:order_id>/payment/bank/pay/', views.bank_transfer_payment, name='bank-payment'),
    path('checkout/<str:order_id>/payment/bank/pay/otp/', views.bank_transfer_otp, name='payment-otp'),
    path('checkout/complete/<str:order_id>/', views.order_completion, name='order-completion'),
    path('checkout/invoice/<str:order_id>/', views.order_invoice, name='order-invoice'),
    path('register/', views.account_creation, name='register'),
    path('login/', views.customer_login, name='login'),
    path('logout/', views.customer_logout, name='logout'),
    path('register/verification/<str:user_id>/', views.email_verification, name='email-verification'),
    path('register/confirmation/<uidb64>/<token>/', views.email_confirmation, name='email-confirmation'),
    path('register/information/', views.register_customer_information, name='register-information'),
    path('register/address/', views.register_customer_address, name='register-address'),
    path('register/payment/', views.register_customer_payment, name='register-payment'),
    path('register/payment/<str:slug>/', views.register_customer_payment_method, name='register-payment-method'),
    path('register/notifications/', views.register_customer_notifications, name='register-notifications'),
    path('register/complete/', views.register_complete, name='register-complete'),
    path('orders/', views.account_orders, name='orders'),
    path('reorder/<str:order_id>/', views.reorder, name='reorder'),
    path('load_cities/', views.load_cities, name='load-cities'),
    path('ewallet-callback/', views.ewallet_callback, name='ewallet-callback')
]  + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

if settings.LOCAL_DEV:
    import debug_toolbar
    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),

handler400 = 'accounts.views.handler_400'
handler403 = 'accounts.views.handler_403'
handler404 = 'accounts.views.handler_404'
handler500 = 'accounts.views.handler_500'