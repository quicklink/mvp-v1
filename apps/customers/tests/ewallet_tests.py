import os
import environ
from django.conf import settings
import django_hosts

from django.test import TestCase
from xendit import Xendit, Balance, EWallet

if not settings.LOCAL_DEV:
    import subprocess
    import ast
    def get_environ_vars():
        completed_process = subprocess.run(
            ['/opt/elasticbeanstalk/bin/get-config', 'environment'],
            stdout=subprocess.PIPE,
            text=True,
            check=True
        )

        return ast.literal_eval(completed_process.stdout)

if not settings.LOCAL_DEV:
    if "XENDIT_API_KEY" in os.environ and "DJANGO_SETTINGS_MODULE" in os.environ:
        api_key = os.environ.get("XENDIT_API_KEY")
        module = os.environ.get("DJANGO_SETTINGS_MODULE")
    else:
        env_vars = get_environ_vars()
        api_key = env_vars.get("XENDIT_API_KEY")
        module = env_vars.get("DJANGO_SETTINGS_MODULE")
else:
    env = environ.Env()
    env.read_env()
    api_key = env("XENDIT_API_KEY")
    module = env("DJANGO_SETTINGS_MODULE")

xendit_instance = Xendit(api_key=api_key)
EWallet = xendit_instance.EWallet

current_url = django_hosts.resolvers.reverse('order-completion', args=(str(1), ), host='shops')
success_redirect_url = django_hosts.resolvers.reverse('order-completion', args=(str(1), ), host='shops')

reference_id_prefix = str(module).split('.')[-1]
reference_id = "ql_" + str(reference_id_prefix) + "_e-wallet_" + str(1)

class eWalletTestCase(TestCase):
    def setUp(self):
        global reference_id, success_redirect_url, current_url

        EWallet.create_ewallet_charge(
            reference_id=reference_id,
            currency="PHP",
            amount=1000,
            checkout_method="ONE_TIME_PAYMENT",
            channel_code="PH_GCASH",
            channel_properties={
                "success_redirect_url": success_redirect_url,
                'failure_redirect_url': current_url,
                'cancel_redirect_url': current_url
            }
        )