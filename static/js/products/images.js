let file;

function FileListItems (files) {
    var b = new ClipboardEvent("").clipboardData || new DataTransfer()
    for (var i = 0, len = files.length; i<len; i++) b.items.add(files[i])
    return b.files
}

function displayImage(file, container) {
    fileURL = (window.URL ? URL : webkitURL).createObjectURL(file)
    let imgTag = `<img src="${fileURL}" alt="User uploaded logo" class="upload__file">`
    container.innerHTML = imgTag;
    container.classList.remove("active");
    container.classList.add("success");
};

// Drag and Drop
function dragDrop(browse, input, container, buttons, add, update, del) {
    var index = parseInt(String(container.id).slice(-1));
    var nextIndex = parseInt(index) + 1;

    if (add) {
        add.addEventListener('click', function() {
            $(`#js-add-image-${index}`).hide();
            $(`#js-product-img-${nextIndex}`).show();
        });
    }

    if (browse) {
        browse.addEventListener('click', function() {
            input.click();
        });
    }

    if (update) {
        update.addEventListener('click', function() {
            input.click();
        });
    }

    if (del) {
        del.addEventListener('click', function() {
            input.files = null;
            let imgTag = `<img src="https://ik.imagekit.io/ripzjge77zz/Quicklink_App/Merchant/Account_Creation/icons/u_image_XucX2a74QNx.svg" alt="Image Icon" class="upload__icon">
            <div class="upload__content">
                <p class="upload__text" id="js-img-text">Drag and drop your product image here, </p>
                <p class="upload__text">or click<span style="color: var(--primary-main);"> Replace Image </span>to browse through your files.</p>
            </div>`;
            container.innerHTML = imgTag;
            container.classList.remove("success");
        });
    }

    if (container) {
        container.addEventListener('dragover', function(e) {
            e.preventDefault();
            container.classList.add("active");
            container.style.transform = "translateY(-2.5%)";
        });

        container.addEventListener('dragleave', function() {
            container.classList.remove("active");
            container.style.transform = "translateY(0)";
        });

        container.addEventListener('drop', function(e) {
            e.preventDefault();
            container.classList.remove("active");
            file = e.dataTransfer.files[0];
            
            let fileType = file.type;
            let validFileTypes = ["image/jpeg", "image/jpg", "image/png"]
            
            if (validFileTypes.includes(fileType)) {
                let fileReader = new FileReader();
                fileReader.onload = function() {
                    let fileURL = fileReader.result;
                    let imgTag = `<img src="${fileURL}" alt="User uploaded logo" class="upload__file">`
                    container.innerHTML = imgTag;
                    container.classList.remove("active");
                    container.classList.add("success");
                    buttons.style.display = "flex";
                }
                fileReader.readAsDataURL(file);
                var files = [file]
                input.files = new FileListItems(files);
            } else {
                new Noty({
                    type: 'error',
                    theme: 'bootstrap-v4',
                    text: "Supported image file formats are .jpg, .jpeg, .png only.",
                    layout: "topRight",
                    timeout: 5000,
                    progressBar: true,
                }).show();
            }
        });
    }

    if (input) {
        input.addEventListener('change', function() {
            displayImage(input.files[0], container);
            buttons.style.display = "flex";
        });
    }
}

const fileBrowse0 = document.getElementById('js-browse-files-0');
const fileInput0 = document.getElementById('id_product_image-0-image');
const productImg0 = document.getElementById('js-product-img-0');
const imgContainer0 = document.getElementById('js-img-upload-0');
const imgBtns0 = document.getElementById('js-browse-files-hidden-0');
const addImg0 = document.getElementById('js-add-image-0');
const updateImg0 = document.getElementById('js-update-image-0');
const deleteImg0 = document.getElementById('js-delete-image-0');

const fileBrowse1 = document.getElementById('js-browse-files-1');
const fileInput1 = document.getElementById('id_product_image-1-image');
const productImg1 = document.getElementById('js-product-img-1');
const imgContainer1 = document.getElementById('js-img-upload-1');
const imgBtns1 = document.getElementById('js-browse-files-hidden-1');
const addImg1 = document.getElementById('js-add-image-1');
const updateImg1 = document.getElementById('js-update-image-1');
const deleteImg1 = document.getElementById('js-delete-image-1');

const fileBrowse2 = document.getElementById('js-browse-files-2');
const fileInput2 = document.getElementById('id_product_image-2-image');
const productImg2 = document.getElementById('js-product-img-2');
const imgContainer2 = document.getElementById('js-img-upload-2');
const imgBtns2 = document.getElementById('js-browse-files-hidden-2');
const addImg2 = document.getElementById('js-add-image-2');
const updateImg2 = document.getElementById('js-update-image-2');
const deleteImg2 = document.getElementById('js-delete-image-2');

dragDrop(fileBrowse0, fileInput0, imgContainer0, imgBtns0, addImg0, updateImg0, deleteImg0);
dragDrop(fileBrowse1, fileInput1, imgContainer1, imgBtns1, addImg1, updateImg1, deleteImg1);
dragDrop(fileBrowse2, fileInput2, imgContainer2, imgBtns2, addImg2, updateImg2, deleteImg2);

function displayImageInputs(el) {
    if (el.attr("id") == "id_product_image-0-image") {
        var file_name = String(fileInput0.value).split("\\")[2];
        $("#js-default").removeClass('hide');
        document.getElementById("js-primary-image").innerHTML = `${file_name}`
        $("#id_product_image-0-default").prop('checked', true);
        $("#js-primary-image").click(function(){ fileInput0.click(); })
    } else if (el.attr("id") == "id_product_image-1-image" || el.attr("id") == "id_product_image-2-image") {
        var file_name_2 = String(fileInput1.value).split("\\")[2];
        var file_name_3 = String(fileInput2.value).split("\\")[2];
        if (file_name_3 == undefined || file_name_3 == null) {
            $("#js-other-images").removeClass('hide');
            document.getElementById("js-second-image").innerHTML = `${file_name_2}`
            $("#js-second-image").click(function(){ fileInput1.click(); })
        } else {
            $("#js-other-images").removeClass('hide');
            document.getElementById("js-second-image").innerHTML = `${file_name_2}`
            document.getElementById("js-third-image").innerHTML = `${file_name_3}`
            fileBrowse.style.display = "none";
            $("#js-second-image").click(function(){ fileInput1.click(); })
            $("#js-third-image").click(function(){ fileInput2.click(); })
        }
    }
}

$(":input:file").each(function() {
    $(this).change(function() {
        displayImageInputs($(this));
    });
});

const imageSlider = document.getElementById("js-image-slider");

function loadImages(file, file_2, file_3) {
    if (file != undefined || file != null) {
        try {
            var fileURL = (window.URL ? URL : webkitURL).createObjectURL(file);
            let imgTag = `<img src="${fileURL}" alt="" class="modal__card__slider__image">`
            imageSlider.innerHTML = imgTag
        } catch (error) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            let imgTag = `<img src="${fileURL}" alt="" class="modal__card__slider__image" id="js-first-slider-image">`
            imageSlider.innerHTML = imgTag
            reader.onload = function(e) {
                document.getElementById("js-first-slider-image").src = e.target.result;
            };
        }
        if (file_2 != undefined || file_2 != null) {
            var fileURL = (window.URL ? URL : webkitURL).createObjectURL(file);
            var fileURL2 = (window.URL ? URL : webkitURL).createObjectURL(file_2);
            let imgTag = `<img src="${fileURL}" alt="" class="modal__card__slider__image"><img src="${fileURL2}" alt="" class="modal__card__slider__image">`
            imageSlider.innerHTML = imgTag
            if (file_3 != undefined || file_3 != null) {
                var fileURL = (window.URL ? URL : webkitURL).createObjectURL(file);
                var fileURL2 = (window.URL ? URL : webkitURL).createObjectURL(file_2);
                var fileURL3 = (window.URL ? URL : webkitURL).createObjectURL(file_3);
                let imgTag = `<img src="${fileURL}" alt="" class="modal__card__slider__image"><img src="${fileURL2}" alt="" class="modal__card__slider__image"><img src="${fileURL3}" alt="" class="modal__card__slider__image">`
                imageSlider.innerHTML = imgTag
            }
        }
    }
}

$("#js-preview-product-btn").click(function() {
    loadImages(fileInput0.files[0], fileInput1.files[0], fileInput2.files[0]);
});