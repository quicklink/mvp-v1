const selectAllBtn = $("#js-select-all");
const deselectAllBtn = $("#js-deselect-all-btn");

const selectBtns = $(".js-select-btn");
const setAsInactiveBtn = $("#js-set-as-inactive-btn");
const deleteSelectedBtn = $("#js-delete-selected-btn");

const openDelModal = $(".js-open-delete-modal");
const exitModalBtns = $(".js-modal-exit-btn");

const toggleBtns = $(".js-switch-btn");

// Changing the header
function checkSelected() {
    if ($(".js-select-btn:checked").length > 0) {
        $("#js-normal-table-header").addClass("hide");
        $("#js-select-products").removeClass("hide");
        document.getElementById("js-selected-input").innerHTML = String($(".js-select-btn:checked").length) + " selected"
    } else {
        $("#js-select-products").addClass("hide");
        $("#js-normal-table-header").removeClass("hide");
    }
}

// Change row color of selected products
function changeRowColor(element) {
    if (element.is(":checked")) {
        element.parent().parent().parent().addClass("active");
    } else {
        element.parent().parent().parent().removeClass("active");
    }
}

// Set as active toggle buttons -- pass data to Django View
toggleBtns.each(function() {
    $(this).click(function() {
        var item = $(this).val();
        var next_url = window.location.href.split("/products/")[0] + "/products/"
        if ($(this).is(":checked")) {
            next_url = next_url + "set-active/"
        } else {
            var inactiveList = []
            inactiveList.push(item);
            item = JSON.stringify(inactiveList)
            next_url = next_url + "set-inactive/"
        }
        $.ajax({
            url: next_url,
            type: "POST",
            data: {'products': item},
            success:function(response){
                console.log("Message data successfully sent.")
            },
            error:function (xhr, textStatus, thrownError){
                console.log("Can't send message data.", xhr, textStatus, thrownError);
            }
        });
    })
});

// Open delete selected products modal
function openDeleteModal(el) {
    $("#js-delete-products-modal").addClass("show");
    if ($(".js-select-btn:checked").length > 1) {
        var selected = $(".js-select-btn:checked").length
        document.getElementById("js-delete-text").innerHTML = `Are you sure you want to delete these ${selected} items?`
    } else {
        var productName = el.attr("data-product");
        el.attr("data-delete", true);
        document.getElementById("js-delete-text").innerHTML = `Are you sure you want to delete ${productName}?`
    }
}

openDelModal.each(function() {
    $(this).click(function() {
        openDeleteModal($(this));
    });
});

// Exit modal buttons
exitModalBtns.each(function() {
    $(this).click(function() {
        $("#js-delete-products-modal").removeClass("show");
    })
})

// Delete all selected products -- pass data to Django View
deleteSelectedBtn.click(function() {
    var selectedProducts = []
    var next_url = window.location.href.split("/products/")[0] + "/products/delete-selected/"
    selectBtns.each(function() {
        if ($(this).is(":checked")) {
            var item = $(this).val();
            selectedProducts.push(item);
        }
    });
    if (selectedProducts.length === 0) {
        selected = $(".js-open-delete-modal[data-delete=true]");
        selected_id = $(".js-open-delete-modal[data-delete=true]").attr("data-id");
        selectedProducts.push(selected_id);
    }
    var selectedProductsJson = JSON.stringify(selectedProducts)
    $.ajax({
        url: next_url,
        type: "POST",
        data: {'products': selectedProductsJson},
        success:function(response){
            location.reload();
        },
        error:function (xhr, textStatus, thrownError){
            new Noty({
                type: 'error',
                theme: 'bootstrap-v4',
                text: "Can't delete product. Try again later and submit a feedback form at https://app.quicklink.ph/resources/feedback/",
                layout: "bottomRight",
                timeout: 3000,
                progressBar: true,
            }).show();
        }
    });
});

$(function(){
    // Select all products
    $("#js-normal-table-header").on("click", "#js-select-all", function() {
        selectBtns.each(function() {
            $(this).prop("checked", true);
            changeRowColor($(this));
        });
        checkSelected();
    });

    // Deselect all products
    $("#js-select-products").on("click", "#js-deselect-all-btn", function() {
        console.log("Deselect all!")
        selectBtns.each(function() {
            $(this).prop("checked", false);
            changeRowColor($(this));
        });
        $("#js-select-all").prop("checked", false);
        checkSelected();
    });

    // Click action on each individual checkbox
    selectBtns.each(function() {
        $(this).click(function() {
            checkSelected();
            changeRowColor($(this));
        });
    });

    // Set as inactive button -- pass data to Django View
    $('#js-select-products').on('click', 'button#js-set-as-inactive-btn', function() {
        var inactiveList = []
        selectBtns.each(function(index) {
            if ($(this).is(":checked")) {
                var item = $(this).val();
                inactiveList.push(item);
                $(`#id_switch_${index}`).prop("checked", false);
            }
        });
        var inactiveListJson = JSON.stringify(inactiveList)
        var next_url = window.location.href.split("/products/")[0] + "/products/set-inactive/"

        $.ajax({
            url: next_url,
            type: "POST",
            data: {'products': inactiveListJson},
            success:function(response){
                console.log("Message data successfully sent.")
            },
            error:function (xhr, textStatus, thrownError){
                console.log("Can't send message data.", xhr, textStatus, thrownError);
            }
        });
    });

    $("#js-select-products").on("click", "#js-open-delete-modal-selected", function() {
        if ($(".js-select-btn:checked").length > 1) {
            openDeleteModal($("#js-open-delete-modal-selected"));
        } else {
            openDeleteModal($(".js-select-btn:checked").eq(0));
        }
    });
});