// Size Radio Variables
// const sizeInputs = document.getElementsByClassName('sizeRadio');
// const sizeRadios = document.getElementsByClassName('size-radio-input');
// const sizeLabels = document.getElementsByClassName('product-size');

// Change radio attributes after DOMContent has been loaded
$(document).ready(function() {
    setInterval(function(){
        // Get price of selected size
        var sizeId = $("input[type='radio'][name='size']:checked").attr('id');
        var sizePrice = parseInt($("label[for='" + sizeId + "']").data('price'));
        if (isNaN(sizePrice) == false) {
            var total = sizePrice
        }

        // Get prices of selected addons
        var addonTotal = 0;
        $('.order-product-addon').find("input:checked").each(function() {
            var addonId = $(this).attr('id');
            var addonPrice = parseInt($("label[for='" + addonId + "']").data('price'));
            addonTotal = addonTotal + addonPrice;
        });

        if (isNaN(addonTotal) == false) {
            total = sizePrice + addonTotal
        }

        var quantity = $('#id_quantity').val();
        if (isNaN(quantity) == false) {
            total = (sizePrice + addonTotal) * quantity
        }

        if (isNaN(total)) {
            document.getElementById('add-to-basket').innerHTML = 
            "Add to Basket";
        } else {
            document.getElementById('add-to-basket').innerHTML = 
            "Add to Basket — PHP " + total;
        }
    }, 1000);

    var sizeRadios = $("input[type='radio'][name='size']");
    sizeRadios.each(function() {
        $(this).click(function() {
            $("#add-to-basket").removeClass("disabled");
        });
    });
})

// Change radio name and id to size_selected when clicked
$('.size-radio-input').click(function() {
    // Reset radio attributes of unselected radio
    $('.size-radio-input').each(function() {
        $(this).prop('checked', false);
    });

    // Turn selected radio into selected_size
    $(this).prop('checked', true);
})