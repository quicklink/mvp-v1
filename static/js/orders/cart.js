// Details buttons
var toggleDetailsBtn = $("#js-toggle-details-btn");
var editDetailsBtns = $(".edit__quantity");
var itemNames = $(".cart__list__item__header__text")

toggleDetailsBtn.click(function() {
    var quantityId;

    editDetailsBtns.each(function(index) {
        quantityId = `#js-item-quantity-${index}`;
        $(quantityId).toggleClass('hide');
        $(this).toggleClass('show');
    })

    if ($(this).html() == 'Show Details') {
        $(this).html('Hide Details');
    } else if ($(this).html() == 'Hide Details') {
        $(this).html('Show Details');
    }

    itemNames.each(function() {
        $(this).toggleClass('show');
    })
})

// Update buttons
var updateBtns = document.getElementsByClassName('update-cart')
var cookieQty = $('#id_quantity')

for (i=0; i < updateBtns.length; i++) {
    updateBtns[i].addEventListener('click', function() {
        var shopSlug = this.dataset.shop
        var productId = this.dataset.product
        var itemId = this.dataset.item
        var action = this.dataset.action
        var index = this.dataset.index

        if (action === 'increase' || action === 'decrease') {
            ajaxUpdateOrder(shopSlug, productId, itemId, action, index)
        }
    })
}

function editOrderProduct(shopSlug, productId, itemId, action, index) {
    var editElement = $(`#js-edit-${itemId}`)
    var qtyElement = $(`#js-item-quantity-${index}`)
    var totalElement = $(`#js-item-total-${index}`)
    var addonsElements = $(`.js-item-${index}-addon`)

    if (action == 'increase') {
        var currentElement = parseInt(editElement.html())
        var newElement = currentElement + 1
        editElement.html(newElement)
        qtyElement.html(`<h3 class="bold">${newElement}</h3>`)
        var perItemTotal = parseFloat(totalElement.html()) / parseFloat(currentElement)
        var newTotal = perItemTotal * newElement;
        totalElement.html(parseFloat(newTotal).toFixed(2));

        addonsElements.each(function() {
            var addonTotal = $(this).text().split(" ")
            addonTotal = addonTotal[addonTotal.length - 1]; // Get the last word in string (in this case, it's the addon price)
            var perAddonTotal = parseFloat(addonTotal) / parseFloat(currentElement)
            var newAddonTotal = parseFloat(perAddonTotal) * newElement;
            console.log(addonTotal, perAddonTotal, newAddonTotal)
            $(this).html(`+ PHP ${parseFloat(newAddonTotal).toFixed(2)}`);
        })
    } else if (action == 'decrease') {
        var currentElement = parseInt(editElement.html())
        var newElement = parseInt(editElement.html()) - 1
        if (newElement == 0) {
            $(`#js-cart-item-${index}`).hide();
        } else {
            editElement.html(newElement)
            qtyElement.html(`<h3 class="bold">${newElement}</h3>`)
            var perItemTotal = parseFloat(totalElement.html()) / parseFloat(currentElement)
            var newTotal = perItemTotal * newElement;
            totalElement.html(parseFloat(newTotal).toFixed(2));
            addonsElements.each(function() {
                var addonTotal = $(this).text().split(" ")
                addonTotal = addonTotal[addonTotal.length - 1]; // Get the last word in string (in this case, it's the addon price)
                var perAddonTotal = parseFloat(addonTotal) / parseFloat(currentElement)
                var newAddonTotal = parseFloat(perAddonTotal) * newElement;
                console.log(addonTotal, perAddonTotal, newAddonTotal)
                $(this).html(`+ PHP ${parseFloat(newAddonTotal).toFixed(2)}`);
            })
        }
    }
}

function ajaxUpdateOrder(shopSlug, productId, itemId, action, index) {
    var url = '/update-cart-item/'
    $.ajax({
        url: url,
        data: {
            'item': itemId,
            'product': productId,
            'action': action,
            'username':shopSlug
        },
        success: function(data) {
            if (data.hasOwnProperty("success") && data["success"] == false) {
                new Noty({
                    type: 'error',
                    theme: 'bootstrap-v4',
                    text: data["error"],
                    layout: "bottomLeft",
                    timeout: 3000,
                    progressBar: true,
                }).show();
            } else {
                data = JSON.parse(data)
                editOrderProduct(shopSlug, productId, itemId, action, index)
                var subtotal = data[0]["fields"]["subtotal"]
                var fees = data[0]["fields"]["fees"]
                var total = data[0]["fields"]["total"]
                $("#js-cart-total").html(`PHP ${parseFloat(total).toFixed(2)}`)
                $("#js-cart-fees").html(`PHP ${parseFloat(fees).toFixed(2)}`)
                $("#js-cart-subtotal").html(`PHP ${parseFloat(subtotal).toFixed(2)}`)
            }
        },
        error: function(xhr, error) {
            console.log(xhr, error)
            new Noty({
                type: 'error',
                theme: 'bootstrap-v4',
                text: "Can't update cart item. Please refresh the page and try again later.",
                layout: "bottomLeft",
                timeout: 3000,
                progressBar: true,
            }).show();
        }
    });
}