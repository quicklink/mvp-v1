const userAvatar = $('#js-header-options');
const profilePicModal = document.getElementById('js-profile-modal');

userAvatar.click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    profilePicModal.classList.toggle('active');
});

// Mobile Screens
const mobileNav = $("#js-mobile-nav");
const mobileCart = $("#js-cart");

const mobileCartBtn = $("#js-mobile-cart");
const exitCartMenu = $("#js-mobile-cart-exit");

const mobileMenu = $("#js-mobile-menu");
const exitMobileMenu = $("#js-mobile-menu-exit");


function toggleMenu() {
    mobileMenu.toggleClass("show");
}

function toggleCart() {
    mobileCart.toggleClass("show");
}

$(document).ready(function() {
    mobileNav.click(function() {
        toggleMenu();
    });
    
    exitMobileMenu.click(function() {
        toggleMenu();
    });
    
    mobileCartBtn.click(function() {
        toggleCart();
    });
    
    exitCartMenu.click(function() {
        toggleCart();
    });
});