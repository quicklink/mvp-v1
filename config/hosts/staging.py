from django_hosts import patterns, host
from django.conf import settings
from customers import urls as shops_urls
from config.hosts import admin_urls

host_patterns = patterns('',
    host(r'staging-app', settings.ROOT_URLCONF, name='app'),
    host(r'staging-shops', shops_urls, name='shops'),
    host(r'staging-zeus', admin_urls, name='zeus'),
)