from django.conf.urls import url
from django.contrib import admin
from django.urls import include
from django.conf import settings
from django.urls import path, include

urlpatterns = [
    url(r'', admin.site.urls),
    url(r'^invitations/', include('invitations.urls', namespace='invitations')),
    url(r'^jet/', include('jet.urls', 'jet')),
]

if settings.LOCAL_DEV:
    import debug_toolbar
    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),