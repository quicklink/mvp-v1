from django_hosts import patterns, host
from django.conf import settings
from customers import urls as shops_urls
from config.hosts import admin_urls

host_patterns = patterns('',
    host(r'testing-app', settings.ROOT_URLCONF, name='app'),
    host(r'testing-shops', shops_urls, name='shops'),
    host(r'testing-zeus', admin_urls, name='zeus')
)