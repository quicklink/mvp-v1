"""QuicklinkDev URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
import accounts

# import debug_toolbar

urlpatterns = [
    url(r'^invitations/', include('invitations.urls', namespace='invitations')),
    path('_nested_admin/', include('nested_admin.urls')),
    # url(r'^hijack/', include('hijack.urls', namespace='hijack')),
    path('accounts/', include('allauth.urls')),
    path('avatar/', include('avatar.urls')),
    path('', include(('accounts.urls', 'accounts'), namespace='users')),
    path('orders/', include(('orders.urls', 'orders'), namespace='orders')),
    path('products/', include(('products.urls', 'products'), namespace='products')),
    path('finances/', include(('finances.urls', 'finances'), namespace='finances')),
]

if settings.LOCAL_DEV:
    import debug_toolbar
    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if not settings.DEBUG:
    handler400 = 'accounts.views.handler_400'
    handler403 = 'accounts.views.handler_403'
    handler404 = 'accounts.views.handler_404'
    handler500 = 'accounts.views.handler_500'
