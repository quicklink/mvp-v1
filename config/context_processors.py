import os
from django.conf import settings

def export_vars(request):
    data = {}
    if 'DJANGO_SETTINGS_MODULE' in os.environ:
        setting = os.environ.get("DJANGO_SETTINGS_MODULE")
    else:
        if not setting.LOCAL_DEV:
            env_vars = settings.get_environ_vars()
            setting = env_vars.get("DJANGO_SETTINGS_MODULE")
        else:
            setting = "config.settings.production"
    setting = setting.split(".")[-1]
    if setting == "dev" or setting == "test":
        setting = "development"
    data['SETTING_TYPE'] = setting.upper()
    return data