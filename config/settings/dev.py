import environ
import os
from .base import *
from django.contrib.auth import authenticate, login, logout
import django.http.request

# Initialise environment variables
env = environ.Env()
env.read_env()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
LOCAL_DEV = True

SECRET_KEY = env("DEV_SECRET_KEY")

ALLOWED_HOSTS = [
    "127.0.0.1",
    "app.quicklink.local", 
    "shops.quicklink.local",
    "zeus.quicklink.local"
]

INSTALLED_APPS += [
    'invitations',
    'debug_toolbar',
]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = [
    '127.0.0.1',
]

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
DATABASES = {
    'default': {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": env("DEV_DB_NAME"),
        "USER": env("DEV_DB_USER"),
        "PASSWORD": env("DEV_DB_PW"),
        "HOST": env("DEV_DB_HOST"),
        "PORT": env("DEV_DB_PORT"),
    }
}

SESSION_COOKIE_AGE = 60 * 30
SESSION_SAVE_EVERY_REQUEST = True

# Django Email Backend
ACCOUNT_ADAPTER = 'invitations.models.InvitationsAdapter'

AWS_SES_ACCESS_KEY_ID = env("AWS_SES_ACCESS_KEY_ID")
AWS_SES_SECRET_ACCESS_KEY = env("AWS_SES_SECRET_ACCESS_KEY")

EMAIL_BACKEND = 'django_ses.SESBackend'
AWS_SES_REGION_NAME = 'ap-southeast-1'
AWS_SES_REGION_ENDPOINT = 'email-smtp.ap-southeast-1.amazonaws.com'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = env("EMAIL")
EMAIL_HOST_PASSWORD = env("EMAIL_PW")
EMAIL_USE_TLS = True
SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_USER = EMAIL_HOST_USER

# Django Static Files
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_ROOT = None
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]

# Django hosts settings
ROOT_URLCONF = 'config.urls'
ROOT_HOSTCONF = 'config.hosts.dev'
PARENT_HOST = 'quicklink.local:8000'