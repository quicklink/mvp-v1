import os
from .base import *

from pathlib import Path
import subprocess
import ast
import requests

DEBUG = True
LOCAL_DEV = False

def get_environ_vars():
    try:
        completed_process = subprocess.run(
            ['/opt/elasticbeanstalk/bin/get-config', 'environment'],
            stdout=subprocess.PIPE,
            text=True,
            check=True
        )
        return ast.literal_eval(completed_process.stdout)
    except:
        env = environ.Env()
        env.read_env()
        return env

def is_ec2_linux():
    """Detect if we are running on an EC2 Linux Instance
    See http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/identify_ec2_instances.html
    """
    if os.path.isfile("/sys/hypervisor/uuid"):
        with open("/sys/hypervisor/uuid") as f:
            uuid = f.read()
            return uuid.startswith("ec2")
    return False

def get_token():
    """Set the autorization token to live for 6 hours (maximum)"""
    headers = {
        'X-aws-ec2-metadata-token-ttl-seconds': '21600',
    }
    response = requests.put('http://169.254.169.254/latest/api/token', headers=headers)
    return response.text


def get_linux_ec2_private_ip():
    """Get the private IP Address of the machine if running on an EC2 linux server.
    See https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html"""
    if not is_ec2_linux():
        return None
    try:
        token = get_token()
        headers = {
            'X-aws-ec2-metadata-token': f"{token}",
        }
        response = requests.get('http://169.254.169.254/latest/meta-data/local-ipv4', headers=headers)
        return response.text
    except:
        return None
    finally:
        if response:
            response.close()

env_vars = get_environ_vars()
if 'STAGING_SECRET_KEY' in os.environ:
    SECRET_KEY = os.environ.get("STAGING_SECRET_KEY")
else:
    try:
        SECRET_KEY = env_vars.get("STAGING_SECRET_KEY")
    except:
        SECRET_KEY = env_vars("STAGING_SECRET_KEY")

ALLOWED_HOSTS = [
    "127.0.0.1",
    "54.251.192.153",
    # "52.221.38.126",
    # "172.31.22.35",
    # "172.31.35.249",
    # "3.0.10.208",
    "ec2-54-251-192-153.ap-southeast-1.compute.amazonaws.com",
    "awseb-awseb-knuxfjjsm26o-335767195.ap-southeast-1.elb.amazonaws.com",
    "ql-staging-env.ap-southeast-1.elasticbeanstalk.com",
    "staging-app.quicklink.ph", "staging-shops.quicklink.ph", "staging-zeus.quicklink.ph",
    "172.31.25.62", # ALB Health Check
    "3.0.253.129"
    "52.221.105.203"
]

if is_ec2_linux():
    private_ip = get_linux_ec2_private_ip()
    if private_ip:
        ALLOWED_HOSTS.append(private_ip)

INSTALLED_APPS += [
    'storages',
    'invitations',
    # 'debug_toolbar',
]

MIDDLEWARE = [
    # Third-Party Middleware (Before Django Middleware)
    'django_hosts.middleware.HostsRequestMiddleware',

    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'config.middlewares.CommonOverrideMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    # Third-Party Middleware (After Django Middleware)
    'django_user_agents.middleware.UserAgentMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django_hosts.middleware.HostsResponseMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    # 'basicauth.middleware.BasicAuthMiddleware',
]

# INTERNAL_IPS = [
#     "54.251.192.153",
# ]

if 'STAGING_DB_NAME' in os.environ:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": os.environ.get("STAGING_DB_NAME"),
            "USER": os.environ.get("STAGING_DB_USER"),
            "PASSWORD": os.environ.get("STAGING_DB_PW"),
            "HOST": os.environ.get("STAGING_DB_HOST"),
            "PORT": os.environ.get("STAGING_DB_PORT"),
        }
    }
else:
    try:
        DATABASES = {
            "default": {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": env_vars.get("STAGING_DB_NAME"),
                "USER": env_vars.get("STAGING_DB_USER"),
                "PASSWORD": env_vars.get("STAGING_DB_PW"),
                "HOST": env_vars.get("STAGING_DB_HOST"),
                "PORT": env_vars.get("STAGING_DB_PORT"),
            }
        }
    except:
        DATABASES = {
            "default": {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": env_vars("STAGING_DB_NAME"),
                "USER": env_vars("STAGING_DB_USER"),
                "PASSWORD": env_vars("STAGING_DB_PW"),
                "HOST": env_vars("STAGING_DB_HOST"),
                "PORT": env_vars("STAGING_DB_PORT"),
            }
        }

# CACHES = {
#     "default": {
#         "BACKEND": "django_redis.cache.RedisCache",
#         "LOCATION": os.environ.get("STAGING_REDIS_HOST"),
#         "OPTIONS": {
#             "CLIENT_CLASS": "django_redis.client.DefaultClient",
#             "PASSWORD": os.environ.get("STAGING_REDIS_PW"),
#         }
#     }
# }

# SESSION_ENGINE = "django.contrib.sessions.backends.cache"
# SESSION_CACHE_ALIAS = "default"
SESSION_COOKIE_AGE = 60 * 30
SESSION_COOKIE_SECURE = True
SESSION_SAVE_EVERY_REQUEST = True

# In some situations, when Redis is only used for cache, you do not want exceptions when Redis is down. This is default behavior in the memcached backend and it can be emulated in django-redis.
# For setup memcached like behaviour (ignore connection exceptions), you should set IGNORE_EXCEPTIONS settings on your cache configuration:
# DJANGO_REDIS_IGNORE_EXCEPTIONS = True

# LOGGING
# if 'LOGFILE' in os.environ:
#     LOGFILE = os.environ.get("LOGFILE"),
# else:
#     try: LOGFILE = env_vars.get("LOGFILE")
#     except: LOGFILE = env_vars("LOGFILE")

# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'handlers': {
#         'file': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': LOGFILE
#             # 'filename': '/var/app/staging/logs/debug.log',
#         },
#     },
#     'loggers': {
#         'django': {
#             'handlers': ['file'],
#             'level': 'DEBUG',
#             'propagate': True,
#         },
#     },
# }

# AWS Credentials
if "STAGING_AWS_ACCESS_KEY_ID" in os.environ:
    AWS_ACCESS_KEY_ID = os.environ.get("STAGING_AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = os.environ.get("STAGING_AWS_SECRET_ACCESS_KEY")
else:
    try:
        AWS_ACCESS_KEY_ID = env_vars.get("STAGING_AWS_ACCESS_KEY_ID")
        AWS_SECRET_ACCESS_KEY = env_vars.get("STAGING_AWS_SECRET_ACCESS_KEY")
    except:
        AWS_ACCESS_KEY_ID = env_vars("STAGING_AWS_ACCESS_KEY_ID")
        AWS_SECRET_ACCESS_KEY = env_vars("STAGING_AWS_SECRET_ACCESS_KEY")

# Django Email Backend
EMAIL_BACKEND = 'django_ses.SESBackend'
AWS_SES_REGION_NAME = 'ap-southeast-1'
AWS_SES_REGION_ENDPOINT = 'email-smtp.ap-southeast-1.amazonaws.com'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 465
EMAIL_USE_TLS = True

if "EMAIL" in os.environ:
    EMAIL_HOST_USER = os.environ.get("EMAIL")
    EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_PW")
else:
    try:
        EMAIL_HOST_USER = env_vars.get("EMAIL")
        EMAIL_HOST_PASSWORD = env_vars.get("EMAIL_PW")
    except:
        EMAIL_HOST_USER = env_vars("EMAIL")
        EMAIL_HOST_PASSWORD = env_vars("EMAIL_PW")

SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_USER = EMAIL_HOST_USER

# S3 BUCKETS CONFIG
if "STAGING_AWS_STORAGE_BUCKET_NAME" in os.environ:
    AWS_STORAGE_BUCKET_NAME = os.environ.get("STAGING_AWS_STORAGE_BUCKET_NAME")
else:
    try:
        AWS_STORAGE_BUCKET_NAME = env_vars.get("STAGING_AWS_STORAGE_BUCKET_NAME")
    except:
        AWS_STORAGE_BUCKET_NAME = env_vars("STAGING_AWS_STORAGE_BUCKET_NAME")

AWS_S3_FILE_OVERWRITE = False
AWS_DEFAULT_ACL = None
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

# Django Security
# HTTP Strict Transport Security
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_SECONDS = 60

# HTTPS / SSL
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

IMAGEKIT_DEFAULT_CACHEFILE_STRATEGY = 'accounts.imagegenerators.FixJustInTime'

# Django hosts settings
ROOT_URLCONF = 'config.urls'
ROOT_HOSTCONF = 'config.hosts.staging'

# Django Invitations
# To see more configuration commands, see documentation here: https://github.com/bee-keeper/django-invitations
ACCOUNT_ADAPTER = 'invitations.models.InvitationsAdapter'
INVITATIONS_INVITATION_ONLY = True
INVITATIONS_CONFIRM_INVITE_ON_GET = True
INVITATIONS_ACCEPT_INVITE_AFTER_SIGNUP = False

INVITATIONS_GONE_ON_ACCEPT_ERROR = True
INVITATIONS_SIGNUP_REDIRECT = '/verified-invite/'
INVITATIONS_LOGIN_REDIRECT = '/login/'