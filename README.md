# Quicklink
Quicklink is a full-stack stand-alone web application developed by Josh Taningco and M Jim Lee. We aim to automate order and financial processes, management, and documentation for small business owners to increase efficiency and return of sales.

## Setup

### Prerequisites
1. Make sure to install the following dependencies on your operating system. [Linux](https://weasyprint.readthedocs.io/en/latest/install.html#linux) [Mac](https://weasyprint.readthedocs.io/en/latest/install.html#macos) [Windows](https://weasyprint.readthedocs.io/en/latest/install.html#windows)
    - Python >3.8.8
    - cairo
    - Pango
    - GDK-PixBuf

### With Pipenv
1. Create a python environment with [pipenv](https://pipenv-fork.readthedocs.io/en/latest/)

2. Install dependencies by going to `requirements/dev` and running `pipenv install`

3. Setup a PostgreSQL database. [Mac](https://www.codementor.io/@engineerapart/getting-started-with-postgresql-on-mac-osx-are8jcopb) [Windows](https://www.microfocus.com/documentation/idol/IDOL_12_0/MediaServer/Guides/html/English/Content/Getting_Started/Configure/_TRN_Set_up_PostgreSQL.htm) 

4. Setup a Redis server. [Tutorial](https://redis.io/topics/quickstart)
   
5. If a .env file doesn't exist, create a new `.env` file in the `config/settings` folder. Template is in `config/.env.example`. See more examples [here](https://dev.to/jakewitcher/using-env-files-for-environment-variables-in-python-applications-55a1).

6. Create a superuser with `python manage.py createsuperuser --settings=config.settings.dev`. Superusers are users with special privileges needed to administer and maintain the system.

7. Create database migrations with `python manage.py makemigrations --settings=config.settings.dev`

8. Migrate database with `python manage.py migrate --settings=config.settings.dev`

9. Load initial data with `python manage.py loaddata apps/*/fixtures/*.json --settings=config.settings.dev`

10. Start development server with `python manage.py runserver --settings=config.settings.dev`

11. To test the development server with debug mode disabled, use `python manage.py runserver --settings=config.settings.dev --insecure`

### Without Pipenv
1. Create a python environment with [virtualenv](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) or [anaconda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) 

2. Activate the newly created virtual environment. [Alternative tutorial](https://medium.com/@diwassharma/starting-a-python-django-project-on-mac-os-x-c089165cf010)

3. Setup a PostgreSQL database. [Mac](https://medium.com/macoclock/mysql-on-mac-getting-started-cecb65b78e#:~:text=From%20System%20Preferences%2C%20open%20MySQL,launch%20MySQL%20from%20System%20Preferences.) [Windows](https://www.microfocus.com/documentation/idol/IDOL_12_0/MediaServer/Guides/html/English/Content/Getting_Started/Configure/_TRN_Set_up_MySQL.htm)

4. Setup a Redis server. [Tutorial](https://redis.io/topics/quickstart)
   
5. Create a new `.env` file in the `config/settings` folder. Template is in `.env.example`. 
  
6. Install dependencies with `pip install -r requirements/txt/dev.txt`.

7. Create a superuser with `python manage.py createsuperuser --settings=config.settings.dev`. Superusers are users with special privileges needed to administer and maintain the system.

8. Create database migrations with `python manage.py makemigrations --settings=config.settings.dev`

9. Migrate database with `python manage.py migrate --settings=config.settings.dev`

10. Load initial data with `python manage.py loaddata apps/*/fixtures/*.json --settings=config.settings.dev`

11. Start development server with `python manage.py runserver --settings=config.settings.dev`

12. To test the development server with debug mode disabled, use `python manage.py runserver --settings=config.settings.dev --insecure`

*Note: **Database migration** is the process of migrating data from one or more source databases (Django models) to one or more target databases (PostgreSQL database).*

## Directory Structure

    Quicklink/
        apps/                       # project-specific applications
            __init__.py
            foo/
                templates/
                     foo/
                        foo.html
                models.py
                views.py
                urls.py
        config/                     # site settings
            .env.example
            settings/
               base.py              # shared settings
               dev.py               # development settings
               test.py              # testing settings
               staging.py           # staging settings
               production.py        # production settings
            asgi.py
            wsgi.py
            urls.py
        docs/                       # documentation
        fixtures/                   # fixture files for develop, testing, and staging environments
        requirements/               # pip requirements files per environment
            _txt/                   # dependencies for different environments in .txt format
                base.txt
                dev.txt
                test.txt
                production.txt
            dev/                    # Pipfile for local / development and testing environments
            prod/                   # Pipfile for staging and production environments
        static/                     # site-specific static files
            css/
            js/
            media/                  # app images, videos, svgs, etc.
            uploads/                # shop images, customer profile images, etc.
        templates/                  # site-specific templates
        manage.py

### apps

All of your Django "apps" go in this directory. These have models, views, forms,
templates or all of the above. These should be Python packages you would add to
your project's `INSTALLED_APPS` list.

Everything in this directory is added to the `PYTHONPATH` when the
`environment.py` file is imported.

### config

Contains all the configuration of your Django installation. This includes the `settings.py` (Django app bootstrapper), `wsgi.py` (App production bootstrapper), and `urls.py` (Define URLs served by all apps and nodes)

### fixtures

Contains all initial data for testing models. It’s useful to pre-populate your database with hard-coded data when you’re first setting up an app. You can provide initial data with migrations or fixtures. See more in Django's documentation: https://docs.djangoproject.com/en/3.2/howto/initial-data/

### static

A subfolder each for CSS, Javascript and images. Third-party files (e.g. the
960.gs CSS or jQuery) go in a `vendor/` subfolder to keep your own code
separate.

### requirements

pip requirements files, optionally one for each app environment. The
`base.txt` is installed in every case.

For pipenv users, use the corresponding folder depending on the app 
environment you're working on.

### templates

Project-wide templates (i.e. those not belonging to any specific app in the
`apps/` folder). The boilerplate includes a `app.html` template that defines
these blocks:

I put these templates and static files into global templates/static directory, not inside each app. These files are usually edited by people, who doesn't care about project code structure or python at all. If you are full-stack developer working alone or in a small team, you can create per-app templates/static directory. It's really just a matter of taste.

The same applies for locale, although sometimes it's convenient to create separate locale directory.

Tests are usually better to place inside each app, but usually there is many integration/functional tests which tests more apps working together, so global tests directory does make sense.

#### <head>

`title` - Text for the browser title bar. You can set a default here and
append/prepend to it in sub-templates using `{{ super }}`.

`site_css` - Primary CSS files for the site. By default, includes
`media/css/reset.css` and `media/css/base.css`. 

`css` - Optional page-specific CSS - empty by default. Use this block if a page
needs an extra CSS file or two, but doesn't want to wipe out the files already
linked via the `site_css` block.

`extra_head` - Any extra content for between the `<head>` tags.

#### <body>

`header` - Top of the body, inside a `div` with the ID `header`.

`content` - After the `header`, inside a `div` with the ID `content`.

`footer` - After `content`, inside a `div` with the ID `footer`.

`site_js` - After all body content, includes site-wide Javascript files. By
default, includes `media/js/application.js` and jQuery. In deployed
environments, links to a copy of jQuery on Google's CDN. If running in solo
development mode, links to a local copy of jQuery from the `media/` directory -
because the best way to fight snakes on a plane is with jQuery on a plane.

`js` - Just like the `css` block, use the `js` block for page-specific
Javascript files when you don't want to wipe out the site-wide defaults in
`site_js`.

### Files

#### manage.py

The standard Django `manage.py`.

*Boilerplate code from 
https://github.com/bueda/django-boilerplate
https://github.com/app-generator/boilerplate-code-django*
https://stackoverflow.com/questions/22841764/best-practice-for-django-project-working-directory-structure

